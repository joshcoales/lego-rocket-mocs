import json
import re
import subprocess
from abc import abstractmethod
from collections import Counter, OrderedDict
from datetime import date
from enum import Enum, EnumMeta
from typing import List, Dict, Optional, Union

import markdown


class DataException(Exception):
    pass


class RenderException(Exception):
    pass


class EntryType(Enum):
    Rocket = "rocket"
    Payload = "payload"
    Other = "other"
    FallonISS = "fallon iss"


class FileType(Enum):
    PDF = "pdf"
    IO = "io"
    LXF = "lxf"
    LDR = "ldr"
    BricklinkXML = "bricklink xml"
    Mecabricks = "mecabricks"
    JPG = "jpg"
    GIF = "gif"
    PNG = "png"
    IMG = "img"
    Video = "video"
    HTML = "html"
    Unknown = "?"


class Column:
    def __init__(
            self,
            json_name: str,
            title: str,
            *,
            visible: bool = True,
            dropdown: 'Dropdown' = None
    ) -> None:
        self.json_name = json_name
        self.title = title
        self.visible = visible
        self.dropdown = dropdown

    @abstractmethod
    def entry_to_html(self, entry: 'TableEntry') -> str:
        pass

    @abstractmethod
    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> Union[str, List[Enum]]:
        pass

    def entry_to_json_value(self, entry: 'TableEntry') -> Union[str, List[str]]:
        return entry.data[self]

    def __eq__(self, other):
        return isinstance(other, Column) and self.json_name == other.json_name

    def __hash__(self):
        return hash(self.json_name)


class IDColumn(Column):

    def entry_to_html(self, entry: 'TableEntry') -> str:
        entry_id = entry.data[self]
        url = "https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#{}".format(entry_id)
        image_src = "img/{}.jpg".format(entry_id)
        image_tag = "<img src=\"{}\" alt=\"{}\" />".format(image_src, entry_id)
        return "<a href=\"{}\" id=\"{}\">{}</a>".format(url, entry_id, image_tag)

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        return md_entry.strip()


class EnumListColumn(Column):
    def __init__(
            self,
            json_name: str,
            title: str,
            enum_type: EnumMeta,
            *,
            visible: bool = True,
            dropdown: 'Dropdown' = None
    ) -> None:
        super().__init__(json_name, title, visible=visible, dropdown=dropdown)
        self.enum_type = enum_type

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> List[Enum]:
        try:
            return [self.enum_type(t.strip().lower()) for t in md_entry.strip(" ~").split(",")]
        except ValueError:
            raise DataException(
                "Unrecognised value \"{}\" in column \"{}\" for entry ID: \"{}\"".format(
                    md_entry, self.json_name, entry_id
                )
            )

    def entry_to_html(self, entry: 'TableEntry') -> str:
        text = ", ".join(t.value for t in entry.data[self])
        if entry.redacted:
            return "<del>{}</del>".format(text)
        return text

    def entry_to_json_value(self, entry: 'TableEntry') -> List[str]:
        return [val.value for val in entry.data[self]]


class TextColumn(Column):
    def __init__(
            self,
            json_name: str,
            title: str,
            *,
            pattern: Optional[str] = None,
            dropdown: 'Dropdown' = None
    ) -> None:
        super().__init__(json_name, title, dropdown=dropdown)
        self.pattern = None
        if pattern:
            self.pattern = re.compile(pattern)

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        stripped_entry = md_entry.strip()
        if stripped_entry.startswith("~~") and stripped_entry.endswith("~~"):
            stripped_entry = stripped_entry[2:-2]
        stripped_entry = stripped_entry.strip()
        # This does not need to be fullmatch, as we only care if the start of the value matches the pattern.
        if self.pattern and not self.pattern.match(stripped_entry):
            raise DataException(
                "Invalid value \"{}\" in column \"{}\" for entry ID: {}".format(
                    stripped_entry, self.json_name, entry_id
                )
            )
        return stripped_entry

    def entry_to_html(self, entry: 'TableEntry') -> str:
        if entry.redacted:
            return "<del>{}</del>".format(entry.data[self])
        return entry.data[self]


class MarkdownColumn(Column):

    def validate_markdown_entry(self, md_entry: str, entry_id: str) -> str:
        return markdown.markdown(md_entry)

    def entry_to_html(self, entry: 'TableEntry') -> str:
        if entry.redacted:
            return "<del>{}</del>".format(entry.data[self])
        return entry.data[self]


class DropdownOption:
    def __init__(self, text: str, value: str):
        self.text = text
        self.value = value


class Dropdown:
    def __init__(self, options: List[DropdownOption], *, hidden: bool = False):
        self.options = options
        self.hidden = hidden

    @abstractmethod
    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        pass

    def to_html(self, column: Column, selected_values: Optional[List[str]] = None) -> str:
        if not selected_values:
            selected_values = []
        if self.hidden:
            return ""
        html = "<label for=\"{}\">{} </label>".format(column.json_name, column.title)
        html += "<select name=\"{}\" id=\"{}\">".format(column.json_name, column.json_name)
        for option in self.options:
            html += "<option value=\"{}\" {}>{}</option>".format(
                option.value,
                "selected" if option.value in selected_values else "",
                option.text
            )
        html += "</select>"
        return html


class EntryTypesDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [DropdownOption("Any", "*")] + [DropdownOption(e.value, e.value) for e in EntryType]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        allowed_values = set(value.casefold() for value in values)
        return [
            entry
            for entry in entries
            if {entry_type.value.casefold() for entry_type in entry.data[column]}.intersection(allowed_values)
        ]


class ScaleDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [
            DropdownOption("Any", "*"),
            DropdownOption("Saturn V scale (1:110)", "1:110"),
            DropdownOption("ISS scale (1:220)", "1:220"),
            DropdownOption("Minifig scale (1:29 - 1:55)", "minifig")
        ]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        return [
            entry
            for entry in entries
            if entry.data[column] in values
        ]


class FileTypesDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [DropdownOption("Any", "*")] + [DropdownOption(e.value, e.value) for e in FileType]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        allowed_values = set(value.casefold() for value in values)
        return [
            entry
            for entry in entries
            if {file_type.value.casefold() for file_type in entry.data[column]}.intersection(allowed_values)
        ]


class PriceDropdown(Dropdown):

    def __init__(self, *, hidden: bool = False):
        options = [
            DropdownOption("Any", "*"),
            DropdownOption("Free", "free"),
            DropdownOption("Non-free", "non-free")
        ]
        super().__init__(options, hidden=hidden)

    def filter(self, entries: List['TableEntry'], column: Column, values: List[str]) -> List['TableEntry']:
        if "*" in values:
            return entries
        result = []
        if "free" in values:
            result += [entry for entry in entries if entry.data[column] == "0"]
        if "non-free" in values:
            result += [entry for entry in entries if entry.data[column] != "0"]
        return result


class TableEntry:
    def __init__(
            self,
            data: Dict[Column, Union[str, List[Enum]]],
            redacted: bool
    ) -> None:
        self.data = data
        self.redacted = redacted

    @classmethod
    def from_markdown_table_row(cls, table_row: str, columns: List[Column]) -> 'TableEntry':
        row_entries = table_row.split("|")
        entry_id = row_entries[0].strip()
        cells = OrderedDict()
        for index, column in enumerate(columns):
            cells[column] = column.validate_markdown_entry(row_entries[index], entry_id)
        raw_name = row_entries[2]
        redacted = raw_name.strip().startswith("~~")
        return cls(cells, redacted)

    def to_json(self) -> Dict[str, str]:
        json_dict = {
            column.json_name: column.entry_to_json_value(self)
            for column in self.data.keys()
        }
        json_dict["redacted"] = self.redacted
        return json_dict

    def to_html_row(self) -> str:
        cells = [
            column.entry_to_html(self)
            for column in self.data.keys()
            if column.visible
        ]
        return "<tr>\n{}\n</tr>".format("\n".join("<td>{}</td>".format(cell) for cell in cells))


class Table:
    id_column = IDColumn("entry_id", "Images")
    columns = [
        id_column,
        EnumListColumn("types", "Types", EntryType, visible=False, dropdown=EntryTypesDropdown(hidden=True)),
        TextColumn("name", "Name"),
        TextColumn("scale", "Scale", pattern=r"\?|X|minifig|~?1:[0-9]+", dropdown=ScaleDropdown()),
        TextColumn("parts", "Parts", pattern=r"\?|X|[0-9]+"),
        TextColumn("designer", "Designer"),
        EnumListColumn("file_types", "Instr.", FileType, dropdown=FileTypesDropdown()),
        TextColumn("price", "Price", dropdown=PriceDropdown()),
        MarkdownColumn("links", "Links"),
        MarkdownColumn("notes", "Notes")
    ]

    def __init__(self, entries: List[TableEntry]) -> None:
        self.entries = entries

    @classmethod
    def load_from_file(cls, filename: str) -> 'Table':
        entries = []
        exceptions = []
        with open(filename, "r") as f:
            for line in f.readlines():
                if line.startswith("---|---|"):
                    continue
                if line.startswith("ID | Type(s) "):
                    continue
                if line.strip() == "":
                    continue
                try:
                    entries.append(TableEntry.from_markdown_table_row(line, cls.columns))
                except DataException as e:
                    exceptions.append(e)
        if exceptions:
            raise DataException("Data exceptions raised during parsing:\n" + "\n".join(str(e) for e in exceptions))
        entry_ids = [entry.data[cls.id_column] for entry in entries]
        counter = Counter(entry_ids)
        duplicates = [k for k, v in counter.items() if v > 1]
        if duplicates:
            raise DataException("Duplicate entry IDs: {}".format(duplicates))
        return cls(entries)

    def to_html(self, *, filters: Optional[Dict[str, List[str]]] = None) -> str:
        filtered_entries = self.filter_entries(filters)
        if not filters:
            filters = {}

        data_filters = "|".join("{}={}".format(key, ",".join(values)) for key, values in filters.items())
        dropdowns = " | ".join(
            column.dropdown.to_html(column, filters.get(column.json_name, []))
            for column in self.columns
            if column.dropdown and not column.dropdown.hidden
        )
        table_header = "<thead>\n<tr class=\"header\">\n{}\n</tr>\n</thead>".format(
            "\n".join(
                "<th data-column=\"{}\">{}</th>".format(column.json_name, column.title)
                for column in self.columns
                if column.visible
            )
        )
        table_body = "<tbody>\n{}\n</tbody>".format(
            "\n".join(entry.to_html_row() for entry in filtered_entries)
        )
        return "<form class=\"rocket_table\" data-filters=\"{}\">{}<table>\n{}\n{}\n</table></form>".format(
            data_filters, dropdowns, table_header, table_body
        )

    def save_to_js(self, filename: str) -> None:
        with open(filename, "w") as f:
            rocket_data = json.dumps([
                entry.to_json() for entry in self.entries
            ], indent=2)
            header_comment = "//This file contains the rocket data in javascript-consumable format\n"
            js_source = "{}const rocketData = {}".format(header_comment, rocket_data)
            f.write(js_source)

    def filter_entries(self, filters: Optional[Dict[str, List[str]]]) -> List[TableEntry]:
        if not filters:
            return self.entries
        column_dict = {
            c.json_name: c for c in self.columns
        }
        entries = self.entries
        for col_name, values in filters.items():
            col = column_dict.get(col_name)
            if not col:
                raise RenderException("Unrecognised column in table filters: {}".format(col_name))
            dropdown = col.dropdown
            if not dropdown:
                raise RenderException("Column \"{}\" cannot be filtered on.".format(col_name))
            entries = dropdown.filter(entries, col, values)
        return entries


class Page:
    table_placeholder = "BUILDTABLEPLACEHOLDER"
    date_placeholder = "MAKEFILEDATEPLACEHOLDER"

    def __init__(self, file_name: str, page_html: str) -> None:
        self.file_name = file_name
        self.page_html = page_html

    @classmethod
    def load_from_file(cls, filename: str) -> 'Page':
        with open(filename, "r", encoding='utf-8') as f:
            return cls(filename, f.read())

    def replace_table_placeholders(self, table: Table) -> None:
        placeholder_re = re.compile(r"(?:<([^>]+)>|){}(?:\((.*)\))?(?:</\1>)?".format(self.table_placeholder))
        for match in placeholder_re.finditer(self.page_html):
            args = match.group(2) or ""
            args_dict: Dict[str, List[str]] = {}
            for arg in args.split("|"):
                key, val = arg.split("=")
                args_dict[key] = val.split(",")
            table_html = table.to_html(filters=args_dict)
            self.page_html = self.page_html.replace(match.group(0), table_html)

    def replace_image_placeholders(self) -> None:
        self.page_html = re.sub(
            r'!<(.*?)>!',
            r'<a href="https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#\1" name="\1"><img src="img/\1.jpg" alt="\1" /></a>',
            self.page_html
        )

    def replace_date_placeholder(self) -> None:
        self.page_html = self.page_html.replace(self.date_placeholder, date.today().strftime("%Y.%m.%d"))

    def save(self) -> None:
        with open(self.file_name, "w") as f:
            f.write(self.page_html)


if __name__ == "__main__":
    # Parse rocket data into table
    data_table = Table.load_from_file("./rocket_data.md")
    # Run pandoc to convert markdown to html
    # pandoc:
    # -f: format is markdown github
    # --css: css style sheet
    # -s: standalone
    subprocess.run(
        "pandoc -f markdown_github+markdown_in_html_blocks --css pandoc.css -o public/index.html rockets.md",
        shell=True
    )
    # Load page
    page = Page.load_from_file("public/index.html")
    # Replace table placeholders with tables
    page.replace_table_placeholders(data_table)
    # Replace placeholders in official lego set table
    page.replace_image_placeholders()
    # Replace date placeholder with date
    page.replace_date_placeholder()
    # Save table data for javascript
    data_table.save_to_js("public/table_data.js")
    # Save file
    page.save()
