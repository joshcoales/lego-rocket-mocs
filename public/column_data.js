// This file sets up the table column behaviours

const itemToText = (item, text) => {
    if (item["redacted"]) {
        return `<del>${text}</del>`
    }
    return text
}

const listsHaveOverlap = (list1, list2) => {
    return list1.some((item) => list2.map((s) => s.toLowerCase()).includes(item.toLowerCase()))
}

const sortLocaleCompare = (list, key) => {
    return list.sort((a, b) => a[key].localeCompare(b[key]))
}

const scaleStringToFactor = (scaleString) => {
    if(scaleString === "minifig") {
        // Assuming minifig scale is 1:35, for the purposes of sorting
        return 35;
    }
    return parseFloat(scaleString.split(/[: ]/)[1]);
}

const IDColumnKey = "entry_id"
const IDColumn = {
    key: IDColumnKey,
    title: "Images",
    visible: true,
    sort: (list) => sortLocaleCompare(list, IDColumnKey),
    dropdown: null,
    toHtml: (item) => `<a href="https://kaerodot.gitlab.io/lego-rocket-mocs/index.html#${item[IDColumnKey]}" id="${item[IDColumnKey]}">
<img src="img/${item[IDColumnKey]}.jpg" alt="${item[IDColumnKey]}" />
</a>`,
}
const TypesColumnKey = "types"
const TypesColumn = {
    key: TypesColumnKey,
    title: "Types",
    visible: false,
    sort: (list) => sortLocaleCompare(list, TypesColumnKey),
    dropdown: {
        filter: (list, allowedValues) => list.filter((item) => {
            return listsHaveOverlap(item[TypesColumnKey], allowedValues)
        }),
    },
    toHtml: (item) => itemToText(item, item[TypesColumnKey]),
}
const NameColumnKey = "name"
const NameColumn = {
    key: NameColumnKey,
    title: "Name",
    visible: true,
    sort: (list) => sortLocaleCompare(list, NameColumnKey),
    dropdown: null,
    toHtml: (item) => itemToText(item, item[NameColumnKey]),
}
const ScaleColumnKey = "scale"
const ScaleColumn = {
    key: ScaleColumnKey,
    title: "Scale",
    visible: true,
    sort: (list) => {
        const xList = list.filter((item) => item[ScaleColumnKey].startsWith("X"))
        const qList = list.filter((item) => item[ScaleColumnKey].startsWith("?"))
        const numList = list.filter(
            (item) => !item[ScaleColumnKey].startsWith("X") && !item[ScaleColumnKey].startsWith("?"))
            .sort((itemA, itemB) => {
                    const scaleFactorA = scaleStringToFactor(itemA[ScaleColumnKey])
                    const scaleFactorB = scaleStringToFactor(itemB[ScaleColumnKey])
                    return scaleFactorA - scaleFactorB
                }
            )
        return [...xList, ...numList, ...qList]
    },
    dropdown: {
        filter: (list, allowedValues) => list.filter((item) => {
            const scaleFactor = scaleStringToFactor(item[ScaleColumnKey])
            if (allowedValues.includes("minifig")) {
                // Minifigs are somewhere between 1:29 scale and 1:55 scale
                return 29 <= scaleFactor && scaleFactor <= 55
            }
            const allowedScaleFactors = allowedValues.map(s => scaleStringToFactor(s))
            return !!allowedScaleFactors.includes(scaleFactor);
        }),
    },
    toHtml: (item) => itemToText(item, item[ScaleColumnKey]),
}
const PartsColumnKey = "parts"
const PartsColumn = {
    key: PartsColumnKey,
    title: "Parts",
    visible: true,
    sort: (list) => {
        const xList = list.filter((item) => item[PartsColumnKey].startsWith("X"))
        const qList = list.filter((item) => item[PartsColumnKey].startsWith("?"))
        const numList = list.filter(
            (item) => !item[PartsColumnKey].startsWith("X") && !item[PartsColumnKey].startsWith("?"))
            .sort((itemA, itemB) => {
                    const numA = parseInt(itemA[PartsColumnKey].match(/\d+/)[0])
                    const numB = parseInt(itemB[PartsColumnKey].match(/\d+/)[0])
                    return numA - numB
                }
            )
        return [...xList, ...numList, ...qList]
    },
    dropdown: null,
    toHtml: (item) => itemToText(item, item[PartsColumnKey]),
}
const DesignerColumnKey = "designer"
const DesignerColumn = {
    key: DesignerColumnKey,
    title: "Designer",
    visible: true,
    sort: (list) => sortLocaleCompare(list, DesignerColumnKey),
    dropdown: null,
    toHtml: (item) => itemToText(item, item[DesignerColumnKey]),
}
const InstructionsColumnKey = "file_types"
const InstructionsColumn = {
    key: InstructionsColumnKey,
    title: "Instr.",
    visible: true,
    sort: (list) => {
        return list.sort((itemA, itemB) => {
            const fileTypesA = itemA[InstructionsColumnKey].sort((a, b) => a.localeCompare(b)).join(", ")
            const fileTypesB = itemB[InstructionsColumnKey].sort((a, b) => a.localeCompare(b)).join(", ")
            return fileTypesA.localeCompare(fileTypesB)
        })
    },
    dropdown: {
        filter: (list, allowedValues) => list.filter((item) => {
            return listsHaveOverlap(item[InstructionsColumnKey], allowedValues)
        }),
    },
    toHtml: (item) => itemToText(item, item[InstructionsColumnKey].join(", ")),
}
const PriceColumnKey = "price"
const PriceColumn = {
    key: PriceColumnKey,
    title: "Price",
    visible: true,
    sort: (list) => {
        return list.sort((a, b) => {
            const priceA = parseFloat(a[PriceColumnKey].replace(/[^\d.]/, ''))
            const priceB = parseFloat(b[PriceColumnKey].replace(/[^\d.]/, ''))
            return priceA - priceB
        })
    },
    dropdown: {
        filter: (list, allowedValues) => list.filter((item) => {
            if (allowedValues.includes("free")) {
                return item[PriceColumnKey] === "0"
            }
            return item[PriceColumnKey] !== "0"
        })
    },
    toHtml: (item) => itemToText(item, item[PriceColumnKey]),
}
const LinksColumnKey = "links"
const LinksColumn = {
    key: LinksColumnKey,
    title: "Links",
    visible: true,
    sort: (list) => sortLocaleCompare(list, LinksColumnKey),
    dropdown: null,
    toHtml: (item) => itemToText(item, item[LinksColumnKey]),
}
const NotesColumnKey = "notes"
const NotesColumn = {
    key: NotesColumnKey,
    title: "Notes",
    visible: true,
    sort: (list) => sortLocaleCompare(list, NotesColumnKey),
    dropdown: null,
    toHtml: (item) => itemToText(item, item[NotesColumnKey]),
}

const listColumns = [
    IDColumn,
    TypesColumn,
    NameColumn,
    ScaleColumn,
    PartsColumn,
    DesignerColumn,
    InstructionsColumn,
    PriceColumn,
    LinksColumn,
    NotesColumn
]
const visibleColumns = listColumns.filter((column) => column.visible)
// Create a map of the above list of table data
const columnMap = listColumns.reduce(function (obj, column) {
    obj[column["key"]] = column
    return obj
}, {})