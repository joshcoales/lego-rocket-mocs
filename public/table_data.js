//This file contains the rocket data in javascript-consumable format
const rocketData = [
  {
    "entry_id": "MOCmoppew40kangara",
    "types": [
      "rocket"
    ],
    "name": "Angara",
    "scale": "1:110",
    "parts": "991",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane1",
    "types": [
      "rocket"
    ],
    "name": "Ariane 1",
    "scale": "1:110",
    "parts": "195",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-1/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365124836684/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/ariane-1/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/909830362755549/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane2",
    "types": [
      "rocket"
    ],
    "name": "Ariane 2",
    "scale": "1:110",
    "parts": "197",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-2/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane3",
    "types": [
      "rocket"
    ],
    "name": "Ariane 3",
    "scale": "1:110",
    "parts": "292",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-3/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane440",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 40",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-40/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane442l",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 42L",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-42l/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane442p",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 42P",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-42p/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane444l",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 44L",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-44l/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane444lp",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 44LP",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-44lp/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365278170002/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/756697184735535/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane444lphermes",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 44LP Hermes",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-44lp-hermes/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauariane444p",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 44P",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/ariane-4-44p/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kariane5",
    "types": [
      "rocket"
    ],
    "name": "Ariane 5",
    "scale": "1:110",
    "parts": "1097",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCmoppew40katv\">ATV</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kariane6",
    "types": [
      "rocket"
    ],
    "name": "ESA Arianespace Ariane 6",
    "scale": "1:110",
    "parts": "1330",
    "designer": "MoppeW40k",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-29196/MoppeW40k/esa-arianespace-ariane-6-1110-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCchhaariane5",
    "types": [
      "rocket"
    ],
    "name": "Ariane 5",
    "scale": "1:110",
    "parts": "?",
    "designer": "Chha",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksafe.com/pages/Chha/space-flight-scale-1-110/ariane-5\">Bricksafe</a> </p>",
    "notes": "<p><a href=\"https://ideas.lego.com/projects/686aa72d-e2d5-4ef5-893b-252b2e328099/comments_tab\">Project on Lego Ideas</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerarianev",
    "types": [
      "rocket"
    ],
    "name": "Midi ESA Ariane V",
    "scale": "?",
    "parts": "293",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56871/SpaceXplorer%2016/midi-esa-ariane-v-rocket/\">Rebrickable</a> </p>",
    "notes": "<p>Contains also Hermes shuttle.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCflorenttodeschiniariane444lp",
    "types": [
      "rocket"
    ],
    "name": "Ariane 4 (A44 LP)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florent Todeschini",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/745641442507776/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/745370059201581/\">Facebook post with pictures</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40katlasii",
    "types": [
      "rocket"
    ],
    "name": "Atlas IIAS",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedatlasv551",
    "types": [
      "rocket"
    ],
    "name": "Atlas V 551",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40katlascentaur",
    "types": [
      "rocket"
    ],
    "name": "Atlas Centaur with Pioneer probe",
    "scale": "1:110",
    "parts": "279",
    "designer": "MoppeW40k",
    "file_types": [
      "io",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/atlas-centaur/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Based on <a href=\"#MOClegorocketsmercuryatlas\">Atlas by legorockets</a>. Pioneer probe by luxordeathbed. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40katlasv",
    "types": [
      "rocket"
    ],
    "name": "Atlas V with Juno Space Probe",
    "scale": "1:110",
    "parts": "462",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtheholypumpkinatlasjunonewhorizons",
    "types": [
      "rocket"
    ],
    "name": "Atlas V 551 with Juno and New Horizons Probe",
    "scale": "1:90",
    "parts": "1111",
    "designer": "theholypumpkin",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=73423\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtheholypumpkinatlascststarliner",
    "types": [
      "rocket"
    ],
    "name": "Atlas V N22 with CST Starliner",
    "scale": "1:90",
    "parts": "830",
    "designer": "theholypumpkin",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=73465\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwillmatlasvstarliner",
    "types": [
      "rocket"
    ],
    "name": "ULA Atlas V N22 with Boeing Starliner",
    "scale": "1:110",
    "parts": "435",
    "designer": "WillM",
    "file_types": [
      "pdf"
    ],
    "price": "$1.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47499/WillM/starliner-atlas-v-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtheholypumpkinatlasosiris",
    "types": [
      "rocket"
    ],
    "name": "Atlas V 411 with OSIRIS-REx Probe",
    "scale": "1:90",
    "parts": "752",
    "designer": "theholypumpkin",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=73469\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichultimateatlasv",
    "types": [
      "rocket"
    ],
    "name": "Ultimate Atlas V",
    "scale": "1:110",
    "parts": "864",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$6.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47289/MuscoviteSandwich/ultimate-atlas-v-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>10 launch variants, includes <a href=\"#MOCmuscovitesandwichmars2020\">Centaur stage and Perseverance mars rover</a>. <a href=\"#MOCmuscovitesandwichnewhorizons\">Compatible New horizon probe</a> can fit inside. Instructions contains decals. <a href=\"#MOCmuscovitesandwichatlasvn22\">Version modified for Boeing Starliner</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichatlasvn22",
    "types": [
      "rocket"
    ],
    "name": "Atlas V N22 and Boeing Starliner",
    "scale": "1:110",
    "parts": "551",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-59417/MuscoviteSandwich/atlas-v-n22-and-boeing-starliner-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"#MOCmuscovitesandwichboeingstarliner\">With compatible Boeing Starliner</a>. It is modified <a href=\"#MOCmuscovitesandwichultimateatlasv\">Ultimate Atlas V</a> modified for Boeing Starliner.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbanthab0mbathena1marsrocket",
    "types": [
      "rocket"
    ],
    "name": "Athena 1 Mars Rocket",
    "scale": "?",
    "parts": "57",
    "designer": "banthab0mb",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=97043\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbanthab0mbathena1payload\">payload</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40knewglenn",
    "types": [
      "rocket"
    ],
    "name": "Blue Origin New Glenn",
    "scale": "1:110",
    "parts": "1563",
    "designer": "MoppeW40k",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28692/MoppeW40k/blue-origin-new-glenn-1110-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/launch-vehicles/new-glenn/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/file/d/1JJFsM3KzdRl3c58zarKA859wUC6AcLml/view\">Direct link</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsnewglenn",
    "types": [
      "rocket"
    ],
    "name": "Blue Origin New Glenn",
    "scale": "1:110",
    "parts": "529",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-27082/MOCrockets/blue-origin-new-glenn-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaubluestreak",
    "types": [
      "rocket"
    ],
    "name": "Blue Streak",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/europa-blue-streak-cora/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365668169963/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/756698208068766/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914012281796/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40knewshepard",
    "types": [
      "rocket"
    ],
    "name": "Blue Origin New Shepard",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/901202293618356/\">Facebook post with images</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorernewshepherd",
    "types": [
      "rocket"
    ],
    "name": "Midi Blue Origin New Shepherd Booster",
    "scale": "?",
    "parts": "133",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56407/SpaceXplorer%2016/midi-blue-origin-new-shepherd-booster/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsilentdawn03blackarrow",
    "types": [
      "rocket"
    ],
    "name": "Black arrow (lipstick rocket)",
    "scale": "1:82",
    "parts": "74",
    "designer": "silentdawn03",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-37558/silentdawn03/black-arrow-lipstick-rocket-182-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaublackarrow",
    "types": [
      "rocket"
    ],
    "name": "Black Arrow",
    "scale": "1:110",
    "parts": "17",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/uk-space-agency/black-arrow/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188913618948502/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912966952441890/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonbumperwac",
    "types": [
      "rocket"
    ],
    "name": "Bumper-WAC",
    "scale": "1:110",
    "parts": "137",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/bumper-wac/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaucora",
    "types": [
      "rocket"
    ],
    "name": "Cora",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/europa-blue-streak-cora/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365668169963/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/756698208068766/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914012281796/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaudiamant",
    "types": [
      "rocket"
    ],
    "name": "Diamant A, B and BP4",
    "scale": "1:110",
    "parts": "67",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/cnes/diamant-a-b-bp4/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365518169978/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912967352441850/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188913708948493/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/diamant-series/\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"#MOCyourihamondiamantlaunchpad\">Compatible launchpad</a>, and <a href=\"#MOCyourihamondiamantlaunchpadcheap\">cheaper version of launchpad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichdeltaii",
    "types": [
      "rocket"
    ],
    "name": "Delta II rocket with MER rover, improved version",
    "scale": "1:110",
    "parts": "407",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28326/MuscoviteSandwich/delta-ii-with-mer-rover-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/launch-vehicles/delta-ii-7925/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Two versions: cheaper Sand Green and <a href=\"https://rebrickable.com/mocs/MOC-54675/MuscoviteSandwich/delta-ii-with-mer-rover-teal-version-saturn-v-scale/\">more accurate Teal</a>. Compatible <a href=\"#MOCmuscovitesandwichdeltaiilaunch\">launch tower</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kdeltaiispitzer",
    "types": [
      "rocket"
    ],
    "name": "Delta II with Spitzer Space Telescope",
    "scale": "1:110",
    "parts": "239",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Spitzer telescope by Mark Balderrama. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjoechambersdeltaii",
    "types": [
      "rocket"
    ],
    "name": "Delta II",
    "scale": "~1:75",
    "parts": "?",
    "designer": "Joe Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/705852663153321/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>ULA Delta II 7925 and 7925-H For Spirit and Opportunity Mars Exploration Rovers, with Star 48 / Delta Payload Assist Module and Cruise Stage and Entry Vehicle for Mars Exploration Rovers</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kdeltaiii",
    "types": [
      "rocket"
    ],
    "name": "Delta III",
    "scale": "1:110",
    "parts": "468",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Google drive is from Facebook group <a href=\"https://www.facebook.com/groups/legospacebuilds/\">Facebook group Bricks in Space</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrenebricks2858deltaivparker",
    "types": [
      "rocket"
    ],
    "name": "Delta 4 Heavy and Parker Solar Probe",
    "scale": "1:110",
    "parts": "1476",
    "designer": "Renebricks2858",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://ideas.lego.com/projects/de2cd76a-adef-4f99-b14b-7ef9a589e121\">Lego Ideas</a>, </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradforddeltaivmheavy",
    "types": [
      "rocket"
    ],
    "name": "Delta IV M, Heavy",
    "scale": "1:110",
    "parts": "794 - 1665",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/454420784963178/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/454420941629829/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Various configurations: Delta <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/454420811629842/\">IV M</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/454420838296506/\">(4,2)</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/454420898296500/\">(5,2)</a>, (5,4), Heavy.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsilentdawn03deltaivheavy",
    "types": [
      "rocket"
    ],
    "name": "Delta IV Heavy",
    "scale": "1:305",
    "parts": "75",
    "designer": "silentdawn03",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-35684/silentdawn03/delta-iv-heavy-1305-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichdeltaivheavy",
    "types": [
      "rocket"
    ],
    "name": "Delta IV Heavy",
    "scale": "1:110",
    "parts": "1853",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$9.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-43290/MuscoviteSandwich/delta-iv-heavy-1110-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerdeltaivmedium",
    "types": [
      "rocket"
    ],
    "name": "ULA Delta IV Medium",
    "scale": "1:305",
    "parts": "46",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-51645/SpaceXplorer/ula-1305-scale-delta-iv-medium/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCskylabbricksdeltaiv",
    "types": [
      "rocket"
    ],
    "name": "Delta IV Evolved Expendable Launch Vehicle family",
    "scale": "1:110",
    "parts": "?",
    "designer": "Sam Bradford, Joseph Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "$10.00",
    "links": "<p><a href=\"https://www.etsy.com/listing/884246817/delta-iv-evolved-expendable-launch?ref=shop_home_active_4\">Etsy</a> </p>",
    "notes": "<p>Includes Delta IV Medium, Medium+ (4,2), (5,2), (5,4), and 3 versions of Heavy. Payloads: Parker Solar Probe, Orion EFT-1 flight test, KH-11 spy satellite, several more.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaudnepr1",
    "types": [
      "rocket"
    ],
    "name": "Dnepr 1",
    "scale": "1:110",
    "parts": "239",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/dnepr-1/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188913782281819/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaueuropa",
    "types": [
      "rocket"
    ],
    "name": "Europa",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/esa/europa-blue-streak-cora/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/263365668169963/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912967909108461/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914012281796/\">Facebook group Lego Rocket Collection</a>  </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCgbdannyexplorer1",
    "types": [
      "rocket"
    ],
    "name": "Mini Explorer 1",
    "scale": "?",
    "parts": "78",
    "designer": "GBDanny",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=16641\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59energiapolyus",
    "types": [
      "rocket"
    ],
    "name": "Energia super heavy-lift launch vehicle",
    "scale": "?",
    "parts": "1437",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/X1k2q4xnaE0\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCflorenttodeschinienergiapolyus",
    "types": [
      "rocket"
    ],
    "name": "Energia with Polyus",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florent Todeschini",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/energia-polyus/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/708752289530025/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCmoppew40kenergiapolyus\">Energia with polyus by MoppeW40k</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kenergiapolyus",
    "types": [
      "rocket"
    ],
    "name": "Energia with Polyus",
    "scale": "1:110",
    "parts": "1842",
    "designer": "MoppeW40k",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCsunder59energiapolyus\">Energia by Sunder59</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanburan",
    "types": [
      "rocket"
    ],
    "name": "Energia-Buran ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "2103",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/energia-buran/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>. <a href=\"#MOCallanjordanburanonly\">Buran only</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanburanonly",
    "types": [
      "rocket"
    ],
    "name": "Buran ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "474",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/buran/\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"#MOCallanjordanburan\">Complex with Energia</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanburant",
    "types": [
      "rocket"
    ],
    "name": "Buran-T ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanenergiam",
    "types": [
      "rocket"
    ],
    "name": "Energia-M ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "1173",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/energia-m/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanenergia",
    "types": [
      "rocket"
    ],
    "name": "Energia ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "1634",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/energia/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanpolyus",
    "types": [
      "rocket"
    ],
    "name": "Energia-Polyus ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "1704",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanpolyusonly",
    "types": [
      "rocket"
    ],
    "name": "Polyus ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "64",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/polyus/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanvulcan",
    "types": [
      "rocket"
    ],
    "name": "Vulkan ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/vulkan/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanvulcanhercules",
    "types": [
      "rocket"
    ],
    "name": "Vulkan-Hercules ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/717042378701016/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanvulkanherculesv2",
    "types": [
      "rocket"
    ],
    "name": "Vulkan-Hercules variant 2 ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/943911956014056/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/938033416601910/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanvulkanherculesv2a",
    "types": [
      "rocket"
    ],
    "name": "Vulkan-Hercules variant 2a ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/943912059347379/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/938033416601910/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanvulkanherculesv2b",
    "types": [
      "rocket"
    ],
    "name": "Vulkan-Hercules variant 2b ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/943912176014034/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/938033416601910/\">Facebook post with description</a>. <a href=\"#energia-family\">Part of Energia faimly</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanuragan1",
    "types": [
      "rocket"
    ],
    "name": "Uragan version 1 ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732066973865223/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732066150531972/\">Facebook post with description</a>. <a href=\"#MOCallanjordanuragan2\">Version 2</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordanuragan2",
    "types": [
      "rocket"
    ],
    "name": "Uragan version 2 ([Energia family](#energia-family))",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732067083865212/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732066150531972/\">Facebook post with description</a>. <a href=\"#MOCallanjordanuragan1\">Version 1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkehu05cygnus",
    "types": [
      "rocket"
    ],
    "name": "Cygnus spacecraft",
    "scale": "1:82",
    "parts": "156",
    "designer": "kehu05",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36345/kehu05/182-cygnus-spacecraft/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovecygnus",
    "types": [
      "rocket"
    ],
    "name": "Cygnus",
    "scale": "?",
    "parts": "705",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "$7.40",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-0305/Perijove/cygnus/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloncygnus",
    "types": [
      "rocket"
    ],
    "name": "Cygnus",
    "scale": "1:110",
    "parts": "42",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/cygnus-enhanced/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Version 2.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobcygnus",
    "types": [
      "rocket"
    ],
    "name": "Cygnus",
    "scale": "1:220",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/778126629259257/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonhtv",
    "types": [
      "rocket"
    ],
    "name": "H-II Transfer Vehicle (HTV)",
    "scale": "1:110",
    "parts": "168",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/h-ii-transfer-vehicle/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40khtv",
    "types": [
      "rocket"
    ],
    "name": "H-II Transfer Vehicle (HTV)",
    "scale": "1:110",
    "parts": "158",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://bricksin.space/launch-vehicles/h-ii-b/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Module is part of digital file of compatible rocket <a href=\"#MOCmoppew40khiib\">H-IIB</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauhermes",
    "types": [
      "rocket"
    ],
    "name": "Hermes: European Space Shuttle",
    "scale": "1:110",
    "parts": "207",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/navettes/esa/hermes/\">Lego Rocket Collection</a>, <a href=\"https://bricksin.space/manned-spacecraft/hermes/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/271205724052624/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/905450999860152/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/267969671042896\">Facebook post with description and pictures</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCzzubnikfalcon1launchpad",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 1 on launch pad",
    "scale": "1:110",
    "parts": "73",
    "designer": "zzubnik",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=98105\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichfalcon1",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 1",
    "scale": "1:110",
    "parts": "?",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsilentdawn03falcon1",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon1",
    "scale": "1:53",
    "parts": "82",
    "designer": "silentdawn03",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-39955/silentdawn03/spacex-falcon1-153-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Two versions of instructions: with cheap bricks and with expensive bricks.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanyfalcon9booster",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Booster",
    "scale": "?",
    "parts": "209",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-23575/Brickbrothers-Germany/spacex-falcon-9-booster/\">Rebrickable</a>~~ </p>",
    "notes": "<p>~~Falcon 9 Booster with a droneship.~~ MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCzzubnikspacexdroneshipfalcon9v11",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Drone Ship with Falcon 9 V1.1",
    "scale": "?",
    "parts": "162",
    "designer": "zzubnik",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=96623\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCquantummechanicfalcon9droneship",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Rocket and Droneship",
    "scale": "1:230",
    "parts": "229",
    "designer": "quantummechanic",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=29574\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCramonmontanafalcon9droneshipjrti",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Micro Falcon 9 with Drone Ship \"Just Read the Instructions\"",
    "scale": "?",
    "parts": "?",
    "designer": "Ramon Monti Montana",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/796774777394442/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCramonmontanafalcon9droneshipocisly",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Micro Falcon 9 with Drone Ship \"Of Course I Still Love You\"",
    "scale": "?",
    "parts": "?",
    "designer": "Ramon Monti Montana",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/796774094061177/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsfalcon9x37b",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Block 5 Crew Dragon with X-37B",
    "scale": "1:110",
    "parts": "222",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$3.33",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-24568/MOCrockets/spacex-falcon-9-block-5-crew-dragon-with-x-37b-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"https://rebrickable.com/mocs/MOC-17641/MOCrockets/spacex-falcon-9-block-5-saturn-v-scale/\">Previous version</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrepragefalcon9",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9",
    "scale": "1:124",
    "parts": "98",
    "designer": "RepRage",
    "file_types": [
      "gif",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://reprage.com/post/lego-falcon-9\">Home page</a>, <a href=\"https://ideas.lego.com/projects/ba33d753-7be1-4900-99b7-b9522f3dc822\">Lego Ideas</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCrepragedroneship\">Droneship</a>, <a href=\"#MOCrepragedragoncapsule\">Dragon</a>, <a href=\"#MOCrepragedragonv2capsule\">Dragon 2</a>, <a href=\"#MOCreprageorbcomm2payload\">Orbcomm payload</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmartin0131falcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9",
    "scale": "1:58",
    "parts": "1250",
    "designer": "Super Penguin",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://moc-pages.com/moc.php/436584\">MOC pages</a> </p>",
    "notes": "<p>Also on <a href=\"https://ideas.lego.com/projects/6eeaabab-8e6e-4ced-88c0-21a72d85c2b5\">Lego Ideas, nick Martin0131</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCendregaspacexfalcon9",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Block 5 with Bangabandhu Satellite-1",
    "scale": "1:110",
    "parts": "253",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=41293\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCendregaspacexfalcon9orbcomm",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 with Orbcomm Satellites",
    "scale": "1:110",
    "parts": "192",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=28727\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCfives14falcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9 WIP 1.1",
    "scale": "?",
    "parts": "647",
    "designer": "Fives14",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=95390\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcurtisdffalcon9",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Rocket",
    "scale": "1:110",
    "parts": "224",
    "designer": "curtisdf",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=32747\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCcurtisdforbcomm\">payload</a> and <a href=\"#MOCcurtisdffalcon9launchpad\">launchpad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCendregaspacexfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy with Scale Tesla Roadster v2",
    "scale": "1:110",
    "parts": "532",
    "designer": "endrega",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/785665388505381/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/785658518506068/\">Facebook group Bricks in Space</a>, <a href=\"https://studio.bricklink.com/v3/studio/design.page?idModel=39466\">Bricklink</a> </p>",
    "notes": "<p>io file at Facebook group contains contains instructions. Original version without instructions is at Bricklink. Compatible payload <a href=\"#MOCendregateslaroadster\">Tesla Roadster</a>. Compatible <a href=\"#MOCsambradfordlaunchcomplex39a\">launch pad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsfalcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9 Block 5",
    "scale": "1:110",
    "parts": "309",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=112696\">Bricklink</a> </p>",
    "notes": "<p>Contains also Dragon spacecraft in cargo and crew version.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy Block 5",
    "scale": "1:110",
    "parts": "542",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-23552/MOCrockets/spacex-falcon-heavy-block-5-saturn-v-scale-updated/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"https://rebrickable.com/mocs/MOC-19440/MOCrockets/spacex-falcon-heavy-block-5-saturn-v-scale/\">Previous version</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCxaelsootyblockvfalcon9crewdragon",
    "types": [
      "rocket"
    ],
    "name": "Sooty Block V Falcon 9 and Crew Dragon",
    "scale": "?",
    "parts": "155",
    "designer": "Xael",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=71596\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichfalcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9 Collection",
    "scale": "1:110",
    "parts": "?",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$6.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41953/MuscoviteSandwich/ultimate-falcon-9-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>4 configurations available. <a href=\"#MOCmuscovitesandwichfalconlegs\">Only legs here</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichfalconlegs",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9 landing legs for cylinder bricks",
    "scale": "1:110",
    "parts": "?",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/913053265766592/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"#MOCmuscovitesandwichfalcon9\">Legs used in this</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154spacexfalcondragon",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Block 5 with Crew Dragon",
    "scale": "1:83 - 1:92",
    "parts": "938",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$13",
    "links": "<p><a href=\"https://www.etsy.com/listing/803550227/custom-lego-spacex-falcon-9-block-5-with\">Etsy</a> </p>",
    "notes": "<p>Stickers are on <a href=\"https://www.etsy.com/listing/814408044/custom-lego-spacex-falcon-9-sticker\">Etsy for $16</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnerozfalcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9 Block 5",
    "scale": "1:110",
    "parts": "?",
    "designer": "neroz",
    "file_types": [
      "pdf"
    ],
    "price": "$4.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41438/neroz/falcon-9/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCnerozdragon1\">Dragon 1</a> and <a href=\"#MOCnerozcrewdragon\">Crew dragon</a>. Sold together.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerfalcon9fs",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 First Stage",
    "scale": "1:110",
    "parts": "133",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55466/SpaceXplorer%2016/1110-scale-spacex-falcon-9-first-stage/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerfalcon9ss",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon 9 Second Stage",
    "scale": "1:110",
    "parts": "30",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55471/SpaceXplorer%2016/1110-scale-spacex-falcon-9-second-stage/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCandydurhamfalconcrewdemo2",
    "types": [
      "rocket"
    ],
    "name": "NASA SpaceX Crew Demo 2 (Falcon 9 with Crew Dragon)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Andy Durham",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/841321986273054/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/831212550617331/\">Facebook post with description</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/854582421613677/\">Sticker sheet</a>. Based on MOCs by <a href=\"#MOCeifflemanfalconheavy\">Eiffleman</a>, <a href=\"#MOClegorocketsfalcon9\">LegoRocket</a> and Clinton Freeman.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegobuilder1234falcon9",
    "types": [
      "rocket"
    ],
    "name": "Falcon 9",
    "scale": "?",
    "parts": "132",
    "designer": "LegoBuilder1234",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-34181/Legobuilder1234/falcon-9/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy",
    "scale": "1:110",
    "parts": "330",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/space-x-falcon-heavy/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/80b37347-6083-4878-978f-fbf7d962a42b\">Lego Ideas</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/447621285643128/\">Facebook post wit digital file</a>. Rocket, two payloads, 48x48 stud landing pad. Review on Rebrickable: <a href=\"https://rebrickable.com/blog/174/how-to-build-a-lego-spacex-falcon-heavy-rocket-part-1/\">part 1</a>, <a href=\"https://rebrickable.com/blog/175/how-to-build-a-lego-spacex-falcon-heavy-rocket-part-2/\">part 2</a>. Copied to <a href=\"https://www.mecabricks.com/en/models/1w2rgm3pj8W\">Mecabricks</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCXfalconheavynopayload",
    "types": [
      "rocket"
    ],
    "name": "Falcon Heavy (no payload)",
    "scale": "1:110",
    "parts": "356",
    "designer": "?",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCXfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "Falcon Heavy with Tesla and Starman",
    "scale": "1:110",
    "parts": "226",
    "designer": "?",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcurtisdffalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy Rocket",
    "scale": "1:110",
    "parts": "458",
    "designer": "curtisdf",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=32764\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCcurtisdffalconheavylaunchpad\">launchpad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCyourihamonspacexfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy",
    "scale": "1:110",
    "parts": "?",
    "designer": "Youri Hamon",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/829880754083844/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/829880984083821/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/820751064996813/\">Facebook post with stickers</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/820520938353159/\">Facebook post with discussion</a>. Based on <a href=\"#MOCeifflemanfalconheavy\">MOC by Eiffleman</a> and <a href=\"#MOCendregaspacexfalconheavy\">MOC by endrega</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "Falcon Heavy",
    "scale": "1:110",
    "parts": "1789",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$9.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55765/MuscoviteSandwich/falcon-heavy-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154spacexfalconheavy",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Falcon Heavy Block 5 with Tesla Roadster",
    "scale": "1:83 - 1:92",
    "parts": "?",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$17",
    "links": "<p><a href=\"https://www.etsy.com/listing/822693335/custom-lego-spacex-falcon-heavy-block-5\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOChankscorpio83falconheavyonpad",
    "types": [
      "rocket"
    ],
    "name": "Falcon Heavy on the Pad",
    "scale": "?",
    "parts": "?",
    "designer": "hankscorpio83",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.reddit.com/r/legoRockets/comments/f1gx4r/falcon_heavy_on_the_pad/\">Reddit</a> </p>",
    "notes": "<p>File with instructions on <a href=\"https://docdro.id/Vr9efq8\">docdroid</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegobuilder1234falconheavy",
    "types": [
      "rocket"
    ],
    "name": "mini Falcon Heavy",
    "scale": "?",
    "parts": "99",
    "designer": "LegoBuilder1234",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-32864/Legobuilder1234/mini-falcon-heavy/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsstarhopper",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Starhopper",
    "scale": "1:110",
    "parts": "367",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$2.50",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47910/MOCrockets/spacex-starhopper-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarhopper",
    "types": [
      "rocket"
    ],
    "name": "Starhopper",
    "scale": "1:110",
    "parts": "554",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starhopper/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsspacexbfrtiny",
    "types": [
      "rocket"
    ],
    "name": "SpaceX tiny BFR",
    "scale": "1:571",
    "parts": "46",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-24351/MOCrockets/spacex-tiny-bfr/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsspacexbfr",
    "types": [
      "rocket"
    ],
    "name": "SpaceX BFR 2018",
    "scale": "1:110",
    "parts": "1714",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$8.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-22656/MOCrockets/spacex-bfr-2018-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerminibfr",
    "types": [
      "rocket"
    ],
    "name": "Mini SpaceX BFR (2018)",
    "scale": "?",
    "parts": "307",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-54146/SpaceXplorer%2016/mini-spacex-bfr-2018/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerlunarstarship",
    "types": [
      "rocket"
    ],
    "name": "Mini SpaceX Lunar Starship",
    "scale": "?",
    "parts": "143",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-58995/SpaceXplorer%2016/mini-spacex-lunar-starship/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipmk1",
    "types": [
      "rocket"
    ],
    "name": "Starship MK1",
    "scale": "1:110",
    "parts": "826",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-mk1/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn1",
    "types": [
      "rocket"
    ],
    "name": "Starship SN1",
    "scale": "1:110",
    "parts": "786",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn1/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn2",
    "types": [
      "rocket"
    ],
    "name": "Starship SN2",
    "scale": "1:110",
    "parts": "187",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn2/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn3",
    "types": [
      "rocket"
    ],
    "name": "Starship SN3",
    "scale": "1:110",
    "parts": "781",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn3/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn4",
    "types": [
      "rocket"
    ],
    "name": "Starship SN4",
    "scale": "1:110",
    "parts": "809",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn4/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn5",
    "types": [
      "rocket"
    ],
    "name": "Starship SN5",
    "scale": "1:110",
    "parts": "805",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn5/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn6",
    "types": [
      "rocket"
    ],
    "name": "Starship SN6",
    "scale": "1:110",
    "parts": "815",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn6/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn7",
    "types": [
      "rocket"
    ],
    "name": "Starship SN7",
    "scale": "1:110",
    "parts": "199",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn7/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn71",
    "types": [
      "rocket"
    ],
    "name": "Starship SN7.1",
    "scale": "1:110",
    "parts": "325",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn71/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarstarshipsn8",
    "types": [
      "rocket"
    ],
    "name": "Starship SN8",
    "scale": "1:110",
    "parts": "1193",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/spacex/starship-sn8/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickrejects74spacexmarsshuttleandrover",
    "types": [
      "rocket"
    ],
    "name": "Space X Mars Mission Main Shuttle and Rover and booster",
    "scale": "?",
    "parts": "3063",
    "designer": "BrickRejects74",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=55027\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovespacexdragon",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon",
    "scale": "?",
    "parts": "817",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "$10.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-4573/Perijove/spacex-dragon/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrepragedragoncapsule",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon Capsule",
    "scale": "1:124",
    "parts": "33",
    "designer": "RepRage",
    "file_types": [
      "lxf",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://reprage.com/post/lego-spacex-dragon\">Home page</a>, <a href=\"https://ideas.lego.com/projects/ba33d753-7be1-4900-99b7-b9522f3dc822\">Lego Ideas</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCrepragefalcon9\">Falcon 9</a>, <a href=\"#MOCrepragedroneship\">Droneship</a>, <a href=\"#MOCrepragedragonv2capsule\">Dragon 2</a>, <a href=\"#MOCreprageorbcomm2payload\">Orbcomm payload</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrepragedragonv2capsule",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon V2 Capsule",
    "scale": "1:124",
    "parts": "74",
    "designer": "RepRage",
    "file_types": [
      "lxf",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://reprage.com/post/lego-spacex-dragon-V2\">Home page</a>, <a href=\"https://ideas.lego.com/projects/ba33d753-7be1-4900-99b7-b9522f3dc822\">Lego Ideas</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCrepragefalcon9\">Falcon 9</a>, <a href=\"#MOCrepragedroneship\">Droneship</a>, <a href=\"#MOCrepragedragoncapsule\">Dragon</a>, <a href=\"#MOCreprageorbcomm2payload\">Orbcomm payload</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdoctordoomsterspacexdragonv2",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon V2",
    "scale": "?",
    "parts": "?",
    "designer": "Doctor Doomster",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.moc-pages.com/moc.php/389498\">MOCpages</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichspacexdragonv2",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon 2",
    "scale": "1:110",
    "parts": "86",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-39080/MuscoviteSandwich/spacex-dragon-2-capsule-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsilentdawn03dragonxl",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon XL",
    "scale": "1:110",
    "parts": "125",
    "designer": "silentdawn03",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-39816/silentdawn03/spacex-dragon-xl-1110-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnerozdragon1",
    "types": [
      "rocket"
    ],
    "name": "Dragon 1",
    "scale": "1:110",
    "parts": "?",
    "designer": "neroz",
    "file_types": [
      "pdf"
    ],
    "price": "$4.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41438/neroz/falcon-9/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCnerozcrewdragon\">Crew dragon</a> and <a href=\"#MOCnerozfalcon9\">Falcon 9</a>. Sold together.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnerozcrewdragon",
    "types": [
      "rocket"
    ],
    "name": "Crew Dragon",
    "scale": "1:110",
    "parts": "?",
    "designer": "neroz",
    "file_types": [
      "pdf"
    ],
    "price": "$4.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41438/neroz/falcon-9/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCnerozdragon1\">Dragon 1</a> <a href=\"#MOCnerozfalcon9\">Falcon 9</a>. Sold together.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerdragons",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Dragon Duo",
    "scale": "1:110",
    "parts": "104",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56635/SpaceXplorer%2016/1110-scale-spacex-dragon-duo/\">Rebrickable</a> </p>",
    "notes": "<p>Contains Dragon 1 and Dragon 2. With stickers.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichcrewdragonrecovery",
    "types": [
      "rocket"
    ],
    "name": "SpaceX Crew Dragon recovery",
    "scale": "1:110",
    "parts": "117",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-49132/MuscoviteSandwich/spacex-crew-dragon-recovery-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorercargodragon2",
    "types": [
      "rocket"
    ],
    "name": "Mini SpaceX Cargo Dragon 2",
    "scale": "?",
    "parts": "46",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-58750/SpaceXplorer%2016/mini-spacex-cargo-dragon-2/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbenjaminunisscoutx1",
    "types": [
      "rocket"
    ],
    "name": "Scout X-1",
    "scale": "1:110",
    "parts": "25",
    "designer": "Benjamin Unis",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsgeminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan",
    "scale": "1:110",
    "parts": "69",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$1.11",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-17263/MOCrockets/gemini-titan-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedgeminitianalternative",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan alternative Capsule build",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Alternative build of <a href=\"#MOCeifflemangeminititan\">Gemini Titan II</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdestroyer56geminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan Rocket",
    "scale": "?",
    "parts": "26",
    "designer": "Destroyer56",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/X1k2qkbEvE0\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsgeminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan",
    "scale": "1:110",
    "parts": "97",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=93244\">Bricklink</a> </p>",
    "notes": "<p>Model version 3.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemangeminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan II",
    "scale": "1:110",
    "parts": "103",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/titan-ii-glv/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Also part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Alternative <a href=\"#MOCluxordeathbedgeminitianalternative\">Capsule build</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/749953552076565/\">Facebook post with parts for digital build, io</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/749286652143255/\">Facebook post with parts for digital build, lxf</a>. <a href=\"#MOCjoeldenninggeminititanmod\">Engine modification</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjoeldenninggeminititanmod",
    "types": [
      "rocket"
    ],
    "name": "Gemini-Titan Engine Modification",
    "scale": "1:110",
    "parts": "?",
    "designer": "Joel Denning",
    "file_types": [
      "io",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/836659686739284/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/836651396740113\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCeifflemangeminititan\">Eiffleman's Gemini Titan II</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichgeminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan II",
    "scale": "1:110",
    "parts": "234",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-34453/MuscoviteSandwich/gemini-titan-rocket-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154geminititan",
    "types": [
      "rocket"
    ],
    "name": "Gemini Titan",
    "scale": "1:110",
    "parts": "?",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$13",
    "links": "<p><a href=\"https://www.etsy.com/listing/819519577/custom-lego-nasa-rockets-saturn-1b\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCevankorenmicrogeminititan",
    "types": [
      "rocket"
    ],
    "name": "Tiny micro Gemini Titan rocket",
    "scale": "?",
    "parts": "13",
    "designer": "Evan Koren",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/927548844317034/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Parts list is in the discussion.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangtitaniiglv",
    "types": [
      "rocket"
    ],
    "name": "Titan II GLV with Gemini capsule, staging",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang, bedrockblaze135, Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/gemini-titan-ii/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/816398612098725/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Two second stage options for GT-1 and GT-3. Gemini spacecraft is a design by u/bedrockblaze135 from reddit. Version 3.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCgillesnawrockititaniiglvpad",
    "types": [
      "rocket"
    ],
    "name": "Titan II GLV and launch pad",
    "scale": "1:110",
    "parts": "?",
    "designer": "Gilles Nawrocki",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/843548652717054/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Based on <a href=\"#MOCkevinhuangtitaniiglv\">Titan II GLV</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorertitanglv",
    "types": [
      "rocket"
    ],
    "name": "Titan GLV",
    "scale": "1:110",
    "parts": "264",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-57842/SpaceXplorer%2016/1110-scale-titan-glv/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemantitaniiic",
    "types": [
      "rocket"
    ],
    "name": "Titan IIIC",
    "scale": "1:110",
    "parts": "343",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/titan-iii-w-x20/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">NASA: Mercury and Gemini</a>. Two payloads: 1, Manned Orbital Laboratory (MOL) with Gemini B; 2, Boeing X-20 Dyna-Soar</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangtitaniiic",
    "types": [
      "rocket"
    ],
    "name": "Titan IIIC",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang, Sebastian Schoen",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/914710142267571/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Only rocket, without payload or aerodynamic shield.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedtitanivaisrb",
    "types": [
      "rocket"
    ],
    "name": "Titan IVA ISRB",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedtitanivb",
    "types": [
      "rocket"
    ],
    "name": "Titan IVB",
    "scale": "1:110",
    "parts": "231",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/titan-iv/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible probe <a href=\"#MOCluxordeathbedcassinihuygens\">Cassini-Huygends</a>. Compatible <a href=\"#MOCluxordeathbedlaunchpad41\">launch pad 41</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedtitaniiie",
    "types": [
      "rocket"
    ],
    "name": "Titan IIIE Launch Vehicle",
    "scale": "1:110",
    "parts": "205",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/titan-iii-e/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page. Compatible <a href=\"#MOCluxordeathbedlaunchpad41\">Launch Pad 41</a>. Compatible <a href=\"#MOCluxordeathbedvoyager\">Voyager</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaxustitaniiie",
    "types": [
      "rocket"
    ],
    "name": "Titan-IIIE with Voyager",
    "scale": "1:110",
    "parts": "309",
    "designer": "Andor Schindler",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://shared.muportal.hu/lsa/\">Lego Space Agency</a> </p>",
    "notes": "<p><a href=\"https://www.reddit.com/r/lego/comments/6gwswr/most_of_my_rockets_and_spacecrafts/\">Picture of whole Andor Schindler's collection.</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncraintitaniiie",
    "types": [
      "rocket"
    ],
    "name": "Titan IIIE",
    "scale": "1:110",
    "parts": "909 with payloads",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903739203364665/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903735103365075/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With launchpad. Compatible <a href=\"#MOCbrandoncraincentaur\">Centaur stage</a>, <a href=\"#MOCbrandoncrainvikinglander\">Viking lander</a>, <a href=\"#MOCbrandoncrainvikingorbiter\">Viking orbiter</a>, <a href=\"#MOCbrandoncrainvoyagerspaceprobe\">Voyager space probe</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40ktitanIIIm",
    "types": [
      "rocket"
    ],
    "name": "Titan III M with Apollo CSM",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/768676590204261/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveautitaniiib",
    "types": [
      "rocket"
    ],
    "name": "Titan III B",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/nasa/titan-iiib/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/905451666526752/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914685615062/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCendregatitan34d",
    "types": [
      "rocket"
    ],
    "name": "Titan 34D",
    "scale": "1:110",
    "parts": "279",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=108967\">Bricklink</a> </p>",
    "notes": "<p>Model contains two UA1206 SRBs as well as two LR-87-11 main engines in the first stage.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaupegasus",
    "types": [
      "rocket"
    ],
    "name": "Pegasus",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/nasa/pegasus/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912968752441710/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914425615088/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanmercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury-Atlas",
    "scale": "1:110",
    "parts": "82",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=3\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Alternative build with <a href=\"#MOCeifflemanagenad\">Agena D payload</a>. Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsmecuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury Atlas",
    "scale": "1:110",
    "parts": "39",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$1.11",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-17342/MOCrockets/mercury-atlas-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCmocrocketsmecuryatlaslaunchpad\">Mercury Atlas launchpad and tower</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCXmercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury-Atlas",
    "scale": "1:110",
    "parts": "69",
    "designer": "?",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsmercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury Atlas",
    "scale": "1:110",
    "parts": "176",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=93698\">Bricklink</a> </p>",
    "notes": "<p>Model version 4.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCgbdannymercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury Atlas rocket",
    "scale": "1:110",
    "parts": "176",
    "designer": "GBDanny",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=33004\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdestroyer56mercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury Atlas Rocket",
    "scale": "?",
    "parts": "20",
    "designer": "Destroyer56",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/qeVaPn4DjzB\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichmercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury-Atlas",
    "scale": "1:110",
    "parts": "165",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36530/MuscoviteSandwich/mercury-atlas-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154mercuryatlas",
    "types": [
      "rocket"
    ],
    "name": "Mercury Atlas",
    "scale": "1:110",
    "parts": "?",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$13",
    "links": "<p><a href=\"https://www.etsy.com/listing/819519577/custom-lego-nasa-rockets-saturn-1b\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangatlaslv3b",
    "types": [
      "rocket"
    ],
    "name": "Atlas LV3B with Mercury, staging",
    "scale": "1:110",
    "parts": "181",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/mercury-atlas-lv-3b/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761723120899608/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761632490908671/\">Facebook post with description</a>. Modification into <a href=\"#MOCkevinhuangbigjoeatlaslv3b\">Big Joe</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangbigjoeatlaslv3b",
    "types": [
      "rocket"
    ],
    "name": "Big Joe 1 \u2013 Atlas LV-3B",
    "scale": "1:110",
    "parts": "180",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/big-joe-atlas-lv-3b/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCkevinhuangatlaslv3b\">Atlas LV3-B</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCflorenttodeschiniatlasmercurylaunchpad",
    "types": [
      "rocket"
    ],
    "name": "Mercury Friendship 7 - Atlas with launchpad",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florent Todeschini",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/809024516169468/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainatlasagenadmercury",
    "types": [
      "rocket"
    ],
    "name": "Atlas Agena D + Mercury Capsule",
    "scale": "1:110",
    "parts": "222",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/834139863657933/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/834140256991227/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/821640168241236\">Facebook post with pictures</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwolframbrosziesredstone",
    "types": [
      "rocket"
    ],
    "name": "Redstone",
    "scale": "1:110",
    "parts": "24",
    "designer": "Wolfram Broszies",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnathanrmercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury-Redstone",
    "scale": "1:110",
    "parts": "72",
    "designer": "Nathan Readioff",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricksafe.com/pages/NathanR/real-space/mercury-redstone-110scale\">Bricksafe</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/151472-mercury-redstone-freedom-7/\">Eurobricks</a>, <a href=\"https://www.mecabricks.com/en/models/Z79a8PMba8w\">Mecabricks</a> </p>",
    "notes": "<p>Redstone with launchpad, separate Mercury capsule with own stand.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanymercuryprogramme",
    "types": [
      "rocket"
    ],
    "name": "Mercury Programm Minature Models",
    "scale": "?",
    "parts": "356",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$2.50",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-21725/Brickbrothers-Germany/mercury-programm-minature-models/\">Rebrickable</a>~~ </p>",
    "notes": "<p>MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCmocrocketsmercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone with Launcher",
    "scale": "1:110",
    "parts": "51",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$1.10",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-17248/MOCrockets/mercury-redstone-with-launcher-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanmercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone",
    "scale": "1:110",
    "parts": "29",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCeifflemanmercuryredstonelaunchset\">launch set</a>. Also part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsmercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone",
    "scale": "1:110",
    "parts": "51",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=40167\">Bricklink</a> </p>",
    "notes": "<p>Model version 3. Compatible <a href=\"#MOClegorocketsmercuryredstonestand\">stand</a>. Compatible <a href=\"#MOCsmokinjoe79redstonelaunchpad\">launch pad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154mercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone",
    "scale": "1:110",
    "parts": "?",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$13",
    "links": "<p><a href=\"https://www.etsy.com/listing/819519577/custom-lego-nasa-rockets-saturn-1b\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwren18redstonerocket",
    "types": [
      "rocket"
    ],
    "name": "Redstone Rocket",
    "scale": "1:55",
    "parts": "169",
    "designer": "wren18",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=92165\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCgbdannymercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mini Mercury Redstone",
    "scale": "?",
    "parts": "84",
    "designer": "GBDanny",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=16614\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcolinisadefaultmercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone Rocket",
    "scale": "?",
    "parts": "410",
    "designer": "Colin-\\_Is_A_Default",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/VzvMQQ4yvJp\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdestroyer56mercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury Redstone Rocket",
    "scale": "?",
    "parts": "28",
    "designer": "Destroyer56",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/Qb82x3Xpa1z\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegobuilder1234mercuryredstone",
    "types": [
      "rocket"
    ],
    "name": "Mercury-Redstone mini",
    "scale": "?",
    "parts": "37",
    "designer": "LegoBuilder1234",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-32897/Legobuilder1234/mercury-redstone-mini/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzermercurygemini",
    "types": [
      "rocket"
    ],
    "name": "Mercury / Gemini",
    "scale": "?",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/pyj6VlV1jRq\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauminotaur4",
    "types": [
      "rocket"
    ],
    "name": "Minotaur 4",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/nasa/minotaur-iv/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914225615108/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/920671928338059/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCfreakcubeapollo",
    "types": [
      "rocket"
    ],
    "name": "Apollo Spacecraft",
    "scale": "?",
    "parts": "7090",
    "designer": "FreakCube",
    "file_types": [
      "pdf"
    ],
    "price": "$30.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-26457/FreakCube/apollo-spacecraft/#bi\">Rebrickable</a> </p>",
    "notes": "<p>Only <a href=\"https://rebrickable.com/mocs/MOC-26457/FreakCube/apollo-spacecraft/\">Command and service module</a>. Only <a href=\"https://rebrickable.com/mocs/MOC-29829/FreakCube/apollo-lunar-module/\">Lunar lander</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaabfanapollo11",
    "types": [
      "rocket"
    ],
    "name": "Apollo 11",
    "scale": "?",
    "parts": "922",
    "designer": "saabfan",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/nKZvmb1QaG6\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanapolloheatshield",
    "types": [
      "rocket"
    ],
    "name": "Apollo capsule with heatshield",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a> </p>",
    "notes": "<p>Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Some stickers also in <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas NASA: Mercury and Gemini</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanapollo13",
    "types": [
      "rocket"
    ],
    "name": "Apollo 13",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/7d001367-12e0-473e-aee5-e6af83e4e6a6/updates?project_updates_page=1\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemanskylab\">NASA: Skylab</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketscsmimproved",
    "types": [
      "rocket"
    ],
    "name": "Improved Apollo Command-and-Service Module (CSM)",
    "scale": "1:110",
    "parts": "13",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=38891\">Bricklink</a> </p>",
    "notes": "<p>Redesign of <a href=\"#SET21309-1\">SET21309-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCserenityapollo11csm",
    "types": [
      "rocket"
    ],
    "name": "NASA Apollo 11 CSM - improved command module",
    "scale": "?",
    "parts": "914",
    "designer": "Serenity",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-40080/Serenity/nasa-apollo-11-csm-improved-command-module/\">Rebrickable</a> </p>",
    "notes": "<p>Version 2. <a href=\"https://rebrickable.com/mocs/MOC-38232/Serenity/nasa-apollo-11-command-and-service-module-csm/\">First version</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwells44saturn1series",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1 series",
    "scale": "1:110",
    "parts": "641 (SA-1)",
    "designer": "David Welling",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a>, <a href=\"https://bricksin.space/launch-vehicles/saturn-1-block-1/\">Brick in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/457672647971325/\">Facebook post with instructions</a>. Contains description and models of Saturn 1 SA-1 to SA-10 and <a href=\"#MOCdavidwells44pegasus\">Pegasus satelite</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwells44saturn1bseries",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B series",
    "scale": "1:110",
    "parts": "?",
    "designer": "David Welling",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/473252666413323/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCeifflemansaturn1b\">Eifflemans</a> and <a href=\"#MOCnathanrsaturn1b\">Nathan Readioff's</a> Saturn 1B. Contains description and models of SA-205 Apollo 7 and SA-206+ for Skylab 1-3. Digital files for <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/787227231682530/\">SA-205</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/787227518349168/\">SA-206+</a> by Johan Broman. Compatible <a href=\"#MOCdavidwellingmilkstool\">Milkstool</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketssaturnib",
    "types": [
      "rocket"
    ],
    "name": "Saturn IB booster",
    "scale": "1:110",
    "parts": "472",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "3.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-18166/MOCrockets/saturn-ib-booster-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Only the booster, rest is used from <a href=\"#SET21309-1\">LEGO set 21309-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemansaturn1b",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B",
    "scale": "1:110",
    "parts": "338",
    "designer": "Eiffleman",
    "file_types": [
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/saturn-1b/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/7d001367-12e0-473e-aee5-e6af83e4e6a6/updates?project_updates_page=3\">Lego Ideas</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemanskylab\">NASA: Skylab</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnathanrsaturn1b",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B",
    "scale": "1:110",
    "parts": "902",
    "designer": "Nathan Readioff",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricksafe.com/pages/NathanR/real-space/saturn-1b\">Bricksafe</a>, <a href=\"https://www.mecabricks.com/en/models/Z1Xjd8NZj8b\">Mecabricks</a> </p>",
    "notes": "<p>Influenced by <a href=\"#MOCeifflemansaturn1b\">Eiffleman's Saturn 1B</a> and <a href=\"#MOCsaxussaturn1b\">Andor Schindler's Saturn 1B</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaxussaturn1b",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B",
    "scale": "1:110",
    "parts": "576",
    "designer": "Andor Schindler",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://shared.muportal.hu/lsa/\">Lego Space Agency</a> </p>",
    "notes": "<p><a href=\"https://www.reddit.com/r/lego/comments/6gwswr/most_of_my_rockets_and_spacecrafts/\">Picture of whole Andor Schindler's collection.</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketssaturn1B",
    "types": [
      "rocket"
    ],
    "name": "Saturn IB",
    "scale": "1:110",
    "parts": "799",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=92636\">Bricklink</a> </p>",
    "notes": "<p>Model version 5.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmaverick9154saturn1b",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B",
    "scale": "1:110",
    "parts": "?",
    "designer": "Maverick9154",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$13",
    "links": "<p><a href=\"https://www.etsy.com/listing/819519577/custom-lego-nasa-rockets-saturn-1b\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainsaturn1bwplatform",
    "types": [
      "rocket"
    ],
    "name": "Saturn 1B Booster & Launch Platform",
    "scale": "1:110",
    "parts": "936",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/899685133770072/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/899684060436846/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible with the S-IVB upper stage of <a href=\"#SET21309-1\">SET21309-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCXsaturniisaturnivbinterstage",
    "types": [
      "rocket"
    ],
    "name": "Saturn II Saturn IVB interstage",
    "scale": "1:110",
    "parts": "195",
    "designer": "?",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangsaturnivb",
    "types": [
      "rocket"
    ],
    "name": "Saturn IVB variants",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/857928597945726/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>This is a preliminary prototype! Several modifications of <a href=\"#SET21309-1\">SET21309-1</a> with added details, proper length, patterns, etc.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildesaturnv",
    "types": [
      "rocket"
    ],
    "name": "Modification of Saturn V with improved accuracy",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/765903217148265/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Many modifications of the <a href=\"#SET21309-1\">set Saturn V</a> with improved accuracy. It's good for Apollo 10, 11, 12, 13 and 14.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbillyamysaturnvhinges",
    "types": [
      "rocket"
    ],
    "name": "Modification of Saturn V - hinging action to the third stage",
    "scale": "1:110",
    "parts": "?",
    "designer": "Billyamy,  David Welling",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/151325-a-small-modification-of-saturn-v/&amp;tab=comments#comment-2943381\">Eurobricks</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/399401547131769/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET21309-1\">set Saturn V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnicodaamsmodsaturnheatshield",
    "types": [
      "rocket"
    ],
    "name": "Modification of Saturn V - bottom heat shield",
    "scale": "1:110",
    "parts": "16",
    "designer": "Nico Daams",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/711081425963778/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Facebook post also contains description of other usefull color changes. Modification of <a href=\"#SET21309-1\">set Saturn V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCgregkremersaturnvlh2feed",
    "types": [
      "rocket"
    ],
    "name": "Modification of Saturn V - LH2 feed",
    "scale": "1:110",
    "parts": "204",
    "designer": "Greg Kremer",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/815100318895221/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcommunitysaturnvmod",
    "types": [
      "rocket"
    ],
    "name": "21309 NASA Saturn-V with community mods",
    "scale": "1:110",
    "parts": "177",
    "designer": "Nico Daams, Greg Kremer, David Welling, Adam Wilde. Compiled by Greg Kremer",
    "file_types": [
      "io",
      "bricklink xml"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/906947749710477/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/906951516376767/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET21309-1\">Saturn V</a>. Various modifications put together.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwhatsuptodaysaturnvmobilelauncher",
    "types": [
      "rocket"
    ],
    "name": "Saturn-V and mobile launcher",
    "scale": "1:600",
    "parts": "385",
    "designer": "Whatsuptoday",
    "file_types": [
      "png"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/eb8af701-47d5-47fb-b252-f77fca15e503\">Lego Ideas</a> </p>",
    "notes": "<p><a href=\"http://whatsuptoday.free.fr/Saturn-V_Mobile_launcher/eBooklet/English/Saturn-V_Mobile_Launcher.html\">The booklet about project</a>. <a href=\"http://whatsuptoday.free.fr/Downloads/final-update-v2--Saturn-V-Mobile-Launcher.rar\">The instructions</a> are in updates of Lego Ideas page, or when booklet is listed to the end, or in updates of other Lego Ideas pages of the same author.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanysaturn5launchpad",
    "types": [
      "rocket"
    ],
    "name": "Saturn V Launchpad and Crawler Miniature Scale",
    "scale": "?",
    "parts": "620",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-21741/Brickbrothers-Germany/saturn-v-launchpad-and-crawler-miniature-scale/\">Rebrickable</a>~~ </p>",
    "notes": "<p>MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCclear08saturnvrocket",
    "types": [
      "rocket"
    ],
    "name": "Saturn V Rocket",
    "scale": "?",
    "parts": "22",
    "designer": "mnemon",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/nKZvmLBLjG6\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichmidisaturnv",
    "types": [
      "rocket"
    ],
    "name": "Midi Saturn V",
    "scale": "?",
    "parts": "448",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-34558/MuscoviteSandwich/midi-scale-saturn-v/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCworriz107saturnv",
    "types": [
      "rocket"
    ],
    "name": "Saturn V mini version",
    "scale": "?",
    "parts": "10",
    "designer": "worriz107",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/a5a6de8d-172a-47c1-8345-a36c61781066\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegoswithgabesaturnvmini",
    "types": [
      "rocket"
    ],
    "name": "Saturn V Mini",
    "scale": "?",
    "parts": "18",
    "designer": "LegosWithGabe",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/c2c5cba5-b3c7-4ddf-af7b-98e75410d8e5#&amp;gid=1&amp;pid=1\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCfsleinadsaturnv",
    "types": [
      "rocket"
    ],
    "name": "Saturn V",
    "scale": "?",
    "parts": "13",
    "designer": "FSLeinad",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/54b3ecbd-0a28-415f-befd-6a64ab42b219\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeja738minisaturnv",
    "types": [
      "rocket"
    ],
    "name": "Mini Saturn V",
    "scale": "?",
    "parts": "38",
    "designer": "EJA738",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/ce969eaa-a460-4114-b1e1-7999da51d954\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCyoungestgraciousdudemicrosaturnv",
    "types": [
      "rocket"
    ],
    "name": "Micro Saturn V",
    "scale": "?",
    "parts": "17",
    "designer": "YoungestGraciousDude",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/a00f8687-4f92-4c21-adfe-e5cb327bd255\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbatman123123123beyondnanosaturnv",
    "types": [
      "rocket"
    ],
    "name": "Beyond Nano Saturn V",
    "scale": "?",
    "parts": "11",
    "designer": "Batman123123123",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/08c15ad0-d8c7-435d-a391-bc1499d861e7\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmattzelegobuilderminisaturnv",
    "types": [
      "rocket"
    ],
    "name": "Mini Saturn V",
    "scale": "?",
    "parts": "9",
    "designer": "Mattzelegobuilder",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/250a2d3e-e49c-40cc-a830-456f54c3b9b3\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkiwilegofanmicroscalesaturnv",
    "types": [
      "rocket"
    ],
    "name": "Microscale Saturn V",
    "scale": "?",
    "parts": "30",
    "designer": "KiwiLegoFan",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/activities/3b7649f6-4a30-4bb6-89b9-82a016091ca6/response/f63add00-4d3e-490c-ba31-0aa7a101bf26\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrubanybinetchrissaturnv35cm",
    "types": [
      "rocket"
    ],
    "name": "Saturn V",
    "scale": "?",
    "parts": "?",
    "designer": "Rubany-binet Chris",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/909030509502201/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/909027832835802/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickprojectmicroapollo",
    "types": [
      "rocket"
    ],
    "name": "Micro Apollo",
    "scale": "?",
    "parts": "27",
    "designer": "Brickproject",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=51529\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcecebuildsapollo11",
    "types": [
      "rocket"
    ],
    "name": "Apollo 11",
    "scale": "?",
    "parts": "196",
    "designer": "CeceBuilds",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/LVyvnOlpal3\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCXsaturnivbcsm",
    "types": [
      "rocket"
    ],
    "name": "Saturn IVB CSM",
    "scale": "1:110",
    "parts": "258",
    "designer": "?",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemansaturnskylab",
    "types": [
      "rocket"
    ],
    "name": "Saturn V for Skylab mission",
    "scale": "1:110",
    "parts": "8",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38/updates?project_updates_page=3\">Lego Ideas</a> </p>",
    "notes": "<p>Description of conversion of <a href=\"#SET21309-1\">NASA Apollo Saturn V set</a> into version used for Skylab missions. Description is in updates of Lego Ideas page for <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo4saturnV",
    "types": [
      "rocket"
    ],
    "name": "Saturn V with Apollo 4",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/661405730931348/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Block I Service Module, LTA-2R, eight ullage rockets on S-II Aft Interstage. Other mods to <a href=\"#SET21309\">Saturn V</a> (S-IVB, roll pattern, S-II thrust structure, etc.)</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo6saturnV",
    "types": [
      "rocket"
    ],
    "name": "Saturn V with Apollo 6",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/661407714264483/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>White Block I Service Module, LTA-10R, eight ullage rocket fairings on S-II Aft Interstage. Other mods to <a href=\"#SET21309\">Saturn V</a> (S-IVB, roll pattern, S-II thrust structure, etc.)</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo8saturnV",
    "types": [
      "rocket"
    ],
    "name": "Saturn V with Apollo 8",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/662189500852971/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With LTA-B. Includes other mods to <a href=\"#SET21309\">Saturn V</a> (e.g. Block II SM radiators, S-IVB improvements, roll pattern, S-II thrust structure, S-II Aft Interstage ullage rockets, interstage internals, corrugations, etc.). (Corrected version with improved stability of LTA-B mounting on top of the S-IVB.)</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjosephschamberssaturnmlv",
    "types": [
      "rocket"
    ],
    "name": "Saturn V MLV Launch Vehicle Variants",
    "scale": "1:110",
    "parts": "?",
    "designer": "Joseph Chambers",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1J6qvzptlAaR6nPAmfOVA99WgkKvdhQPU?fbclid=IwAR3hIxsU_XGDkX8dJTy1gkR_PjpyA0AFlJV6T7gvkr3Xs3gvyR0cQps0IYU\">Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/945663225838929/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/945662629172322/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsskylab",
    "types": [
      "rocket"
    ],
    "name": "Skylab",
    "scale": "1:110",
    "parts": "2014",
    "designer": "legorockets, rab36, GBDanny",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=41105\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannspaceshipone",
    "types": [
      "rocket"
    ],
    "name": "Space Ship One",
    "scale": "1:110",
    "parts": "30",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/795318167540103/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/795316750873578/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichvirgingalactictwo",
    "types": [
      "rocket"
    ],
    "name": "Virgin Galactic Spaceship Two VSS Unity",
    "scale": "1:110",
    "parts": "77",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-49920/MuscoviteSandwich/virgin-galactic-spaceship-two-vss-unity-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsimonpaulspaceshiptwo",
    "types": [
      "rocket"
    ],
    "name": "Virgin Galactic Spaceship Two",
    "scale": "1:110",
    "parts": "109",
    "designer": "Simon Paul",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/846914822380437/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwhovianspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:110",
    "parts": "833",
    "designer": "Whovian41110",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-10169/Whovian41110/space-shuttle/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCpsiborgvipshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "?",
    "parts": "32",
    "designer": "PsiborgVIP",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-19192/PsiborgVIP/space-shuttle-micro/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkingsknightspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "NASA Space Shuttle",
    "scale": "1:110",
    "parts": "1363",
    "designer": "KingsKnight",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/space-shuttle/\">Bricks in Space webpage</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/152554-moc-lego-ideas-space-shuttle-saturn-v-scale/\">Eurobricks</a>, <a href=\"http://www.moc-pages.com/moc.php/440857\">MOCpages</a>, <a href=\"http://www.moc-pages.com/moc.php/440223\">MOCpages</a> </p>",
    "notes": "<p>Also on <a href=\"https://ideas.lego.com/projects/50a447cc-0acb-4fff-b3c9-41739fed157c\">Lego Ideas</a> (not approved), instructions on <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, for proper marking selections see <a href=\"http://axmpaperspacescalemodels.com/wp-content/uploads/2018/04/Space-Shuttle-External-Markings.pdf\">this paper model</a>, for sticker sheet <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/494762367595686/\">see Facebook post</a>. Compatible <a href=\"#MOCkingsknightlaunchpad\">launch pad</a>. Modification by David Welling also on Google drive.  Compatible <a href=\"#MOCkingsknightastrovan\">Astrovan</a>. <a href=\"#MOCraymanhendrickssts1\">Modification for STS-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkingsknightspaceshuttlev2",
    "types": [
      "rocket"
    ],
    "name": "NASA Space Shuttle 40th Anniversary Edition (2020)",
    "scale": "1:110",
    "parts": "2122",
    "designer": "KingsKnight",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-46228/KingsKnight/space-shuttle-1110-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/manned-spacecraft/nasa-space-shuttle/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Improved version of <a href=\"#MOCkingsknightspaceshuttle\">NASA Space Shuttle</a>. <a href=\"https://imgur.com/a/wPCm5CF\">Photos on imgur</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/808472969557956/\">Facebook post with description</a>. <a href=\"#MOCraymanhendrickssts1v2\">Modification for STS-1</a>. <a href=\"#MOCfabianreichlshuttlestand\">Horizontal stand</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwellingspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "NASA Space Shuttle",
    "scale": "1:110",
    "parts": "?",
    "designer": "David Welling, shiva, Simon Paul",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p>Facebook group Bricks in Space. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/367551420316782/\">Orbiter</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/368240336914557/\">SRB</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/374219386316652/\">Ext. tank</a>. </p>",
    "notes": "<p>Also on <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">Goole drive</a>. Modification of <a href=\"#MOCkingsknightspaceshuttle\">Kings Knight's shuttle</a>, see also for link to stickers and marking selection. <a href=\"#MOCraymanhendrickssts1\">Modification for STS-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCraymanhendrickssts1",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle STS-1 Columbia",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rayman Hendricks",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/804198053318781/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Rework to closely match Columbia's livery for STS-1. Based on: <a href=\"#MOCkingsknightspaceshuttle\">Kingsknhigt's version 1</a>, <a href=\"#MOCdavidwellingspaceshuttle\">David Welling's, shiva's, Simon Paul's</a>, others.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCraymanhendrickssts1v2",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle STS-1 Columbia, 2020",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rayman Hendricks",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/838653996539853/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Rework to closely match Columbia's livery for STS-1. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/817092738695979/\">Facebook post with description</a>. Based on: <a href=\"#MOCkingsknightspaceshuttlev2\">Kingsknhigt's version 2</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlerspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:110",
    "parts": "?",
    "designer": "Andor Schindler",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/689505764788011/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsspaceshuttlests1",
    "types": [
      "rocket"
    ],
    "name": "NASA Space Shuttle STS-1",
    "scale": "1:125",
    "parts": "899",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$4.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47083/MOCrockets/nasa-space-shuttle-sts-1-expedition-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10231-1\">SET10231-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmaxfelkelshuttledecalsheet",
    "types": [
      "rocket"
    ],
    "name": "Improved decals for SET 10231-1",
    "scale": "?",
    "parts": "X",
    "designer": "Max Felkel",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/813282365743683/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10231-1\">Lego set 10231-1</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/813281682410418/\">Facebook post with pictures</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjamesjtspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "NASA Shuttle Discovery in Technic",
    "scale": "?",
    "parts": "700",
    "designer": "JamesJT",
    "file_types": [
      "video"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/105024-c-model-42025-nasa-shuttle-orbiter/&amp;page=3\">Eurobricks</a>, <a href=\"https://ideas.lego.com/projects/196dca05-f8ac-4bab-a4ac-a1e499841ccb\">Lego Ideas</a>, <a href=\"http://www.moc-pages.com/moc.php/417204\">MOCpages</a> </p>",
    "notes": "<p>Instructions as video on <a href=\"https://www.youtube.com/watch?v=HNVXj6jPxjg\">youtube</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaabfanmicrospaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Micro Space Shuttle",
    "scale": "?",
    "parts": "134",
    "designer": "saabfan",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/QVzvMXyNjJp\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:220",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/725455527859701/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:220",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/729469227458331/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/733368287068425/\">Facebook post with digital file</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:220",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/713686045703316/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/713253679079886/\">Facebook post with pictures</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildespaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "1:220",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/737789796626274\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>It is modification of <a href=\"#MOCeifflemanspaceshuttle\">Eiffleman's space shuttle</a>. <a href=\"https://www.dropbox.com/s/jfutrb4d9depi5p/Space Shuttle (ISS) Adam Wilde (after Eiffleman) EXPLODED VIEW.io?dl=0\">Digital file with exploded view</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/767729550298965/\">Instructions made by Johan Broman</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCarkaitzruizmodularshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle, modular",
    "scale": "1:220",
    "parts": "?",
    "designer": "Arkaitz Ruiz",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/790487288023191/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/790487131356540/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichstsexternaltank",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle tank and SRBs",
    "scale": "1:220",
    "parts": "?",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-38675/MuscoviteSandwich/1220-space-shuttle-tank-and-srbs\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Created for Shuttle from <a href=\"#SET21321-1\">LEGO SET SET21321-1</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/753820785023175/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrollingbricksspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle with SRB and External tank",
    "scale": "1:352",
    "parts": "?",
    "designer": "RollingBricks",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0, 4 Euro",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-35336/RollingBricks/space-shuttle-carrier-aircraft-and-rockets/#bi\">Rebrickable</a>, <a href=\"https://rebrickable.com/mocs/MOC-35333/RollingBricks/space-shuttle/?inventory=1#comments\">Rebrickable</a>, <a href=\"https://rebrickable.com/mocs/MOC-35335/RollingBricks/space-shuttle-for-carrier-aircraft/?inventory=1#comments\">Rebrickable</a> </p>",
    "notes": "<p>Space shuttle is free in both versions for launch and aircraft carrier. SRB with External Tank and aircraft carrier is paid.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrollingbricksaircraftandspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle with aircraft carrier",
    "scale": "1:352",
    "parts": "?",
    "designer": "RollingBricks",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0, 4 Euro",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-35336/RollingBricks/space-shuttle-carrier-aircraft-and-rockets/#bi\">Rebrickable</a>, <a href=\"https://rebrickable.com/mocs/MOC-35333/RollingBricks/space-shuttle/?inventory=1#comments\">Rebrickable</a>, <a href=\"https://rebrickable.com/mocs/MOC-35335/RollingBricks/space-shuttle-for-carrier-aircraft/?inventory=1#comments\">Rebrickable</a> </p>",
    "notes": "<p>Space shuttle is free in both versions for launch and aircraft carrier. SRB with External Tank and aircraft carrier is paid.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadryspaceshuttle",
    "types": [
      "rocket"
    ],
    "name": "Space Shuttle",
    "scale": "?",
    "parts": "126",
    "designer": "Adry",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41873/Adry/space-shuttle/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadryspaceshuttleevolution",
    "types": [
      "rocket"
    ],
    "name": "space shuttle evolution",
    "scale": "?",
    "parts": "769",
    "designer": "Adry",
    "file_types": [
      "lxf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-43010/Adry/space-shuttle-evolution/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkylesezawichseadragon",
    "types": [
      "rocket"
    ],
    "name": "Sea Dragon",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kyle Sezawich",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/764559027282684/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/764434503961803/\">Facebook post with images</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveausafir",
    "types": [
      "rocket"
    ],
    "name": "Safir",
    "scale": "1:110",
    "parts": "20",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/iran/safir-1/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912969045775014/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914498948414/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveausimorgh",
    "types": [
      "rocket"
    ],
    "name": "Simorgh",
    "scale": "1:110",
    "parts": "23",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/iran/simorgh/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912969355774983/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914598948404/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichpslv",
    "types": [
      "rocket"
    ],
    "name": "Polar Satellite Launch Vehicle with Mangalyaan",
    "scale": "1:110",
    "parts": "392",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56639/MuscoviteSandwich/pslv-with-mangalyaan-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"#MOCmuscovitesandwichmangalyaan\">With compatible Mangalyaan</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59protonur500v2",
    "types": [
      "rocket"
    ],
    "name": "Proton(UR-500) rocket V2",
    "scale": "1:110",
    "parts": "484",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/mkjA1LqBjZG\">Mecabricks</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.mecabricks.com/en/models/xGejeZqJvKX\">Older version 1</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kproton",
    "types": [
      "rocket"
    ],
    "name": "Proton",
    "scale": "1:110",
    "parts": "819",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCsunder59protonur500v2\">Proton by Sunder59</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kprotonm",
    "types": [
      "rocket"
    ],
    "name": "Proton M",
    "scale": "1:110",
    "parts": "794",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-m/\">Bricks in Space webpage</a>, <a href=\"https://rebrickable.com/mocs/MOC-39838/MoppeW40k/proton-m-1110-saturn-v-scale/?inventory=1#bi\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Paint job should represent the launch of ExoMars 2016. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/763501067388480/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobur500",
    "types": [
      "rocket"
    ],
    "name": "UR-500",
    "scale": "1:110",
    "parts": "324",
    "designer": "DamoB",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-series/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714175132321074/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of DamoB\u2019s modular, stageable, interchangable Proton rocket collection, together with <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>, <a href=\"#MOCdamobprotonblokdm\">Proton Blok DM</a> and <a href=\"#MOCdamobprotonbreezem\">Proton Breeze-M</a>. Digital file at <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714385908966663/\">Facebook group Bricks in Space</a>. <a href=\"#MOCdamobprotonlaunchpad\">Compatible launch pad</a>. <a href=\"#MOCdamobprotontrain\">Compatible train</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonk",
    "types": [
      "rocket"
    ],
    "name": "Proton-K",
    "scale": "1:110",
    "parts": "353",
    "designer": "DamoB",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-series/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714175132321074/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of DamoB\u2019s modular, stageable, interchangable Proton rocket collection, together with <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>, <a href=\"#MOCdamobprotonblokdm\">Proton Blok DM</a> and <a href=\"#MOCdamobprotonbreezem\">Proton Breeze-M</a>. Digital file at <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714385908966663/\">Facebook group Bricks in Space</a>. <a href=\"#MOCdamobprotonlaunchpad\">Compatible launch pad</a>. <a href=\"#MOCdamobprotontrain\">Compatible train</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonkzarya",
    "types": [
      "rocket"
    ],
    "name": "Proton-K Zarya",
    "scale": "1:110",
    "parts": "413",
    "designer": "DamoB",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-series/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714175132321074/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of DamoB\u2019s modular, stageable, interchangable Proton rocket collection, together with <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonblokdm\">Proton Blok DM</a> and <a href=\"#MOCdamobprotonbreezem\">Proton Breeze-M</a>. Digital file at <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714385908966663/\">Facebook group Bricks in Space</a>. <a href=\"#MOCdamobprotonlaunchpad\">Compatible launch pad</a>. <a href=\"#MOCdamobprotontrain\">Compatible train</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonblokdm",
    "types": [
      "rocket"
    ],
    "name": "Proton Blok DM",
    "scale": "1:110",
    "parts": "400",
    "designer": "DamoB",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-series/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714175132321074/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of DamoB\u2019s modular, stageable, interchangable Proton rocket collection, together with <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a> and <a href=\"#MOCdamobprotonbreezem\">Proton Breeze-M</a>. Digital file at <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714385908966663/\">Facebook group Bricks in Space</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonbreezem",
    "types": [
      "rocket"
    ],
    "name": "Proton Breeze-M",
    "scale": "1:110",
    "parts": "464",
    "designer": "DamoB",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/proton-series/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714175132321074/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of DamoB\u2019s modular, stageable, interchangable Proton rocket collection, together with <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a> and <a href=\"#MOCdamobprotonblokdm\">Proton Blok DM</a>. Digital file at <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/714385908966663/\">Facebook group Bricks in Space</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieksputnik",
    "types": [
      "rocket"
    ],
    "name": "Sputnik",
    "scale": "1:110",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/95bdcce7-f7e9-463d-a223-48637b704258\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page of <a href=\"#MOCtechnieksoyuzrocket\">Soyuz rocket</a>. Based on <a href=\"#MOCtechniekvostoklauncher\">Vostok launcher</a>. <a href=\"#MOCrudilandmannsoyuzmod\">Mod of core stage</a>. <a href=\"#MOCtechnieksoyuzmod\">Another mod of core stage</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordr7sputnik",
    "types": [
      "rocket"
    ],
    "name": "R7 Sputnik",
    "scale": "1:110",
    "parts": "497",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/r-7-sputnik/\">Bricks in Space webpage</a>   </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtechniekvostoklauncher",
    "types": [
      "rocket"
    ],
    "name": "Vostok launcher",
    "scale": "1:110",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/95bdcce7-f7e9-463d-a223-48637b704258\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page of <a href=\"#MOCtechnieksoyuzrocket\">Soyuz rocket</a>. <a href=\"#MOCrudilandmannsoyuzmod\">Mod of core stage</a>. <a href=\"#MOCrudilandmannvoskhod2spacecraft\">Mod for Voskhod 2</a>. <a href=\"#MOCtechnieksoyuzmod\">Another mod of core stage</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59vostok1",
    "types": [
      "rocket"
    ],
    "name": "Vostok-1",
    "scale": "?",
    "parts": "183",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/yr121gDnvlB\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnathanrvostok1",
    "types": [
      "rocket"
    ],
    "name": "Vostok 1",
    "scale": "?",
    "parts": "244",
    "designer": "Nathan Readioff",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/Qb82xEg9j1z\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzervostok",
    "types": [
      "rocket"
    ],
    "name": "Vostok",
    "scale": "?",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/1k2qMegY2E0\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannvostokspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Vostok spacecraft",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/755802114825042/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordr7vostok",
    "types": [
      "rocket"
    ],
    "name": "R7 Vostok",
    "scale": "1:110",
    "parts": "573",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/r-7-vostok/\">Bricks in Space webpage</a>  </p>",
    "notes": "<p>Contains <a href=\"#MOCtechniekvostokcapsule\">Vostok capsule</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannvoskhod2spacecraft",
    "types": [
      "rocket"
    ],
    "name": "Voskhod 2 spacecraft and mod for Voskhod 2 rocket",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/795196254218961/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/794400687631851/\">Facebook post with description</a>. Mod for <a href=\"#MOCtechniekvostoklauncher\">Vostok rocket by tech_niek</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordr7voskhod",
    "types": [
      "rocket"
    ],
    "name": "R7 Voskhod",
    "scale": "1:110",
    "parts": "599",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/r-7-voskhod/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Contains <a href=\"#MOCtechniekvostokcapsule\">Vostok capsule</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCstaniasoyuz",
    "types": [
      "rocket"
    ],
    "name": "Soyuz Rocket",
    "scale": "1:193",
    "parts": "145",
    "designer": "Stania",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/3c9ec0c0-fa71-4a53-9177-38c79be7fd36\">LEGO Ideas</a>, <a href=\"https://rebrickable.com/mocs/MOC-7157/Stania/soyuz-rocket/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkei_abesoyuz",
    "types": [
      "rocket"
    ],
    "name": "Minifig-scale LEGO Soyuz -50th anniversary from the first flight-",
    "scale": "?",
    "parts": "8609",
    "designer": "Kei_Abe",
    "file_types": [
      "lxf"
    ],
    "price": "10.00 \u20ac",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-13775/Kei_Abe/minifig-scale-lego-soyuz-50th-anniversary-from-the-first-flight-/\">Rebrickable</a> </p>",
    "notes": "<p>Contains Soyuz FG rocket, Soyuz 7K-OK spacecraft, Soyuz MS spacecraft.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCendregasoyuz",
    "types": [
      "rocket"
    ],
    "name": "Soyuz-FG",
    "scale": "1:92",
    "parts": "612",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=41554\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsuchshibeinusoyuz",
    "types": [
      "rocket"
    ],
    "name": "Soyuz",
    "scale": "1:110",
    "parts": "?",
    "designer": "suchshibeinu",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.reddit.com/r/lego/comments/b6hzyx/i_made_a_soyuz_in_the_scale_of_the_saturn_v_1111/?ref=share&amp;ref_source=embed&amp;utm_content=media&amp;utm_medium=post_embed&amp;utm_name=c50bbb57796940a5a78b9c84ab5b0614&amp;utm_source=embedly&amp;utm_term=b6hzyx\">Reddit</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Also on <a href=\"https://ideas.lego.com/projects/41fbdc40-2f67-410e-b8c1-61f06e1bfc9c\">Lego Ideas</a>. Compatible <a href=\"#MOCdanfallonsoyuzspacecraft\">Soyuz spacecraft</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieksoyuzrocket",
    "types": [
      "rocket"
    ],
    "name": "Soyuz rocket",
    "scale": "1:110",
    "parts": "306",
    "designer": "tech_niek",
    "file_types": [
      "io",
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/soyuz/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://ideas.lego.com/projects/95bdcce7-f7e9-463d-a223-48637b704258\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/485365701868686/\">Facebook post with digital file</a>. <a href=\"#MOCrudilandmannsoyuzmod\">Mod of core stage</a>. <a href=\"#MOCtechnieksoyuzmod\">Another mod of core stage</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieksoyuzmod",
    "types": [
      "rocket"
    ],
    "name": "R-7 rocket modification with vernier thrusters",
    "scale": "1:110",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/802690553469531/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification for tech_niek's designs of <a href=\"#MOCtechnieksoyuzrocket\">Soyuz</a>, <a href=\"#MOCtechniekvostoklauncher\">Vostok</a>, <a href=\"#MOCtechnieksputnik\">Sputnik</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsoyuzmod",
    "types": [
      "rocket"
    ],
    "name": "R-7 rocket modification of tech_niek design",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/786960088375911/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification for tech_niek's designs of <a href=\"#MOCtechnieksoyuzrocket\">Soyuz</a>, <a href=\"#MOCtechniekvostoklauncher\">Vostok</a>, <a href=\"#MOCtechnieksputnik\">Sputnik</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichsoyuz",
    "types": [
      "rocket"
    ],
    "name": "Soyuz",
    "scale": "1:110",
    "parts": "517",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-34825/MuscoviteSandwich/soyuz-rocket-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlersoyuz21v",
    "types": [
      "rocket"
    ],
    "name": "Soyuz 2-1v",
    "scale": "?",
    "parts": "435",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://imgur.com/a/AWZSm\">Imgur</a>, <a href=\"http://arrakis.muportal.hu/ldd/soyuz.zip\">http</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCandorschindlersoyuzlaunchpad\">launchpad</a> and <a href=\"#MOCandorschindlersoyuztransportererector\">transporter</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlersoyuzfg",
    "types": [
      "rocket"
    ],
    "name": "Soyuz-FG",
    "scale": "?",
    "parts": "477",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://imgur.com/a/AWZSm\">Imgur</a>, <a href=\"http://arrakis.muportal.hu/ldd/soyuz.zip\">http</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCandorschindlersoyuzlaunchpad\">launchpad</a> and <a href=\"#MOCandorschindlersoyuztransportererector\">transporter</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlersoyuzuprogress",
    "types": [
      "rocket"
    ],
    "name": "Soyuz U / Progress",
    "scale": "?",
    "parts": "438",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://imgur.com/a/AWZSm\">Imgur</a>, <a href=\"http://arrakis.muportal.hu/ldd/soyuz.zip\">http</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCandorschindlersoyuzlaunchpad\">launchpad</a> and <a href=\"#MOCandorschindlersoyuztransportererector\">transporter</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsuchshibeinuprogress",
    "types": [
      "rocket"
    ],
    "name": "Progress",
    "scale": "1:110",
    "parts": "?",
    "designer": "suchshibeinu",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCdanfallonprogressspacecraft\">Progress spacecraft</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieksoyuzspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Soyuz spacecraft",
    "scale": "1:110",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/95bdcce7-f7e9-463d-a223-48637b704258\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page of <a href=\"#MOCtechnieksoyuzrocket\">Soyuz rocket</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaxussoyuz",
    "types": [
      "rocket"
    ],
    "name": "Soyuz-TMA spacecraft",
    "scale": "1:110",
    "parts": "44",
    "designer": "Andor Schindler",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://shared.muportal.hu/lsa/\">Lego Space Agency</a> </p>",
    "notes": "<p><a href=\"https://www.reddit.com/r/lego/comments/6gwswr/most_of_my_rockets_and_spacecrafts/\">Picture of whole Andor Schindler's collection.</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketssoyuzspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Soyuz spacecraft",
    "scale": "1:110",
    "parts": "16",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=40247\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkehu05soyuzspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Soyuz spacecraft",
    "scale": "1:82",
    "parts": "129",
    "designer": "kehu05",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36363/kehu05/182-soyuz-spacecraft/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichsoyuzspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Soyuz spacecraft",
    "scale": "1:110",
    "parts": "64",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-40606/MuscoviteSandwich/soyuz-spacecraft-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/iss/soyuz/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdarthsobansoyuzcapsule",
    "types": [
      "rocket"
    ],
    "name": "Soyuz Capsule (minifig scale)",
    "scale": "?",
    "parts": "297",
    "designer": "DarthSoban",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=10658\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonsoyuzspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Soyuz spacecraft",
    "scale": "1:110",
    "parts": "49",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCmuscovitesandwichsoyuzspacecraft\">Soyuz spacecraft by MuscoviteSandwich</a>. Compatible <a href=\"#MOCsuchshibeinusoyuz\">Soyuz rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonprogressspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Progress spacecraft",
    "scale": "1:110",
    "parts": "54",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCmuscovitesandwichsoyuzspacecraft\">Soyuz spacecraft by MuscoviteSandwich</a>. Compatible <a href=\"#MOCsuchshibeinuprogress\">Progress rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveautsiklon3",
    "types": [
      "rocket"
    ],
    "name": "Tsyklon 3",
    "scale": "1:110",
    "parts": "274",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/tsyklon-3/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/244029736770223/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/920727881665797/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaukosmos1",
    "types": [
      "rocket"
    ],
    "name": "Kosmos 1",
    "scale": "1:110",
    "parts": "216",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaukosmos2",
    "types": [
      "rocket"
    ],
    "name": "Kosmos 2",
    "scale": "1:110",
    "parts": "30",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaukosmos3",
    "types": [
      "rocket"
    ],
    "name": "Kosmos 3",
    "scale": "1:110",
    "parts": "206",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaukosmos3m",
    "types": [
      "rocket"
    ],
    "name": "Kosmos 3M",
    "scale": "1:110",
    "parts": "253",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaur12",
    "types": [
      "rocket"
    ],
    "name": "R 12",
    "scale": "1:110",
    "parts": "22",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaur14",
    "types": [
      "rocket"
    ],
    "name": "R 14",
    "scale": "1:110",
    "parts": "185",
    "designer": "Nicolas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/famille-kosmos/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085874497943/\">Facebook group Lego Rocket Collection</a>, <a href=\"https://bricksin.space/launch-vehicles/kosmos-series/?fbclid=IwAR3vopjmWdc-0W6PX0vWrl5GB0qojn_BFfOhaxSiNHDxooxgEoYQBhzoCUM\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/200084107831453\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkehu05shenzhouspacecraft",
    "types": [
      "rocket"
    ],
    "name": "Shenzhou spacecraft",
    "scale": "1:82",
    "parts": "146",
    "designer": "kehu05",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36418/kehu05/182-shenzhou-spacecraft/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichshenzoucapsule",
    "types": [
      "rocket"
    ],
    "name": "Shenzou Capsule",
    "scale": "1:110",
    "parts": "79",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-50529/MuscoviteSandwich/shenzhou-capsule-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/manned-spacecraft/shenzhou/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCfitzruskn1",
    "types": [
      "rocket"
    ],
    "name": "Soviet N1 Rocket + LK Lander + Soyuz spacecraft",
    "scale": "1:110",
    "parts": "820",
    "designer": "woodpiece, fitzrurisk",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-20871/fitzrurisk/soviet-n1-rocket-lk-lander-soyuz-spacecraft/\">Rebrickable</a> </p>",
    "notes": "<p>Original by Woodpiece, improved by fitzrurisk.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkei_aben1",
    "types": [
      "rocket"
    ],
    "name": "Soviet N1-L3 moon rocket",
    "scale": "?",
    "parts": "?",
    "designer": "KEI ABE",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.flickr.com/photos/91934169@N08/8346545714/in/photostream/\">Flickr</a>, <a href=\"http://www.brickshelf.com/cgi-bin/gallery.cgi?f=520169\">Brickshelf</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwolframbrosziesn1",
    "types": [
      "rocket"
    ],
    "name": "Soviet N1 Rocket, Soyuz 7K-LOK, LK Lander",
    "scale": "1:110",
    "parts": "2320",
    "designer": "Wolfram Broszies",
    "file_types": [
      "ldr"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/660864780985443/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Model will be part of <a href=\"#TOCspecialmention\">Bricks in space</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCallanjordann1",
    "types": [
      "rocket"
    ],
    "name": "N1",
    "scale": "1:110",
    "parts": "?",
    "designer": "Allan Jordan",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/370586206679970/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Based on <a href=\"#MOCfitzruskn1\">N1 by fitzrusk, woodpiece</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspanglen1",
    "types": [
      "rocket"
    ],
    "name": "Soviet N1 Moon Rocket",
    "scale": "1:110",
    "parts": "902",
    "designer": "Spangle",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-37172/Spangle/soviet-n1-moon-rocket-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCfitzruriskmoonlander",
    "types": [
      "rocket"
    ],
    "name": "Soviet LK Moon Lander",
    "scale": "?",
    "parts": "26",
    "designer": "fitzrurisk",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-20223/fitzrurisk/soviet-lk-moon-lander/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmaxifiguresls",
    "types": [
      "rocket"
    ],
    "name": "Space Launch System",
    "scale": "?",
    "parts": "225",
    "designer": "MaxiFigure",
    "file_types": [
      "png"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-0894/MaxiFigure/space-launch-system/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kslsblock1",
    "types": [
      "rocket"
    ],
    "name": "NASA Space Launch System SLS Block 1",
    "scale": "1:110",
    "parts": "2392",
    "designer": "MoppeW40k",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28893/MoppeW40k/nasa-space-launch-system-sls-block-1-1110-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://bricksin.space/launch-vehicles/sls/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Block 1 configuration with Orion capsule</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kgslvmkii",
    "types": [
      "rocket"
    ],
    "name": "GSLV Mk II",
    "scale": "1:110",
    "parts": "272",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kgslvmkiii",
    "types": [
      "rocket"
    ],
    "name": "GSLV Mk III",
    "scale": "1:110",
    "parts": "675",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Compatible probe <a href=\"#MOCmoppew40kchandrayaan2\">Chandrayaan 2</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerglsvmk3",
    "types": [
      "rocket"
    ],
    "name": "Midi ISRO GLSV Mk3",
    "scale": "?",
    "parts": "322",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56800/SpaceXplorer%2016/midi-isro-glsv-mk3/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40knuclear",
    "types": [
      "rocket"
    ],
    "name": "Reusable Nuclear Shuttle",
    "scale": "1:110",
    "parts": "807",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28541/MoppeW40k/reusable-nuclear-shuttle-1110-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kvulcan",
    "types": [
      "rocket"
    ],
    "name": "ULA Vulcan",
    "scale": "1:110",
    "parts": "1000",
    "designer": "MoppeW40k",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28693/MoppeW40k/ula-vulcan-1110-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsvulcandreamchaser",
    "types": [
      "rocket"
    ],
    "name": "ULA Vulcan with Dream Chaser",
    "scale": "1:110",
    "parts": "267",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-38634/MOCrockets/ula-vulcan-with-dream-chaser-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbenjaminunisvanguard",
    "types": [
      "rocket"
    ],
    "name": "Vanguard",
    "scale": "1:110",
    "parts": "21",
    "designer": "Benjamin Unis",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/448411938897396/\">Facebook group Bricks in Space</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsantares",
    "types": [
      "rocket"
    ],
    "name": "Antares with Enhanced Cygnus",
    "scale": "1:110",
    "parts": "80",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$1.50",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-26741/MOCrockets/antares-with-enhanced-cygnus-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsboeingstarliner",
    "types": [
      "rocket"
    ],
    "name": "Boeing Starliner \\ Atlas V N22",
    "scale": "1:110",
    "parts": "125",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$2.22",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-22633/MOCrockets/boeing-starliner-atlas-v-n22-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichboeingstarliner",
    "types": [
      "rocket"
    ],
    "name": "Boeing Starliner",
    "scale": "1:110",
    "parts": "99",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-58785/MuscoviteSandwich/boeing-starliner-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCmuscovitesandwichatlasvn22\">Atlas V N22</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerstarliner",
    "types": [
      "rocket"
    ],
    "name": "Boeing CST-100 Starliner Capsule",
    "scale": "1:110",
    "parts": "166",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-54997/SpaceXplorer%2016/1110-scale-boeing-cst-100-starliner-capsule/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"#MOCspacexplorerstarstand\">Compatible stand</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketselectron",
    "types": [
      "rocket"
    ],
    "name": "Rocket Lab \"Electron\"",
    "scale": "1:110",
    "parts": "17",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-22514/MOCrockets/rocket-lab-electron-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjnaujokelectron",
    "types": [
      "rocket"
    ],
    "name": "RocketLabs Electron",
    "scale": "1:110",
    "parts": "16",
    "designer": "jnaujok",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/458532817885308/\">Facebook group Bricks in Space</a>, <a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=62030\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCraymanhendrickselectron",
    "types": [
      "rocket"
    ],
    "name": "Electron",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rayman Hendricks",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/838686989869887/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnerozelectron",
    "types": [
      "rocket"
    ],
    "name": "Electron",
    "scale": "1:26",
    "parts": "80",
    "designer": "neroz",
    "file_types": [
      "pdf"
    ],
    "price": "$1.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-46603/neroz/electron-scale-126/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanjuno1",
    "types": [
      "rocket"
    ],
    "name": "Juno 1",
    "scale": "1:110",
    "parts": "25",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Some stickers also in <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas NASA: Mercury and Gemini</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanjunoii",
    "types": [
      "rocket"
    ],
    "name": "Juno II",
    "scale": "1:110",
    "parts": "30",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Some stickers also in <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas NASA: Mercury and Gemini</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegobuilder1234juno2",
    "types": [
      "rocket"
    ],
    "name": "Juno 2",
    "scale": "?",
    "parts": "48",
    "designer": "LegoBuilder1234",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-34742/Legobuilder1234/juno-2/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwolframbroszieslittlejoe",
    "types": [
      "rocket"
    ],
    "name": "Little Joe",
    "scale": "1:110",
    "parts": "16",
    "designer": "Wolfram Broszies",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCwolframbroszieslittlejoelaunchpad\">launchpad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmnemonlittlejoe",
    "types": [
      "rocket"
    ],
    "name": "Little Joe",
    "scale": "?",
    "parts": "22",
    "designer": "mnemon",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/0Qa3Bn8pvMo\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanlittlejoeii",
    "types": [
      "rocket"
    ],
    "name": "Little Joe II",
    "scale": "1:110",
    "parts": "37",
    "designer": "Eiffleman",
    "file_types": [
      "io",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/778401709231749/\">Facebook group Bricks in Space</a>, <a href=\"https://bricksin.space/launch-vehicles/little-joe-ii/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38/updates?project_updates_page=3\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p>Digital file at facebook page is updated version with decreased price. Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a>. Compatible launcher: <a href=\"#MOCeifflemanlittlejoeiilauncher\">Little Joe II Launcher</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kares1",
    "types": [
      "rocket"
    ],
    "name": "Ares 1",
    "scale": "1:110",
    "parts": "444",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexploreraresi",
    "types": [
      "rocket"
    ],
    "name": "Mini NASA Ares I",
    "scale": "?",
    "parts": "82",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56338/SpaceXplorer%2016/mini-nasa-ares-i-display-model/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCXdynasoar",
    "types": [
      "rocket"
    ],
    "name": "Dyna-Soar",
    "scale": "1:110",
    "parts": "29",
    "designer": "?",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsx20suborbital",
    "types": [
      "rocket"
    ],
    "name": "X-20 (Dyna-Soar) [Suborbital]",
    "scale": "1:110",
    "parts": "36",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=38929\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsx20",
    "types": [
      "rocket"
    ],
    "name": "X-20 (Dyna-Soar) [orbital]",
    "scale": "1:110",
    "parts": "33",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=39111\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40khiib",
    "types": [
      "rocket"
    ],
    "name": "H-II B",
    "scale": "1:110",
    "parts": "844",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://bricksin.space/launch-vehicles/h-ii-b/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Contains <a href=\"#MOCmoppew40khtv\">H-II Transfer Vehicle (HTV)</a>. <a href=\"#MOCflorenttodeschinihiib\">Modification by Florent Todeschini</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCflorenttodeschinihiib",
    "types": [
      "rocket"
    ],
    "name": "H-II B",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florent Todeschini",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/884367915301794\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/884367735301812\">Facebook post with description</a>. Modification of <a href=\"#MOCmoppew40khiib\">MoppeW40k version</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40klongmarch2f",
    "types": [
      "rocket"
    ],
    "name": "Long March 2F",
    "scale": "1:110",
    "parts": "343",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"#MOCflorenttodeschinilongmarch2\">Improved version</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCflorenttodeschinilongmarch2",
    "types": [
      "rocket"
    ],
    "name": "Long March 2F",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florent Todeschini, MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/922117261526859/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Improved version of <a href=\"#MOCmoppew40klongmarch2f\">MoppeW40k's Long March 2F</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/922115074860411/\">Facebook post with descritpion and photos</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichlongmarch2f",
    "types": [
      "rocket"
    ],
    "name": "Long March 2F",
    "scale": "1:110",
    "parts": "1042",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-50824/MuscoviteSandwich/long-march-2f-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaulongmarch5b",
    "types": [
      "rocket"
    ],
    "name": "Long March 5B",
    "scale": "1:110",
    "parts": "799",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/cnsa/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914108948453\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40klongmarch7",
    "types": [
      "rocket"
    ],
    "name": "Long March 7",
    "scale": "1:110",
    "parts": "343",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/780917608980159/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaulongmarch8",
    "types": [
      "rocket"
    ],
    "name": "Long March 8",
    "scale": "1:110",
    "parts": "282",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/cnsa/longmarch-8/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaulongmarch8reusable",
    "types": [
      "rocket"
    ],
    "name": "Long March 8 reusable",
    "scale": "1:110",
    "parts": "401",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/cnsa/longmarch-8-reutilisable/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaunextgenchinesecapsule",
    "types": [
      "rocket"
    ],
    "name": "Next Generation Chinese Capsule",
    "scale": "1:110",
    "parts": "44",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/navettes/cnsa/next-gen-chinese-capsule/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912968492441736/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914335615097/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40komega",
    "types": [
      "rocket"
    ],
    "name": "Omega",
    "scale": "1:110",
    "parts": "559",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanthordelta",
    "types": [
      "rocket"
    ],
    "name": "Thor-Delta B",
    "scale": "1:110",
    "parts": "29",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Some stickers also in <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas NASA: Mercury and Gemini</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kdreamchaser",
    "types": [
      "rocket"
    ],
    "name": "Dream Chaser",
    "scale": "1:110",
    "parts": "72",
    "designer": "MoppeW40k",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/dream-chaser/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsergekellerskylark",
    "types": [
      "rocket"
    ],
    "name": "Skylark sounding rocket",
    "scale": "1:110",
    "parts": "6",
    "designer": "Serge Keller",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/943766299361955/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonveronique",
    "types": [
      "rocket"
    ],
    "name": "V\u00e9ronique",
    "scale": "1:110",
    "parts": "9",
    "designer": "Dan Fallon",
    "file_types": [
      "img"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/photo.php?fbid=10156917751493177&amp;set=gm.695484387523482&amp;type=3&amp;theater&amp;ifg=1\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmarkbalderramaa4",
    "types": [
      "rocket"
    ],
    "name": "Aggregat 4",
    "scale": "1:110",
    "parts": "36",
    "designer": "Mark Balderrama",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/445700545835202/\">Facebook group Bricks in Space</a> <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkaeroa4withvehicles",
    "types": [
      "rocket"
    ],
    "name": "A-4/V-2 rocket with Support and Supply vehicles",
    "scale": "1:110",
    "parts": "730",
    "designer": "Kaero",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-33001/Kaero/a-4v-2-rocket-with-support-and-supply-vehicles/\">Rebrickable</a> </p>",
    "notes": "<p>A-4 rocket and 11 support and supply vehicles, based on <a href=\"#MOCmarkbalderramaa4\">Aggregat 4</a> and <a href=\"#MOCwolframbrosziesv2launchplatoon\">V2 Launch Platoon</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichv2",
    "types": [
      "rocket"
    ],
    "name": "V2 rocket",
    "scale": "1:110",
    "parts": "48",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36584/MuscoviteSandwich/v2-a4-rocket-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Version 2.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannv2",
    "types": [
      "rocket"
    ],
    "name": "V2",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/782559185482668/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Description in <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/779054772499776/\">Facebook group Bricks in Space</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdludersmodernizedv2",
    "types": [
      "rocket"
    ],
    "name": "Modernized V2 Rocket",
    "scale": "?",
    "parts": "310",
    "designer": "DLuders",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-3101/DLuders/modernized-v2-rocket/\">Rebrickable</a> </p>",
    "notes": "<p>Technic style</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCastrodroidv2",
    "types": [
      "rocket"
    ],
    "name": "V-2",
    "scale": "1:56",
    "parts": "141",
    "designer": "AstroDroid",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=50326\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59v2",
    "types": [
      "rocket"
    ],
    "name": "V-2",
    "scale": "?",
    "parts": "1715",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/xGejeZW3vKX\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kvega",
    "types": [
      "rocket"
    ],
    "name": "Vega",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kvegac",
    "types": [
      "rocket"
    ],
    "name": "Vega C",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorervega",
    "types": [
      "rocket"
    ],
    "name": "ESA Arianespace Vega",
    "scale": "1:110",
    "parts": "263",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-57348/SpaceXplorer%2016/1110-scale-esa-arianespace-vega-launch-vehicle/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauvls1",
    "types": [
      "rocket"
    ],
    "name": "VLS 1",
    "scale": "1:110",
    "parts": "79",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/aeb/vls-1/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/912969629108289/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/188914788948385/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonwresat",
    "types": [
      "rocket"
    ],
    "name": "WRESAT (Sparta rocket)",
    "scale": "1:110",
    "parts": "55",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/wresat/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Compatible launchpad <a href=\"#MOCdanfallonwoomerala8\">Woomera LA-8</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauzenit2",
    "types": [
      "rocket"
    ],
    "name": "Zenit 2",
    "scale": "1:110",
    "parts": "244",
    "designer": "Nicolas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/fusees/roscomos/zenit-2/\">Lego Rocket Collection</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/924837827921469/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/178082023364995/permalink/200085741164623/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkeorion11",
    "types": [
      "rocket"
    ],
    "name": "Orion 1.1",
    "scale": "?",
    "parts": "414",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=76965\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsorion",
    "types": [
      "rocket"
    ],
    "name": "Orion spacecraft",
    "scale": "1:110",
    "parts": "62",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=47265\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerartemisorioncev",
    "types": [
      "rocket"
    ],
    "name": "NASA Artemis Orion CEV Capsule",
    "scale": "1:110",
    "parts": "32",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-51697/SpaceXplorer%2016/1110-scale-nasa-artemis-orion-cev-capsule/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCastrogatororionnuclear",
    "types": [
      "rocket"
    ],
    "name": "Orion Nuclear Pulse Rocket",
    "scale": "?",
    "parts": "1261",
    "designer": "astrogator",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=94052\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanagenad",
    "types": [
      "payload"
    ],
    "name": "Agena D payload for Atlas Rocket (replaces Mercury Capsule)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Alternative build of <a href=\"#MOCeifflemanmercuryatlas\">Mercury-Atlas</a> Part of <a href=\"#MOCeifflemanrocketgarden\">NASA Rocket Garden</a>. Instructions shown in updates of Lego ideas page. Some stickers also in <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">Lego Ideas NASA: Mercury and Gemini</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbanthab0mbathena1payload",
    "types": [
      "payload"
    ],
    "name": "Athena 1 Rocket PAYLOAD!",
    "scale": "?",
    "parts": "18",
    "designer": "banthab0mb",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=97871\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbanthab0mbathena1marsrocket\">rocket</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonatv",
    "types": [
      "payload"
    ],
    "name": "ATV",
    "scale": "1:110",
    "parts": "145",
    "designer": "Dan Fallon",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/atv/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40katv",
    "types": [
      "payload"
    ],
    "name": "European Space Agency ESA Automated Transfer Vehicle ATV",
    "scale": "1:110",
    "parts": "83",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Compatible rocket <a href=\"#MOCmoppew40kariane5\">Ariane 5</a>. Version 6.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwillmatv",
    "types": [
      "payload"
    ],
    "name": "ATV Automated Transfer Vehicle",
    "scale": "1:110",
    "parts": "204",
    "designer": "WillM",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-61138/WillM/atv-automated-transfer-vehicle-iss-saturn-v-1110-scale/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncraincentaur",
    "types": [
      "payload"
    ],
    "name": "Centaur stage",
    "scale": "1:110",
    "parts": "?",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903739203364665/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903735103365075/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbrandoncraintitaniiie\">Titan IIIE</a>, <a href=\"#MOCbrandoncrainvikinglander\">Viking lander</a>, <a href=\"#MOCbrandoncrainvikingorbiter\">Viking orbiter</a>, <a href=\"#MOCbrandoncrainvoyagerspaceprobe\">Voyager space probe</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kchandrayaan2",
    "types": [
      "payload"
    ],
    "name": "Chandrayaan 2",
    "scale": "1:110",
    "parts": "338",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Compatible rocket <a href=\"#MOCmoppew40kgslvmkiii\">GSLV Mk III</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmollerdyneticshls",
    "types": [
      "payload"
    ],
    "name": "Dynetics HLS",
    "scale": "1:110",
    "parts": "?",
    "designer": "Florentin M\u00f6ller",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/941709316234320/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/941044762967442/\">Facebook post with image</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCvioletteclingersmithfobos2",
    "types": [
      "payload"
    ],
    "name": "Fobos 2",
    "scale": "1:32",
    "parts": "?",
    "designer": "Violette Clingersmith",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/800164673722119/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/800161147055805/\">Facebook post with renders and discussion</a>. Also on <a href=\"https://ideas.lego.com/projects/acc406b4-f6a4-4c1a-9f9f-07a434e92364\">Lego Ideas 1</a>, <a href=\"https://ideas.lego.com/projects/7047e92a-4882-46be-b49d-703554fd55c1\">Lego Ideas 2</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnasagpmobservatory",
    "types": [
      "payload"
    ],
    "name": "GPM Core Observatory",
    "scale": "1:110",
    "parts": "210",
    "designer": "NASA",
    "file_types": [
      "pdf",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://gpm.nasa.gov/education/interactive/gpm-core-observatory-lego-model\">NASA</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCharkinswellinghubble",
    "types": [
      "payload"
    ],
    "name": "Hubble Telescope",
    "scale": "?",
    "parts": "64",
    "designer": "Andrew Harkins, David Welling",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/hubble-space-telescope/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/483904772014779/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/359568274448430/\">Facebook discussion with MOC developement</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCendregahubble",
    "types": [
      "payload"
    ],
    "name": "Hubble Space Telescope",
    "scale": "1:93",
    "parts": "205",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=32504\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkehubble",
    "types": [
      "payload"
    ],
    "name": "Hubble",
    "scale": "?",
    "parts": "900",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=79722\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCxlegolddgalleryhubble",
    "types": [
      "payload"
    ],
    "name": "Hubble Space Telescope",
    "scale": "?",
    "parts": "2052",
    "designer": "?",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/794867630918490/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Unknown author, model used to be available in the Lego LDD Gallery to download.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtopaceshubble",
    "types": [
      "payload"
    ],
    "name": "Hubble Space Telescope for KingsKnight's Space Shuttle",
    "scale": "1:110",
    "parts": "46",
    "designer": "TOPACES",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47731/TOPACES/hubble-space-telescope-for-kingsknights-space-shuttle/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCkingsknightspaceshuttlev2\">Space Shuttle by KingsKnight</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCearendil2016minijameswebbtelescope",
    "types": [
      "payload"
    ],
    "name": "Mini James Webb Telescope",
    "scale": "?",
    "parts": "25",
    "designer": "Earendil2016",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=17897\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwillmjameswebb",
    "types": [
      "payload"
    ],
    "name": "James Webb Space Telescope - Optics",
    "scale": "1:110",
    "parts": "108",
    "designer": "WillM",
    "file_types": [
      "io"
    ],
    "price": "",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-49148/WillM/james-webb-space-telescope-optics/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwhatsuptodayinsight",
    "types": [
      "payload"
    ],
    "name": "InSight: The Next NASA's Mars Lander",
    "scale": "?",
    "parts": "?",
    "designer": "Whatsuptoday",
    "file_types": [
      "html"
    ],
    "price": "0",
    "links": "<p><a href=\"http://whatsuptoday.free.fr/InSight/Mars/Lander/building_eng.html#page/1\">whatsuptoday.free.fr</a>, <a href=\"https://ideas.lego.com/projects/550d5733-3f89-4640-bc5b-a6a59afa469e\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59marsinsight",
    "types": [
      "payload"
    ],
    "name": "Mars InSight",
    "scale": "?",
    "parts": "166",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/KZvm9BoLvG6\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanypathfinder",
    "types": [
      "payload"
    ],
    "name": "Mars Pathfinder",
    "scale": "?",
    "parts": "76",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$2.00",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-21737/Brickbrothers-Germany/mars-pathfinder/\">Rebrickable</a>~~ </p>",
    "notes": "<p>MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCmuscovitesandwichpathfinder",
    "types": [
      "payload"
    ],
    "name": "Mars Vignette #3: Pathfinder",
    "scale": "1:110",
    "parts": "41",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55056/MuscoviteSandwich/mars-vignette-3-pathfinder-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildemarsroverdiorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Sojourner's first roll-out on Mars",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde, Mark Balderrama",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/767314840340436/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/767316107006976/\">Facebook post with pdf</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/767315973673656/\">Facebook post with digital file</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichopportunity",
    "types": [
      "payload"
    ],
    "name": "Mars Exploration Rover Opportunity/Spirit",
    "scale": "? (simliar to [LEGO set 21104-1](#SET21104-1))",
    "parts": "116",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-26833/MuscoviteSandwich/mars-exploration-rover-opportunityspirit/\">Rebrickable</a> </p>",
    "notes": "<p>Older version as pdf on <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/503490050056251/\">facebook</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichucsmarsexplorationrover",
    "types": [
      "payload"
    ],
    "name": "UCS Mars Exploration Rover Opportunity/Spirit",
    "scale": "?",
    "parts": "771",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$6.50",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-39989/MuscoviteSandwich/ucs-opportunityspirit-mars-exploration-rover/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanyrover",
    "types": [
      "payload"
    ],
    "name": "Mars Rover",
    "scale": "?",
    "parts": "105",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$3.00",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-21748/Brickbrothers-Germany/mars-rover/\">Rebrickable</a>~~ </p>",
    "notes": "<p>MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCvioletteclingersmithmarsexplorationrover",
    "types": [
      "payload"
    ],
    "name": "Mars Exploration Rover",
    "scale": "1:20",
    "parts": "167",
    "designer": "Violette Clingersmith",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/702356230169631/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Also on <a href=\"https://ideas.lego.com/projects/db2a7391-29ae-45e5-9d9b-2b0deb1ad547\">Lego Ideas</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovecuriosity",
    "types": [
      "payload"
    ],
    "name": "Mars Science Laboratory Curiosity Rover",
    "scale": "?",
    "parts": "314",
    "designer": "Perijove",
    "file_types": [
      "pdf",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-0271/Perijove/mars-science-laboratory-curiosity-rover/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCperijovedescentstage\">Skycrane</a> and <a href=\"#MOCperijoveskycranestand\">Stand</a>. This MOC succeeded on LEGO Cuusoo and resulted in LEGO set <a href=\"#SET21104-1\">21104-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovedescentstage",
    "types": [
      "payload"
    ],
    "name": "Mars Science Laboratory Descent Stage",
    "scale": "?",
    "parts": "364",
    "designer": "Perijove",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-0288/Perijove/mars-science-laboratory-descent-stage/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCperijovecuriosity\">Rover</a> and <a href=\"#MOCperijoveskycranestand\">Stand</a>.Skycrane and Curiosity by same Designer are compatible</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCperijoveskycranestand",
    "types": [
      "payload"
    ],
    "name": "Mars Science Laboratory Sky Crane Stand",
    "scale": "?",
    "parts": "160",
    "designer": "Perijove",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-0287/Perijove/mars-science-laboratory-sky-crane-stand/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCperijovecuriosity\">Rover</a> and <a href=\"#MOCperijovedescentstage\">Stand</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwxzhrscuriositymarsrover",
    "types": [
      "payload"
    ],
    "name": "CUUSOO-Curiosity Mars Rover",
    "scale": "?",
    "parts": "379",
    "designer": "wxzhrs",
    "file_types": [
      "lxf"
    ],
    "price": "$1.49",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-26498/wxzhrs/cuusoo-curiosity-mars-rover/\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"SET21104-1\">SET21104-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCpedrojmarsroverperseverance",
    "types": [
      "payload"
    ],
    "name": "NASA Mars Rover 2020 Perseverance",
    "scale": "1:10",
    "parts": "1157",
    "designer": "PedroJ",
    "file_types": [
      "io"
    ],
    "price": "$4.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-49467/PedroJ/nasa-mars-rover-2020-perseverance/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjosephchambersperseverance",
    "types": [
      "payload"
    ],
    "name": "Perseverance Mars Rover with Ingenuity drone",
    "scale": "?",
    "parts": "?",
    "designer": "Joe Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1HpGUycrAX4SduKQpFS-r0iqXsR6-qSCu\">AB's Google drive</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET21104-1\">SET21104-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichmangalyaan",
    "types": [
      "payload"
    ],
    "name": "Mangalyaan",
    "scale": "1:110",
    "parts": "?",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-56639/MuscoviteSandwich/pslv-with-mangalyaan-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p><a href=\"#MOCmuscovitesandwichpslv\">Compatible PSLV</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichlandedmer",
    "types": [
      "payload"
    ],
    "name": "Mars Vignette #1: Landed MER",
    "scale": "1:110",
    "parts": "45",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-54908/MuscoviteSandwich/mars-vignette-1-landed-mer-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichmerandodyssey",
    "types": [
      "payload"
    ],
    "name": "Mars Vignette #2: MER rover and 2001 Mars Odyssey orbiter",
    "scale": "1:110",
    "parts": "38",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55052/MuscoviteSandwich/mars-vignette-2-mer-rover-and-2001-mars-odyssey-orbiter-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichmars2020",
    "types": [
      "payload"
    ],
    "name": "Mars 2020 over Earth",
    "scale": "1:110",
    "parts": "41",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47718/MuscoviteSandwich/mars-2020-over-earth-saturn-v-scaled/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Contains centaur stage with Perseverance mars rover. Also part of <a href=\"#MOCmuscovitesandwichultimateatlasv\">Atlas V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCperijoveingenuity",
    "types": [
      "payload"
    ],
    "name": "NASA Mars Helicopter Ingenuity",
    "scale": "1:3",
    "parts": "729",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-51015/Perijove/nasa-mars-helicopter-ingenuity/\">Rebrickable</a>, <a href=\"https://ideas.lego.com/projects/1af4fefb-180f-49f6-95a4-a4897456411f\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCpedrojingenuitymarshelicopter",
    "types": [
      "payload"
    ],
    "name": "NASA-Ingenuity Mars helicopter",
    "scale": "?",
    "parts": "354",
    "designer": "PedroJ",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47907/PedroJ/nasa-ingenuity-mars-helicopter/\">Rebrickable</a>, <a href=\"https://ideas.lego.com/projects/c4cb16b2-b981-481c-8948-a58c99f9f51a\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerosirisrex",
    "types": [
      "payload"
    ],
    "name": "OSIRIS-REx Probe",
    "scale": "1:110",
    "parts": "9",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-54928/SpaceXplorer%2016/1110-scale-nasa-osiris-rex-probe/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkeiabelunokhod1",
    "types": [
      "payload"
    ],
    "name": "Lunokhod 1",
    "scale": "1:32",
    "parts": "?",
    "designer": "KEI ABE",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/794186174319969/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.flickr.com/photos/91934169@N08/sets/72157632471865586/with/8361607226/\">Pictures and description on Flickr</a> </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCvioletteclingersmithlunokhod2",
    "types": [
      "payload"
    ],
    "name": "Lunokhod 2",
    "scale": "1:32",
    "parts": "?",
    "designer": "Violette Clingersmith, Kei Abe",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/943995169339068/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanyvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager",
    "scale": "?",
    "parts": "176",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$3.50",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-23481/Brickbrothers-Germany/voyager/\">Rebrickable</a>~~ </p>",
    "notes": "<p>MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCluispgvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager Mission",
    "scale": "?",
    "parts": "161",
    "designer": "LuisPG",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/97fdfda3-b07e-429b-90d1-48331c8afc4d\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions on <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager with Centaur Launch Vehicle",
    "scale": "1:110",
    "parts": "35",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-vehicles/titan-iii-e/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/365808437157747/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Only voyager also at <a href=\"https://bricksin.space/payloads/voyager/\">Bricks in Space</a>. Compatible launcher <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCqaajakvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager",
    "scale": "?",
    "parts": "164",
    "designer": "qaajak",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=30215\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdarthsobanvoyager77",
    "types": [
      "payload"
    ],
    "name": "Voyager '77",
    "scale": "?",
    "parts": "77",
    "designer": "DarthSoban",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=68425\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/819445128460740/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Designed to fit into Titan IIIE (in developemnt).</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainvoyagerspaceprobe",
    "types": [
      "payload"
    ],
    "name": "Voyager space probe",
    "scale": "1:110",
    "parts": "?",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903739203364665/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903735103365075/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbrandoncraintitaniiie\">Titan IIIE</a>, <a href=\"#MOCbrandoncraincentaur\">Centaur stage</a>, <a href=\"#MOCbrandoncrainvikinglander\">Viking lander</a>, <a href=\"#MOCbrandoncrainvikingorbiter\">Viking orbiter</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonvoyageriandii",
    "types": [
      "payload"
    ],
    "name": "Voyager I and II",
    "scale": "1:110",
    "parts": "21",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/voyager-i-and-ii/\">Bricks in Space webpage</a>, <a href=\"https://rebrickable.com/mocs/MOC-43225/phreaddee/voyager-i-voyager-ii/\">Rebrickable</a> </p>",
    "notes": "<p>Proper in scale, but do not fit into Titan IIIE builds.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCstephenpakbazvoyager",
    "types": [
      "payload"
    ],
    "name": "Voyager",
    "scale": "1:20",
    "parts": "?",
    "designer": "Stephen Pakbaz, Violette Clingersmith",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/946453139093271/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/946447969093788/\">Facebook post with images</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichvoyager2",
    "types": [
      "payload"
    ],
    "name": "Voyager 2",
    "scale": "1:110",
    "parts": "55",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-42851/MuscoviteSandwich/voyager-2-1110-scale/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovesmap",
    "types": [
      "payload"
    ],
    "name": "SMAP",
    "scale": "?",
    "parts": "195",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-6118/Perijove/smap/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovetess",
    "types": [
      "payload"
    ],
    "name": "TESS",
    "scale": "?",
    "parts": "419",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-7773/Perijove/tess/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCflerchjjgpsiia",
    "types": [
      "payload"
    ],
    "name": "GPS IIA Satellite",
    "scale": "?",
    "parts": "171",
    "designer": "flerchjj",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-4439/flerchjj/gps-iia-satellite/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCalexbrettgaia",
    "types": [
      "payload"
    ],
    "name": "The Gaia Space Telescope",
    "scale": "?",
    "parts": "315",
    "designer": "alex.brett",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=93314\">Bricklink</a>, <a href=\"https://ideas.lego.com/projects/8d29e8bc-215c-443b-84e8-2441260d70e9\">Lego Ideas</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCstaniajuno",
    "types": [
      "payload"
    ],
    "name": "Juno Space Probe",
    "scale": "? (~1:40)",
    "parts": "527",
    "designer": "Stania",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/90c88106-1e38-416e-a7b5-631f43a2d946\">LEGO Ideas</a>, <a href=\"https://rebrickable.com/mocs/MOC-7779/Stania/juno-space-probe/\">Rebrickable</a>, <a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=7876\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCperijovejuno",
    "types": [
      "payload"
    ],
    "name": "Mini Juno Spacecraft",
    "scale": "?",
    "parts": "85",
    "designer": "Perijove",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-5184/Perijove/mini-juno-spacecraft/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedjuno",
    "types": [
      "payload"
    ],
    "name": "Juno",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible launcher <a href=\"#MOCluxordeathbedatlasv551\">Atlas V 551</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwxzhrsminijuno",
    "types": [
      "payload"
    ],
    "name": "CUUSOO-Mini Juno Spacecraft",
    "scale": "?",
    "parts": "116",
    "designer": "wxzhrs",
    "file_types": [
      "io"
    ],
    "price": "$1.49",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-26497/wxzhrs/cuusoo-mini-juno-spacecraft/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegobuilder1234junosatellite",
    "types": [
      "payload"
    ],
    "name": "mini Juno satellite",
    "scale": "?",
    "parts": "160",
    "designer": "LegoBuilder1234",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-33010/Legobuilder1234/mini-juno-satellite/#details\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedgalileo",
    "types": [
      "payload"
    ],
    "name": "Galileo and Internal shuttle bay launcher system: IUS launcher and cradle",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/433382443733679/\">Facebook group Bricks in Space</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedcomptonobservatory",
    "types": [
      "payload"
    ],
    "name": "Compton Gamma Ray observatory",
    "scale": "1:110",
    "parts": "29",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/433382443733679/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedchandraobservatory",
    "types": [
      "payload"
    ],
    "name": "Chandra X-Ray observatory",
    "scale": "1:110",
    "parts": "16",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/433382443733679/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedmagellan",
    "types": [
      "payload"
    ],
    "name": "Magellan",
    "scale": "1:110",
    "parts": "16",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/433382443733679/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedulysses",
    "types": [
      "payload"
    ],
    "name": "Ulysses",
    "scale": "1:110",
    "parts": "20",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/433382443733679/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkegalileo",
    "types": [
      "payload"
    ],
    "name": "Galileo Satellit",
    "scale": "?",
    "parts": "515",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=77039\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjoeldenninggalileo",
    "types": [
      "payload"
    ],
    "name": "Galileo space probe",
    "scale": "1:110",
    "parts": "?",
    "designer": "Joel Denning",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871628593242393/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkeenvisat",
    "types": [
      "payload"
    ],
    "name": "Envisat",
    "scale": "?",
    "parts": "1079",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=77654\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkeaquaeospm1",
    "types": [
      "payload"
    ],
    "name": "Aqua (EOS-PM1)",
    "scale": "?",
    "parts": "1015",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=78621\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbednewhorizons",
    "types": [
      "payload"
    ],
    "name": "New horizons",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible launcher <a href=\"#MOCluxordeathbedatlasv551\">Atlas V 551</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkenewhorizons20",
    "types": [
      "payload"
    ],
    "name": "Pluto Probe New Horizons 2.0 with Stand",
    "scale": "?",
    "parts": "305",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=72899\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnumontagnewhorizons",
    "types": [
      "payload"
    ],
    "name": "New Horizons",
    "scale": "?",
    "parts": "331",
    "designer": "nu_montag",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/DoK2wlrMj9k\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzernewhorizons",
    "types": [
      "payload"
    ],
    "name": "New Horizons probe",
    "scale": "?",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/1XjdPBn4j8b\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonnewhorizons",
    "types": [
      "payload"
    ],
    "name": "New Horizons probe",
    "scale": "1:110",
    "parts": "13",
    "designer": "Dan Fallon",
    "file_types": [
      "io",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/new-horizons/\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"https://rebrickable.com/mocs/MOC-43214/phreaddee/new-horizons/\">Older version at Rebrickable</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichnewhorizons",
    "types": [
      "payload"
    ],
    "name": "New Horizons",
    "scale": "1:110",
    "parts": "39",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-47440/MuscoviteSandwich/new-horizons-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"#MOCmuscovitesandwichultimateatlasv\">Compatible rocket</a>. Based on <a href=\"#MOCdanfallonnewhorizons\">New Horizons by Dan Fallon</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedcassinihuygens",
    "types": [
      "payload"
    ],
    "name": "Cassini-Huygens",
    "scale": "1:110",
    "parts": "17",
    "designer": "luxordeathbed",
    "file_types": [
      "lxf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/cassini-huygens/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible launcher <a href=\"#MOCluxordeathbedtitanivb\">Titan IV B</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCqaajakcassinihuygens",
    "types": [
      "payload"
    ],
    "name": "Cassini-Huygens",
    "scale": "?",
    "parts": "257",
    "designer": "qaajak",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=30891\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedviking",
    "types": [
      "payload"
    ],
    "name": "Viking probe and lander",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/viking/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible launcher <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaabfanvikng1lander",
    "types": [
      "payload"
    ],
    "name": "Viking 1 Lander",
    "scale": "?",
    "parts": "118",
    "designer": "saabfan",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/Z79a83bbv8w\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainvikinglander",
    "types": [
      "payload"
    ],
    "name": "Viking lander",
    "scale": "1:110",
    "parts": "?",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903739203364665/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903735103365075/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbrandoncraintitaniiie\">Titan IIIE</a>, <a href=\"#MOCbrandoncraincentaur\">Centaur stage</a>, <a href=\"#MOCbrandoncrainvikingorbiter\">Viking orbiter</a>, <a href=\"#MOCbrandoncrainvoyagerspaceprobe\">Voyager space probe</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainvikingorbiter",
    "types": [
      "payload"
    ],
    "name": "Viking orbiter",
    "scale": "1:110",
    "parts": "?",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903739203364665/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/903735103365075/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCbrandoncraintitaniiie\">Titan IIIE</a>, <a href=\"#MOCbrandoncraincentaur\">Centaur stage</a>, <a href=\"#MOCbrandoncrainvikinglander\">Viking lander</a>, <a href=\"#MOCbrandoncrainvoyagerspaceprobe\">Voyager space probe</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedhelios",
    "types": [
      "payload"
    ],
    "name": "Helios",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible launcher <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkekeplertelescope",
    "types": [
      "payload"
    ],
    "name": "Kepler Telescope",
    "scale": "?",
    "parts": "294",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=75340\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCabubkelunarreconnaissance",
    "types": [
      "payload"
    ],
    "name": "Lunar Reconnaissance Orbiter",
    "scale": "?",
    "parts": "307",
    "designer": "abubke",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=76041\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonpioneer1011",
    "types": [
      "payload"
    ],
    "name": "Pioneer 10/11",
    "scale": "1:110",
    "parts": "25",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/payloads/pioneer-10-11/\">Bricks in Space webpage</a>, <a href=\"https://rebrickable.com/mocs/MOC-43150/phreaddee/pioneer-10-pioneer-11/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketssputnik",
    "types": [
      "payload"
    ],
    "name": "sputnik",
    "scale": "1:110",
    "parts": "172",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=20673\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnathanrsputnik",
    "types": [
      "payload"
    ],
    "name": "Sputnik",
    "scale": "?",
    "parts": "174",
    "designer": "Nathan Readioff",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/nkOjLgdN2x6\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdoctordoomstersputnik1",
    "types": [
      "payload"
    ],
    "name": "Sputnik 1",
    "scale": "?",
    "parts": "?",
    "designer": "Doctor Doomster",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.moc-pages.com/moc.php/326977\">MOCpages</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCluispenasputnik",
    "types": [
      "payload"
    ],
    "name": "Sputnik",
    "scale": "1:12",
    "parts": "?",
    "designer": "Luis Pe\u00f1a Garc\u00eda",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/809402492798337/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/photo.php?fbid=10158354625889841&amp;set=p.10158354625889841&amp;type=3&amp;theater\">Facebook comment with detail of inner construction</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechniekapollosoyuz",
    "types": [
      "payload"
    ],
    "name": "Apollo-Soyuz test project",
    "scale": "1:110",
    "parts": "44",
    "designer": "tech_niek",
    "file_types": [
      "jpg",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/95bdcce7-f7e9-463d-a223-48637b704258\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/485363475202242/\">Facebook post with digital file</a>. Based on <a href=\"#MOCtechnieksoyuzspacecraft\">Soyuz spacecraft</a>, <a href=\"#SET21309-1\">NASA Apollo Saturn V</a> and <a href=\"#MOCeifflemansaturn1b\">Saturn 1B</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollosoyuzdiorama",
    "types": [
      "payload"
    ],
    "name": "Apollo-Soyuz test project",
    "scale": "1:110",
    "parts": "167 - 214",
    "designer": "Adam Wilde, Nico Daams, Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/astp/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/725875714484349/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/723835278021726/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsapollosoyuztestproject",
    "types": [
      "payload"
    ],
    "name": "Apollo-Soyuz Test Project (with display stand)",
    "scale": "1:110",
    "parts": "49",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=40249\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCreprageorbcomm2payload",
    "types": [
      "payload"
    ],
    "name": "SpaceX Orbcomm2 Payload",
    "scale": "1:124",
    "parts": "57",
    "designer": "RepRage",
    "file_types": [
      "lxf",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://reprage.com/post/lego-spacex-orbcomm2\">Home page</a>, <a href=\"https://ideas.lego.com/projects/ba33d753-7be1-4900-99b7-b9522f3dc822\">Lego Ideas</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCrepragefalcon9\">Falcon 9</a>, <a href=\"#MOCrepragedroneship\">Droneship</a>, <a href=\"#MOCrepragedragoncapsule\">Dragon</a>, <a href=\"#MOCrepragedragonv2capsule\">Dragon 2</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCcurtisdforbcomm",
    "types": [
      "payload"
    ],
    "name": "Orbcomm OG-2 Payload",
    "scale": "1:110",
    "parts": "43",
    "designer": "curtisdf",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=33147\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCcurtisdffalcon9\">rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCendregateslaroadster",
    "types": [
      "payload"
    ],
    "name": "Tesla Roadster Payload for Falcon Heavy",
    "scale": "1:110",
    "parts": "23",
    "designer": "endrega",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=32925\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCendregaspacexfalconheavy\">SpaceX Falcon Heavy</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCderpydragonlandermod",
    "types": [
      "payload"
    ],
    "name": "10029 Redesign",
    "scale": "1:35",
    "parts": "741",
    "designer": "derpydragon",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-8472/derpydragon/10029-redesign/\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10029-1\">LEGO set 10029-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichlemmod",
    "types": [
      "payload"
    ],
    "name": "10266 LEM realism modifacations",
    "scale": "1:35",
    "parts": "148",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "png",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-29245/MuscoviteSandwich/10266-lem-realisam-modifacations/\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">LEGO set 10266-1</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/561032664301989/\">Also on Facebook</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildelm5eagle",
    "types": [
      "payload"
    ],
    "name": "Lunar Lander reworked as LM-5 'Eagle'",
    "scale": "1:35",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/720177855054135/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">LEGO set 10266-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichlunarlanderfoldablelegs",
    "types": [
      "payload"
    ],
    "name": "NASA Apollo 11 Lunar Lander foldable landing legs mod",
    "scale": "1:35",
    "parts": "36",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "png"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnicodaamslemworkinghatchmod",
    "types": [
      "payload"
    ],
    "name": "LM realism mod: Inward opening Egress hatch",
    "scale": "1:35",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/731509977254256/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">LEGO set 10266-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmichaelcameronlm5",
    "types": [
      "payload"
    ],
    "name": "Apollo 11 Lunar Lander, the \"Eagle\" (LM-5)",
    "scale": "1:35",
    "parts": "?",
    "designer": "Michael Cameron",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/610681982670390/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCchrisnxlemdiorama",
    "types": [
      "payload"
    ],
    "name": "LEM diorama",
    "scale": "1:35",
    "parts": "2087",
    "designer": "Chris Nx",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/658857351186186/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">NASA Apollo 11 Lunar Lander</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/658856721186249/\">Facebook post with description</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanmodifiedlunarlander",
    "types": [
      "payload"
    ],
    "name": "Apollo 11 Lunar Lander, Modification to Lego Set 10266",
    "scale": "1:35",
    "parts": "?",
    "designer": "Eiffleman, others",
    "file_types": [
      "pdf",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/582749082130347/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">NASA Apollo 11 Lunar Lander</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/583253125413276/\">Facebook post with lxf containing parts needed for modification</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/579029195835669/\">Facebook post with sticker</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/568613093543946/\">Stickers for astronaut backpacks</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/568773510194571/\">Sticker with flag</a>. Facebook post with <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/755608734844380/\">io file with parts</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/755643141507606/\">xml file with parts list</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechniekmodifiedlunarlander",
    "types": [
      "payload"
    ],
    "name": "Aft equipement bay mod of Lunar Lander",
    "scale": "1:35",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/578193635919225/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">NASA Apollo 11 Lunar Lander</a>. Replaces step 166 - 177 in the official set.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClouiskowolowskiapollolunarlander",
    "types": [
      "payload"
    ],
    "name": "Apollo 11 Lunar Lander, Modification to Lego Set 10266",
    "scale": "1:35",
    "parts": "?",
    "designer": "Louis Kowolowski",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/577453499326572/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET10266-1\">NASA Apollo 11 Lunar Lander</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketslm",
    "types": [
      "payload"
    ],
    "name": "Lunar Module",
    "scale": "1:110",
    "parts": "73",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=88807\">Bricklink</a> </p>",
    "notes": "<p>Redesign of <a href=\"#SET21309-1\">SET21309-1</a>. Model version 3.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkurtsmocsapollolunarmodule",
    "types": [
      "payload"
    ],
    "name": "Apollo Lunar Module",
    "scale": "?",
    "parts": "?",
    "designer": "Kurt's MOCs",
    "file_types": [
      "pdf"
    ],
    "price": "CA$4.99",
    "links": "<p><a href=\"http://www.moc-pages.com/moc.php/425996\">MOCpages</a>, <a href=\"https://www.etsy.com/ca/listing/270387836/lego-building-instructions-apollo-lunar?ref=shop_home_active_31\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOColivierrttechniclm",
    "types": [
      "payload"
    ],
    "name": "Lunar Lander, technic",
    "scale": "?",
    "parts": "?",
    "designer": "Olivier RT",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/753999175005336/\">Facebook post with description</a>. Also on <a href=\"https://ideas.lego.com/projects/d1788d13-b1c7-4541-a279-87ab8caf44f4\">LEGO Ideas</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbhsapollo11moonlander",
    "types": [
      "payload"
    ],
    "name": "Apollo 11 Moonlander",
    "scale": "?",
    "parts": "484",
    "designer": "BHs",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-41674/BHs/apollo-11-moonlander/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichminiapollolem",
    "types": [
      "payload"
    ],
    "name": "Mini Apollo Command module and LEM",
    "scale": "1:220",
    "parts": "24",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-50000/MuscoviteSandwich/mini-apollo-command-module-and-lem-iss-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCspacexplorerapollocsmlmlunarstand\">Stand</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOChachiroku24csm",
    "types": [
      "payload"
    ],
    "name": "Apollo 11 Command and Service Module",
    "scale": "1:35",
    "parts": "?",
    "designer": "Hachiroku24",
    "file_types": [
      "video",
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.youtube.com/watch?v=23Ajiy4L4TY\">Youtube</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/848505108888075/\">Conversion into digital io model by Rudi Landmann</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/848417635563489/\">Pdf instructions by Samuel Shuker</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/848890115516241/\">Parts list by Rudi Landmann</a>. Modification by <a href=\"#MOCoverycsm\">Robert Overy</a>. Also recreated on <a href=\"https://www.mecabricks.com/en/models/VzvMrXlgaJp\">Mecabricks by 2.71828182</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCoverycsm",
    "types": [
      "payload"
    ],
    "name": "Modification of Hachiroku24's CSM",
    "scale": "1:35",
    "parts": "?",
    "designer": "Robert Overy",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/935874483484470/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/935874006817851/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOChachiroku24csm\">Hachiroku24's CSM</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuanglunarmodule",
    "types": [
      "payload"
    ],
    "name": "Lunar Module 5 (Apollo 11)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/839294966475756/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET21309-1\">SET21309-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo5diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 5 unmanned Lunar Module LM-1",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-1/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/707573862981201/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Preflight integration into the SLA.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeltabapollo8",
    "types": [
      "payload"
    ],
    "name": "Lunar Module Test Article 'B' dummy mass for Apollo 8",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/762549730816947/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo9diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 9 Lunar Module LM-3 'Spider'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-3-spider/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/709414119463842/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo10diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 10 Lunar Module LM-4 'Snoopy'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-4-snoopy/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/718384711900116/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Includes two versions: full Lunar Module and Ascent Stage only.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo11diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 11 Lunar Module LM-5 'Eagle'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-5-eagle/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/718390135232907/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Landscape not to scale but craters around the LM more or less right, contains EASEP (with LRRR), TV camera, SWC, stereo camera.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo12diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 12 Lunar Module LM-6 'Intrepid'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-6-intrepid/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748054825599771/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Landscape not to scale, contains Surveyor 3, Surveyor Crater, Block Crater and erectable S-band antenna. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748060238932563/\">Facebook post with digital file</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo13diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 13 Lunar Module LM-7 'Aquarius' and damaged CSM-109 'Odyssey\u2019",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-7-aquarius/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/718386045233316/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo14diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 14 Lunar Module LM-8 'Antares'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-8-antares/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/718386045233316/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo15diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 15 Lunar Module LM-10 'Falcon'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-10-falcon/\">Brick in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/716594045412516/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>LRV deployment with Hadley\u2013Apennine backdrop.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo16diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 16 Lunar Module LM-11 'Orion'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-11-orion/\">Brick in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/716594045412516/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>LRV deployment with Hadley\u2013Apennine backdrop.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildeapollo17diorama",
    "types": [
      "payload"
    ],
    "name": "Diorama of Apollo 17 Lunar Module LM-12 'Challenger'",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/apollo-program/lm-12-challenger/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/716602605411660/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Landscape not to scale, contains LRV-3, orange soil at Shorty Crater, jettison bag, and Harrison Schmitt at Split Boulder. The LRV is developed from Damian Bailey's MOC.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnrike89apollolandingsite",
    "types": [
      "payload"
    ],
    "name": "Apollo lunar landing site",
    "scale": "1:110",
    "parts": "42",
    "designer": "nrike89",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-49527/nrike89/apollo-lunar-landing-site/\">Rebrickable</a> </p>",
    "notes": "<p>Lunar Module, astronauts and the flag are not included, they must be taken from the official set <a href=\"#SET21309-1\">21309-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildegrummancontractorsmodel",
    "types": [
      "payload"
    ],
    "name": "Grumman Contractor's Model of the Apollo Lunar Module",
    "scale": "?",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/751750428563544/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732712060467381/\">Facebook post with digital file</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamoblunarrover",
    "types": [
      "payload"
    ],
    "name": "Lunar Rover",
    "scale": "1:110",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/544676829270906/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCvalerierochelunarrover",
    "types": [
      "payload"
    ],
    "name": "Lunar Rover",
    "scale": "1:110",
    "parts": "11",
    "designer": "Valerie Roche",
    "file_types": [
      "lxf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/lunar-rover/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsvenjlunarrovingvehicle",
    "types": [
      "payload"
    ],
    "name": "Micro Lunar Roving Vehicle",
    "scale": "1:110",
    "parts": "10",
    "designer": "Sven J",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-44793/Sven J/micro-lunar-roving-vehicle-for-saturn-v/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanlunarroverandbase",
    "types": [
      "payload"
    ],
    "name": "Apollo Lunar Rover",
    "scale": "?",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "pdf",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/558042754600980/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/560762537662335/\">Facebook post with digital file</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCchrispaynelunarrover",
    "types": [
      "payload"
    ],
    "name": "Lunar Rover",
    "scale": "?",
    "parts": "?",
    "designer": "Chris Spain",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/563118180760104/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/563118324093423/\">Facebook post with parts list</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbonsaikaapollo17lunarrover",
    "types": [
      "payload"
    ],
    "name": "Apollo 17 Lunar Roving Vehicle",
    "scale": "?",
    "parts": "253",
    "designer": "Bonsaika",
    "file_types": [
      "io"
    ],
    "price": "$2.99",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-38097/Bonsaika/apollo-17-lunar-roving-vehicle/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCgothamknightapollolunarrover",
    "types": [
      "payload"
    ],
    "name": "Apollo Lunar Rover",
    "scale": "?",
    "parts": "81",
    "designer": "GothamKnight",
    "file_types": [
      "pdf"
    ],
    "price": "$2.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-36145/GothamKnight/apollo-lunar-rover/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCromainnolunarroverrn",
    "types": [
      "payload"
    ],
    "name": "Lunar Rover",
    "scale": "?",
    "parts": "?",
    "designer": "Romain No",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/178082023364995/permalink/262090611630802/\">Facebook group Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildesurveyor3",
    "types": [
      "payload"
    ],
    "name": "Surveyor 3",
    "scale": "?",
    "parts": "199",
    "designer": "Adam Wilde, Eiffleman",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/627886070949981/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59luna94f",
    "types": [
      "payload"
    ],
    "name": "Luna-9",
    "scale": "?",
    "parts": "205",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/kOjLdNZMjx6\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59luna16",
    "types": [
      "payload"
    ],
    "name": "Luna-16",
    "scale": "?",
    "parts": "859",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/X1k2qx7EaE0\">Mecabricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichhayabusa",
    "types": [
      "payload"
    ],
    "name": "Hayabusa",
    "scale": "1:110",
    "parts": "55",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-40241/MuscoviteSandwich/hayabusa2-saturn-v-scale/?inventory=1#bi\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwells44pegasus",
    "types": [
      "payload"
    ],
    "name": "Pegasus satelite (transformed S-IV stage)",
    "scale": "1:110",
    "parts": "?",
    "designer": "davidwells44",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p>Part of instructions of <a href=\"#MOCdavidwells44saturn1series\">Saturn 1 series</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwellingapollovenusflyby",
    "types": [
      "payload"
    ],
    "name": "Apollo Venus flyby",
    "scale": "1:110",
    "parts": "?",
    "designer": "David Welling",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/608852006186721/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanskylab",
    "types": [
      "payload"
    ],
    "name": "NASA: Skylab",
    "scale": "1:110",
    "parts": "420 (deployed state)",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/space-stations/skylab/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/7d001367-12e0-473e-aee5-e6af83e4e6a6\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/444370359301554/\">Facebook post with digital file</a>. 3 versions: 1-launch configuration, 2-as undamaded, 3-as damaged. Lego Ideas page also contain <a href=\"#MOCeifflemansaturn1b\">Saturn 1B</a> and <a href=\"#MOCeifflemanapollo13\">Apollo 13</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsaxusskylab",
    "types": [
      "payload"
    ],
    "name": "Skylab",
    "scale": "1:110",
    "parts": "416",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.reddit.com/r/lego/comments/6dxi43/saturnskylab_for_21309/\">reddit</a> </p>",
    "notes": "<p><a href=\"http://arrakis.muportal.hu/ldd/rocket-21309-saturn-skylab.lxf\">Instructions</a>, <a href=\"https://imgur.com/a/zbSKq#lbQOPNO\">images on imgur</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordnicodaamsskylab",
    "types": [
      "payload"
    ],
    "name": "Skylab with Apollo",
    "scale": "1:110",
    "parts": "?",
    "designer": "Sam Bradford, Nico Daams",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/723214411417146/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/722777611460826/\">Facebook post with images</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanskylab220",
    "types": [
      "payload"
    ],
    "name": "Skylab",
    "scale": "1:220",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/738024063269514/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCskylabbricksskylab",
    "types": [
      "payload"
    ],
    "name": "Skylab",
    "scale": "1:110",
    "parts": "1426 - 1455",
    "designer": "Sam Bradford, Joseph Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "$10.00",
    "links": "<p><a href=\"https://www.etsy.com/listing/860427153/173-scale-nasa-skylab-lego-model?ref=shop_home_active_3&amp;crt=1\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerskylabcollection",
    "types": [
      "payload"
    ],
    "name": "NASA Apollo Skylab Collection",
    "scale": "1:220",
    "parts": "292",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-51578/SpaceXplorer%2016/nasa-apollo-skylab-collection/\">Rebrickable</a> </p>",
    "notes": "<p>Contains Saturn 1b first stage, Skylab space station, Saturn 1b S-IVB upper stage designed by <a href=\"#MOCmuscovitesandwichmidisaturnv\">Muscovite Sandwich's Midi Saturn V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCramonmontanastarlinksatellite",
    "types": [
      "payload"
    ],
    "name": "Starlink satelite",
    "scale": "?",
    "parts": "?",
    "designer": "Ramon Monti Montana",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/802503320154921/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCOrigamitensaiissbuildingset",
    "types": [
      "payload"
    ],
    "name": "International Space Station (with all modules)",
    "scale": "?",
    "parts": "267",
    "designer": "origamitensai",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-8964/origamitensai/international-space-station-with-all-modules/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>ISS with Cygnus Orb Space x Dragon and HTV. Also on <a href=\"https://ideas.lego.com/projects/6022ca59-d731-4a12-8e07-e793ef911ba3\">Lego Ideas</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCvalerierocheissminifig",
    "types": [
      "payload"
    ],
    "name": "International Space Station Minifig scale",
    "scale": "?",
    "parts": "13561",
    "designer": "Valerie Roche",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss220scale",
    "types": [
      "payload"
    ],
    "name": "International Space Station",
    "scale": "1:220",
    "parts": "631",
    "designer": "Dan Fallon",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCkehu05issvisitingvehicles",
    "types": [
      "payload"
    ],
    "name": "ISS Visiting Vehicles",
    "scale": "1:220",
    "parts": "377",
    "designer": "kehu05",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-39696/kehu05/1220-iss-visiting-vehicles-compatible-with-set-21321/\">Rebrickable</a> </p>",
    "notes": "<p>Improves and complements vehicles in <a href=\"#SET21321-1\">LEGO set 21321-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClocutus666issxtended",
    "types": [
      "payload"
    ],
    "name": "7467 ISS (xtended)",
    "scale": "?",
    "parts": "?",
    "designer": "Locutus 666",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.moc-pages.com/moc.php/388707\">MOCpages</a> </p>",
    "notes": "<p>Extended version of the <a href=\"#SET7467-1\">LEGO set 7467 International Space Station</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanisssolarpanelradiatormod",
    "types": [
      "payload"
    ],
    "name": "ISS Solar Panel Radiator MOD",
    "scale": "1:220",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732931150445472/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/732623727142881/\">Facebook post with description</a>. Modification of <a href=\"#SET21321-1\">ISS set</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanaxionspace",
    "types": [
      "payload"
    ],
    "name": "Axiom Space ISS module",
    "scale": "?",
    "parts": "20",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/721423368262917/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#SET21321-1\">ISS</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnicholastuckerissimprovedrussiansegment",
    "types": [
      "payload"
    ],
    "name": "ISS - improved russian segment",
    "scale": "1:220",
    "parts": "?",
    "designer": "Nicholas Tucker",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/901795370225715/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Improvement of <a href=\"#SET21321-1\">LEGO set 21321-1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveauissbeam",
    "types": [
      "payload"
    ],
    "name": "ISS Beam module",
    "scale": "1:110",
    "parts": "?",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/916119422126643/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildesoyuzprogressimprovement",
    "types": [
      "payload"
    ],
    "name": "Soyuz/Progress improvement of [SET21321-1](#SET21321-1)",
    "scale": "1:220",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/735259740212613/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#SET21321-1\">ISS set</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kmir",
    "types": [
      "payload"
    ],
    "name": "Mir",
    "scale": "1:110",
    "parts": "860",
    "designer": "MoppeW40k",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppethierrymir",
    "types": [
      "payload"
    ],
    "name": "Mir with display stand",
    "scale": "1:110",
    "parts": "?",
    "designer": "MoppeW40k, Thierry",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/745621625843091/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"#MOCmoppew40kmir\">It is modification of MoppeW40k's MIR</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonmir",
    "types": [
      "payload"
    ],
    "name": "Mir",
    "scale": "1:110",
    "parts": "695",
    "designer": "Dan Fallon",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/all/mir/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/461527237585866/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannmir",
    "types": [
      "payload"
    ],
    "name": "Mir",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/910385209366731/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/910384482700137/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59almazspacestation",
    "types": [
      "payload"
    ],
    "name": "Almaz space station",
    "scale": "?",
    "parts": "1437",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/nKZvmXJQjG6\">Mecabricks</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/150957-moc-almaz-space-station/&amp;tab=comments#comment-2803160\">Eurobricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmarkbalderramasalyut",
    "types": [
      "payload"
    ],
    "name": "Salyut 1",
    "scale": "1:110",
    "parts": "134",
    "designer": "Mark Balderrama",
    "file_types": [
      "io",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/space-stations/salyut-1/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/443815949356995/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCXsalyut",
    "types": [
      "payload"
    ],
    "name": "Salyut",
    "scale": "1:110",
    "parts": "15 XXX",
    "designer": "?",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40ksalyut1",
    "types": [
      "payload"
    ],
    "name": "Saluyt 1",
    "scale": "1:110",
    "parts": "202",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/all/salyut-1-2/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsalyut1",
    "types": [
      "payload"
    ],
    "name": "Salyut 1",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/899272357144683/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With Soyuz 10 or 11. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/899272053811380/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsalyut2",
    "types": [
      "payload"
    ],
    "name": "Salyut 2",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/901307046941214/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With Soyuz 7K-T.  <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/901306646941254/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsalyut4",
    "types": [
      "payload"
    ],
    "name": "Salyut 4",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/902900940115158/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With Soyuz 7K-T.  <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/902900300115222/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40ksalyut6",
    "types": [
      "payload"
    ],
    "name": "Salyut 6",
    "scale": "1:110",
    "parts": "178",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/space-stations/salyut-6/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsalyut6",
    "types": [
      "payload"
    ],
    "name": "Salyut 6",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/904086406663278/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With Soyuz 7K-T, Soyuz T, Progress 7K-TG, and TKS-2.  <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/904085743330011/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonsalyut7",
    "types": [
      "payload"
    ],
    "name": "Salyut 7 (with Kosmos 1686 / Soyuz)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/space-stations/salyut-7-with-kosmos-1686-soyuz/\">Bricks in Space webpage</a> </p>",
    "notes": "<p><a href=\"#MOCtechnieksalyutkosmossoyuz\">Modified version</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieksalyutkosmossoyuz",
    "types": [
      "payload"
    ],
    "name": "Salyut 7 (with Kosmos 1686 / Soyuz)",
    "scale": "1:110",
    "parts": "336",
    "designer": "tech_niek",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/all/salyut-7-with-kosmos-1686-soyuz-2/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>Based on <a href=\"#MOCmoppew40ksalyut1\">Saluyt 1 by MoppeW40k</a> and on <a href=\"#MOCdanfallonsalyut7\">Salyut 7 by Dan Fallon</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrudilandmannsalyut7",
    "types": [
      "payload"
    ],
    "name": "Salyut 7",
    "scale": "1:220",
    "parts": "?",
    "designer": "Rudi Landmann",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/906261183112467/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>With Soyuz 7K-T, Soyuz T, Progress 7K-TG, TKS-3, and TKS-4.  <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/906260256445893/\">Facebook post with images and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59soyuz7kt",
    "types": [
      "payload"
    ],
    "name": "Soyuz 7\u041a-\u0422",
    "scale": "?",
    "parts": "488",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/en/models/LGVjKGbAjnz\">Mecabricks</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/150762-moc-soyuz-7k-t/&amp;tab=comments#comment-2800084\">Eurobricks</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnicomoppew40kzarya",
    "types": [
      "payload"
    ],
    "name": "Zarya",
    "scale": "1:110",
    "parts": "166",
    "designer": "Nico, MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtechniekvostokcapsule",
    "types": [
      "payload"
    ],
    "name": "Vostok capsule",
    "scale": "1:110",
    "parts": "12",
    "designer": "tech_niek",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/488438594894730/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCsambradfordr7vostok\">Vostok</a> and <a href=\"#MOCsambradfordr7voskhod\">Voskhod</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichx37b",
    "types": [
      "payload"
    ],
    "name": "X-37b Spaceplane",
    "scale": "1:110",
    "parts": "35",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-30848/MuscoviteSandwich/x-37b-space-plane-saturn-v-scale/\">Rebrickable</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCblockheedx37b",
    "types": [
      "payload"
    ],
    "name": "X-37B Orbital Test Vehicle",
    "scale": "?",
    "parts": "73",
    "designer": "Blockheed",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=9202\">Bricklink</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmoppew40kcss",
    "types": [
      "payload"
    ],
    "name": "Chinese modular space station, Tianhe, Wentian and Mengtian modules",
    "scale": "1:110",
    "parts": "646",
    "designer": "MoppeW40k",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/space-stations/chinese-space-station/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/780534345685152/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCfelix1999spacestationspacecrafts",
    "types": [
      "payload"
    ],
    "name": "Space Station and Spacecrafts",
    "scale": "1:220",
    "parts": "484",
    "designer": "felix_1999",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-37620/felix_1999/21321-space-station-and-spacecrafts/\">Rebrickable</a> </p>",
    "notes": "<p>Alternative build to <a href=\"#SET21321-1\">ISS Set 21321</a>. Contains builds of: MIR, Salyut 6, Salyut 7, Skylab, Space Shuttle Atlantis, Apollo 11 Lunar Lander, Mars Rover (Opportunity, Spirit, Curiosity), Apollo CSM, Kosmos 1686 TKS Spacecraft, Progress M-34, Goldstone Deep Space Communications Complex, Cosmonauts.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzerminiaturespacestations",
    "types": [
      "payload"
    ],
    "name": "US, Chinese, and Soviet/Russian Miniature Space Stations",
    "scale": "?",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/eDvVrMG8vBR\">Mecabricks</a> </p>",
    "notes": "<p>Contains: Saljut 1, 2, 3, 4, 5, 6, 7, 7 &amp; Kosmos 1686, Soyuz TMA, Mir &amp; Atlantis, Soyuz &amp; Apollo, Chinese Space Station, ISS, Skylab.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzerprobesandtelescopes",
    "types": [
      "payload"
    ],
    "name": "Probes and Telescopes",
    "scale": "?",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/79a8yXKq28w\">Mecabricks</a> </p>",
    "notes": "<p>Contains: James Webb Space Telescope, Parker Solar Probe, Ranger 6, SMART-1, Venera 9, Pioneer 10, MAVEN, Chandra X-Ray Observatory, Spitzer Space Telescope, Juno Spacecraft, Galileo, Curiosity Aeroshell.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleyaxh1",
    "types": [
      "payload"
    ],
    "name": "Axiom Habitation Module",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871701553235097/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxn1\">Node</a>, <a href=\"#MOCtrombleyaxptm\">Power and Thermal</a>, <a href=\"#MOCtrombleyaxrmf\">Research</a>, <a href=\"#MOCtrombleycupolaclosed\">Cupola closed</a>, <a href=\"#MOCtrombleycupolaopen\">Cupola open</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleyaxn1",
    "types": [
      "payload"
    ],
    "name": "Axiom Node Module",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871701383235114/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxh1\">Habitation</a>, <a href=\"#MOCtrombleyaxptm\">Power and Thermal</a>, <a href=\"#MOCtrombleyaxrmf\">Research</a>, <a href=\"#MOCtrombleycupolaclosed\">Cupola closed</a>, <a href=\"#MOCtrombleycupolaopen\">Cupola open</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleyaxptm",
    "types": [
      "payload"
    ],
    "name": "Axiom Power and Thermal Module",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871701866568399/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxh1\">Habitation</a>, <a href=\"#MOCtrombleyaxn1\">Node</a>, <a href=\"#MOCtrombleyaxrmf\">Research</a>, <a href=\"#MOCtrombleycupolaclosed\">Cupola closed</a>, <a href=\"#MOCtrombleycupolaopen\">Cupola open</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleyaxrmf",
    "types": [
      "payload"
    ],
    "name": "Axiom Research Module",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871701693235083/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxh1\">Habitation</a>, <a href=\"#MOCtrombleyaxn1\">Node</a>, <a href=\"#MOCtrombleyaxptm\">Power and Thermal</a>, <a href=\"#MOCtrombleycupolaclosed\">Cupola closed</a>, <a href=\"#MOCtrombleycupolaopen\">Cupola open</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleycupolaclosed",
    "types": [
      "payload"
    ],
    "name": "Axiom Cupola Module - Closed",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871702436568342/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxh1\">Habitation</a>, <a href=\"#MOCtrombleyaxn1\">Node</a>, <a href=\"#MOCtrombleyaxptm\">Power and Thermal</a>, <a href=\"#MOCtrombleycupolaopen\">Cupola open</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtrombleycupolaopen",
    "types": [
      "payload"
    ],
    "name": "Axiom Cupola Module - Open",
    "scale": "1:110",
    "parts": "?",
    "designer": "Jonathan Trombley",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/872331966505389/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Axiom Station. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/871697043235548/\">Facebook post with description</a>. Other modules: <a href=\"#MOCtrombleyaxh1\">Habitation</a>, <a href=\"#MOCtrombleyaxn1\">Node</a>, <a href=\"#MOCtrombleyaxptm\">Power and Thermal</a>, <a href=\"#MOCtrombleycupolaclosed\">Cupola closed</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorergateway",
    "types": [
      "payload"
    ],
    "name": "Mini Lunar Gateway Space Station",
    "scale": "?",
    "parts": "78",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-58344/SpaceXplorer%2016/mini-lunar-gateway-space-station/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbrickbrothers-germanysaturn5display",
    "types": [
      "other"
    ],
    "name": "Saturn V Display",
    "scale": "1:110",
    "parts": "623",
    "designer": "Brickbrothers-Germany",
    "file_types": [
      "pdf"
    ],
    "price": "$5.00",
    "links": "<p>~~<a href=\"https://rebrickable.com/mocs/MOC-21396/Brickbrothers-Germany/saturn-v-display/\">Rebrickable</a>~~ </p>",
    "notes": "<p>~~Stand for <a href=\"#SET21309-1\">LEGO set 21309-1</a>~~. MOC was removed from Rebrickable.</p>",
    "redacted": true
  },
  {
    "entry_id": "MOCjkbrickworkssaturnplatform",
    "types": [
      "other"
    ],
    "name": "Saturn V Launch Platform",
    "scale": "1:110",
    "parts": "122",
    "designer": "JKBrickworkds",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-8497/JKBrickworks/saturn-v-launch-platform/\">Rebrickable</a> </p>",
    "notes": "<p>Stand for <a href=\"#SET21309-1\">LEGO set 21309-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildesaturnvstand",
    "types": [
      "other"
    ],
    "name": "Saturn V Cruciform Display Stand",
    "scale": "1:110",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/932084943863424/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/932085880529997/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Based on stand by <a href=\"#MOCjkbrickworkssaturnplatform\">JKBrickworks</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCpioneer4x4displaystand",
    "types": [
      "other"
    ],
    "name": "Saturn V display stand for 21309",
    "scale": "1:110",
    "parts": "53",
    "designer": "Pioneer4x4",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-19170/Pioneer4x4/saturn-v-display-stand-for-21309/\">Rebrickable</a>, <a href=\"https://www.bricklink.com/v3/studio/public_gallery.page?idUser=458421\">Bricklink</a> </p>",
    "notes": "<p>Stand for <a href=\"#SET21309-1\">LEGO set 21309-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCignatius666stand",
    "types": [
      "other"
    ],
    "name": "Stand for Saturn V 21309",
    "scale": "1:110",
    "parts": "563",
    "designer": "Ignatius666",
    "file_types": [
      "pdf"
    ],
    "price": "$2.50",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-22033/Ignatius666/stand-for-saturn-v-21309/\">Rebrickable</a> </p>",
    "notes": "<p>Stand for <a href=\"#SET21309-1\">LEGO set 21309-1</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwhatsuptodaysaturnvstand",
    "types": [
      "other"
    ],
    "name": "Saturn V stand",
    "scale": "1:110",
    "parts": "80 - 87",
    "designer": "whatsuptoday",
    "file_types": [
      "html"
    ],
    "price": "0",
    "links": "<p><a href=\"http://whatsuptoday.free.fr/Downloads/21309-Saturn-V-Stands-building-instructions.zip\">whatsuptoday.free.fr</a> </p>",
    "notes": "<p>Link at <a href=\"https://ideas.lego.com/projects/a88109ec-9970-4fe1-98b4-9bd535856ab4/updates\">Lego Ideas page, updates section</a>. Two colour version available.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbrandoncrainsaturnvplatform",
    "types": [
      "other"
    ],
    "name": "Saturn V Launch Platform Display",
    "scale": "1:110",
    "parts": "387",
    "designer": "Brandon Crain",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/842358216169431/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/842819666123286/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemancrawler",
    "types": [
      "other"
    ],
    "name": "NASA Crawler-Transporter",
    "scale": "1:110",
    "parts": "2202",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/nasa-crawler/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/504379736633949/\">Facebook post with digital file</a>. Instructions shown in updates of Lego Ideas page. Also contains: <a href=\"#MOCeifflemanastrovan\">Astrovan</a>, <a href=\"#MOCeifflemanmqf\">Mobile Quarantine Facility</a>, <a href=\"#MOCeifflemanemdsw8train\">EMD SW-8 train</a>, <a href=\"#MOCeifflemanlittlejoeii\">Little Joe II</a>, <a href=\"#MOCeifflemansaturnskylab\">conversion of Saturn V for Skylab mission</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCthierrybroyllecrawler",
    "types": [
      "other"
    ],
    "name": "NASA Crawler-Transporter with elevating function",
    "scale": "1:110",
    "parts": "?",
    "designer": "Thierry Broylle",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/734570876948166/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemancrawlershuttle",
    "types": [
      "other"
    ],
    "name": "NASA Crawler-Transporter for Space Shuttle MLP",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/438752759863314/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a> with the addition of the gantry to link up the Crawler to the Space Shuttle's MLP. The original Saturn 5 ladders remained at the other end of the crawler. The Two side gantries were lowered to the ground and used to access the Crawler.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCheinzsteinercrawlershuttlemotor",
    "types": [
      "other"
    ],
    "name": "Motorized Crawler-Transporter Shuttle era",
    "scale": "1:110",
    "parts": "?",
    "designer": "Heinz Steiner",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/780564042348849/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Description in <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/779992192406034/\">Facebook group Bricks in Space</a>. Based on designs by Eiffleman and Joseph Chambers.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCozshancrawler",
    "types": [
      "other"
    ],
    "name": "NASA Crawler Transporter undercarriage",
    "scale": "?",
    "parts": "?",
    "designer": "OzShan",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/108590-wip-150-nasa-crawler-transporter/&amp;page=3\">Eurobricks</a> </p>",
    "notes": "<p>Author built whole crawler transporter but only undercarriage is available as digital file.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjosephchamberscrawler",
    "types": [
      "other"
    ],
    "name": "NASA Apollo Era Crawler Transporter",
    "scale": "1:110",
    "parts": "2362",
    "designer": "Joseph Chambers",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/739649443106976/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1rpmFoIxpQoUdeYk7_XHIAhow6qALoQwK\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"#MOCjosephchamberslut\">Compatible LUT</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCvintzbricks68lutlegs",
    "types": [
      "other"
    ],
    "name": "Legs for Launch Umbilical Tower Saturn V",
    "scale": "1:110",
    "parts": "170",
    "designer": "vintzbrick68",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/946184629120122/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCnathanrlut",
    "types": [
      "other"
    ],
    "name": "Apollo 11 Launch Umbilical Tower and Mobile Launch Platform (MLP)",
    "scale": "1:110",
    "parts": "8189",
    "designer": "Nathan Readioff",
    "file_types": [
      "lxf",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. III</a>, <a href=\"https://bricksin.space/launch-pads/launch-complex-39a/\">Bricks in Space webpage</a>, <a href=\"https://www.bricksafe.com/pages/NathanR/real-space/apollo-lut\">Bricksafe</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/154148-moc-apollo-launch-tower/\">Eurobricks</a>, <a href=\"https://www.mecabricks.com/en/models/zOjo8Vppv6Z\">Mecabricks</a>, <a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=86126\">Bricklink</a> </p>",
    "notes": "<p>Compatible with <a href=\"#SET21309-1\">NASA Apollo Saturn V</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrochekremerlut",
    "types": [
      "other"
    ],
    "name": "Apollo 11 Launch Umbilical Tower and Mobile Launch Platform (MLP)",
    "scale": "1:110",
    "parts": "3046",
    "designer": "Valerie Roche, Greg Kremer",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://1drv.ms/u/s!AoSq7VrbcK1Nl_55JHbXDPq-uajmPA?e=rRUNoW\">OneDrive, version 10</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/787998284938758/\">Facebook group Bricks in Space - update for version 11</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/726473117757942/\">Facebook post with description and version 10 pdf</a>. Compatible with <a href=\"#SET21309-1\">NASA Apollo Saturn V</a>. Modification of Valerie Roche LUT by Greg Kremer. Version 11. <a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=31343\">Bricklink - incorrect author, old version</a>. <a href=\"#MOCalexanderehlelutrp1\">Compatible RP1</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOChendrickslutrochekremermod",
    "types": [
      "other"
    ],
    "name": "Modification of LUT by Roche, Kremer",
    "scale": "1:110",
    "parts": "?",
    "designer": "Rayman Hendricks",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/756299081442012/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of hold down arms to decrease price of LUT, for version 10 of LUT.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCalexanderehlelutrp1",
    "types": [
      "other"
    ],
    "name": "LUT RP1 (Fuel System Service Tower)",
    "scale": "1:110",
    "parts": "?",
    "designer": "Alexander Ehle",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/858121094593143/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/857414011330518/\">Facebook post with picture</a>. Addition for <a href=\"#MOCrochekremerlut\">this LUT</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCbaileyfullartonlut",
    "types": [
      "other"
    ],
    "name": "Launch Umbilical Tower",
    "scale": "1:110",
    "parts": "?",
    "designer": "Bailey Fullarton",
    "file_types": [
      "?"
    ],
    "price": "$10",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/621017088303546/\">Facebook</a> </p>",
    "notes": "<p>One have to ask Bailey Fullarton on Facebook for the instructions. Compatible with <a href=\"#SET21309-1\">NASA Apollo Saturn V</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjosephchamberslut",
    "types": [
      "other"
    ],
    "name": "Launch Umbilical Tower",
    "scale": "1:110",
    "parts": "8200 - 8500",
    "designer": "Joseph Chambers",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "$15",
    "links": "<p><a href=\"https://www.flickr.com/photos/162862975@N08/sets/72157710329680076/with/40826157703/\">Flickr</a>, <a href=\"mailto:LegoJoe109@gmail.com\">email</a> </p>",
    "notes": "<p>2 versions: for Apollo and Skylab missions. Compatible with <a href=\"#SET21309-1\">NASA Apollo Saturn V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmichaelcameronlut",
    "types": [
      "other"
    ],
    "name": "Launch Umbilical Tower",
    "scale": "1:110",
    "parts": "5130",
    "designer": "Michael Cameron",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/459046757833914/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/media/set/?set=oa.456987081373215\">Facebook post with pictures</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCskylabbrickslut",
    "types": [
      "other"
    ],
    "name": "Lego Saturn V Launch Umbilical Tower",
    "scale": "1:110",
    "parts": "?",
    "designer": "Sam Bradford, Joseph Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "$25",
    "links": "<p><a href=\"https://www.etsy.com/listing/872929174/lego-saturn-v-launch-umbilical-tower?ref=shop_home_active_1&amp;crt=1\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjanotechniclut",
    "types": [
      "other"
    ],
    "name": "Launch Tower Mk I for Saturn V with Crawler",
    "scale": "1:110",
    "parts": "7706",
    "designer": "Janotechnic",
    "file_types": [
      "pdf"
    ],
    "price": "$25",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-60088/Janotechnic/launch-tower-mk-i-for-saturn-v-2130992176-with-crawler/\">Rebrickable</a> </p>",
    "notes": "<p>Technic style LUT. Compatible with <a href=\"#SET21309-1\">NASA Apollo Saturn V</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwellinglaunchcomplex34",
    "types": [
      "other"
    ],
    "name": "Launch Complex 34",
    "scale": "1:110",
    "parts": "3396",
    "designer": "David Welling, Nico Daams",
    "file_types": [
      "pdf",
      "io",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/launch-complex-34/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/750984728640114/\">Facebook group Bricks in Space, part I</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/750988301973090/\">Facebook group Bricks in Space, part II</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/476433696095220/\">Facebook post with digital file, OLD version</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/512835325788390/\">Facebook post with OLD version</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCthierrybroyellelaunchcomplex34",
    "types": [
      "other"
    ],
    "name": "Launch Complex 34",
    "scale": "1:110",
    "parts": "?",
    "designer": "Thierry Broyelle",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/934503630288222/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCdavidwellinglaunchcomplex34\">David Welling's and Nico Daams's LC34</a>. Changes: modified base, umbilical tower, added pillars, waterfall pipes, ECS building, modified railway, flame deflector. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdavidwellingmilkstool",
    "types": [
      "other"
    ],
    "name": "Launchpad 39 \"Milkstool\"",
    "scale": "1:110",
    "parts": "1098",
    "designer": "David Welling",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a>, <a href=\"https://bricksin.space/launch-pads/milkstool/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/514658048939451/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Designed for LUT by Valerie Roche and <a href=\"#MOCdavidwells44saturn1bseries\">Saturn 1B series</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkingsknightlaunchpad",
    "types": [
      "other"
    ],
    "name": "Space Shuttle partiall launch pad",
    "scale": "1:110",
    "parts": "432",
    "designer": "KingsKnight",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/space-shuttle/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/404057999999457/\">Facebook group Bricks in Space</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/152554-moc-lego-ideas-space-shuttle-saturn-v-scale/\">Eurobricks</a> </p>",
    "notes": "<p>Instructions on <a href=\"https://drive.google.com/file/d/1BwXVsjiAiOqNHo2Bfy3Bz9UIFvlFTNYa/view\">AB's Google drive</a>. Compatible <a href=\"#MOCkingsknightspaceshuttle\">space shuttle</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanspaceshuttlemlp",
    "types": [
      "other"
    ],
    "name": "Space Shuttle Mobile Launch Platform",
    "scale": "1:110",
    "parts": "2053",
    "designer": "Eiffleman",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/space-shuttle-mobile-launcher-platform/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/489124978159425/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"#MOCkingsknightspaceshuttle\">Compatible Space Shuttle</a>. <a href=\"#MOCeifflemancrawler\">Compatible crawler</a>. Compatible modification <a href=\"#MOCeifflemanspaceshuttlemlpmodbaffle\">with water baffles</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanthierrybroyellemlpsts2",
    "types": [
      "other"
    ],
    "name": "Space Shuttle Mobile Launch Platform for STS-2",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman, Thierry Broyelle",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/762449250826995/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCeifflemanspaceshuttlemlp\">Eiffleman's MLP</a> for STS-2 Columbia launch, <a href=\"#MOCkingsknightspaceshuttle\">Kingsknight's shuttle</a>. 90 % derived from Eiffleman's one. Changed length, size of SRBs trench, strenghted pod interface, changed waterfall pipes, pipes on the sides.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanspaceshuttlemlpmodbaffle",
    "types": [
      "other"
    ],
    "name": "Water Baffle addition for the Space Shuttle MLP",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/779200772485176/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/779031792502074/\">Facebook post with description</a>. <a href=\"#MOCeifflemanspaceshuttlemlp\">Compatible MLP</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsdroneship",
    "types": [
      "other"
    ],
    "name": "SpaceX Autonomous Droneship JRTI",
    "scale": "1:110",
    "parts": "1558",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$3.34",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-22227/MOCrockets/spacex-autonomous-droneship-jrti-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCbryantobiasdroneship",
    "types": [
      "other"
    ],
    "name": "Space X drone ship \"Of Course I Still Love You\"",
    "scale": "?",
    "parts": "2029",
    "designer": "Bryan R. Tobias",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/418941591844431/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Logo added by Eiffleman. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/418532591885331/\">Original facebook post by Bryan R. Tobias</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCrepragedroneship",
    "types": [
      "other"
    ],
    "name": "SpaceX Autonomous Spaceport Drone Ship",
    "scale": "1:124",
    "parts": "909",
    "designer": "RepRage",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://reprage.com/post/lego-spacex-drone-ship\">Home page</a>, <a href=\"https://ideas.lego.com/projects/ba33d753-7be1-4900-99b7-b9522f3dc822\">Lego Ideas</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCrepragefalcon9\">Falcon 9</a>, <a href=\"#MOCrepragedragoncapsule\">Dragon</a>, <a href=\"#MOCrepragedragonv2capsule\">Dragon 2</a>, <a href=\"#MOCreprageorbcomm2payload\">Orbcomm payload</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichdeltaiilaunch",
    "types": [
      "other"
    ],
    "name": "Delta II launch tower",
    "scale": "1:110",
    "parts": "271",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-28333/MuscoviteSandwich/delta-ii-launch-tower-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCmuscovitesandwichdeltaii\">Delta II</a> and <a href=\"#MOCjohanbromandeltaii\">improved Delta II</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmocrocketsmecuryatlaslaunchpad",
    "types": [
      "other"
    ],
    "name": "Mercury Atlas launch pad and tower",
    "scale": "1:110",
    "parts": "306",
    "designer": "MOCrockets",
    "file_types": [
      "pdf"
    ],
    "price": "$2.22",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-17562/MOCrockets/mercury-atlas-launch-pad-and-tower-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCmocrocketsmecuryatlas\">Mercury Atlas</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOClegorocketsmercuryredstonestand",
    "types": [
      "other"
    ],
    "name": "Mercury Redstone display stand",
    "scale": "1:110",
    "parts": "?",
    "designer": "legorockets",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=37869\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOClegorocketsmercuryredstone\">rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsmokinjoe79redstonelaunchpad",
    "types": [
      "other"
    ],
    "name": "Redstone Launch Pad",
    "scale": "1:110",
    "parts": "269",
    "designer": "smokinjoe79",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=64778\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOClegorocketsmercuryredstone\">rocket</a>. Previous version <a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=62906\">Bricklink</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonlaunchcomplex5",
    "types": [
      "other"
    ],
    "name": "Launch Complex 5",
    "scale": "1:110",
    "parts": "461",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/lc-5/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanmercuryredstonelaunchset",
    "types": [
      "other"
    ],
    "name": "NASA Mercury-Redstone Launch Set with Launch Pad 5",
    "scale": "1:110",
    "parts": "900",
    "designer": "Eiffleman",
    "file_types": [
      "pdf",
      "jpg",
      "io",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/mercury-redstone-set/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/6d8550be-a281-43db-87ac-678681954f89\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/501235960281660/\">Facebook post with pdf</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/501236696948253/\">facebook post with digital file</a>. Compatible <a href=\"#MOCeifflemanmercuryredstone\">rocket</a>. Contains Launch Tower, a Redstone Booster transporter, a tractor and trolley for the Mercury Capsule, astronaut Transfer Van (TV No.1) and Reo tractor and the Mobile Pad Egress Tower (Cherry Picker). Compatible <a href=\"#MOCtechnieklc5base\">base</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCtechnieklc5base",
    "types": [
      "other"
    ],
    "name": "Display base for LC5 gantry by Eiffleman",
    "scale": "1:110",
    "parts": "?",
    "designer": "tech_niek",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/720171831721404/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCeifflemanmercuryredstonelaunchset\">launchpad</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonlaunchcomplex14",
    "types": [
      "other"
    ],
    "name": "Launch Complex 14",
    "scale": "1:110",
    "parts": "4055",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/lc-14/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonlaunchcomplex19",
    "types": [
      "other"
    ],
    "name": "Launch Complex 19",
    "scale": "1:110",
    "parts": "6602",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/launch-complex-19/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCskylabbrickslc39",
    "types": [
      "other"
    ],
    "name": "Apollo Era Launch Complex 39",
    "scale": "1:110",
    "parts": "?",
    "designer": "Sam Bradford, Joseph Chambers",
    "file_types": [
      "pdf"
    ],
    "price": "$15.00",
    "links": "<p><a href=\"https://www.etsy.com/listing/922107940/launch-complex-39-lego-instructions?ref=shop_home_active_2&amp;crt=1\">Etsy</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmikaellindberglc39shuttleera",
    "types": [
      "other"
    ],
    "name": "Launch Complex 39, Shuttle Era",
    "scale": "1:110",
    "parts": "12513",
    "designer": "Mikael Lindberg",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/launch-complex-39a-shuttle-era/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/715366462201941/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordlaunchcomplex39a",
    "types": [
      "other"
    ],
    "name": "Launch Complex 39 A",
    "scale": "1:110",
    "parts": "?",
    "designer": "Sam Bradford",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/499495137122409/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/499495353789054/\">Facebook post with digital file</a>. Compatible <a href=\"#MOCendregaspacexfalconheavy\">SpaceX Falcon Heavy</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordlaunchcomplex39afull",
    "types": [
      "other"
    ],
    "name": "Full Launch Complex 39 A with Falcon 9 Block 5 and Dragon",
    "scale": "1:110",
    "parts": "9283",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/space-x-launch-complex-39a/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCcurtisdffalcon9launchpad",
    "types": [
      "other"
    ],
    "name": "SpaceX Launch Pad Fixture for Falcon 9",
    "scale": "1:110",
    "parts": "371",
    "designer": "curtisdf",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=33145\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCcurtisdffalcon9\">rocket</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCcurtisdffalconheavylaunchpad",
    "types": [
      "other"
    ],
    "name": "SpaceX Launch Pad Fixture for Falcon Heavy",
    "scale": "1:110",
    "parts": "371",
    "designer": "curtisdf",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.bricklink.com/v3/studio/design.page?idModel=33145\">Bricklink</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCcurtisdffalconheavy\">rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedlaunchpad41",
    "types": [
      "other"
    ],
    "name": "Launch Pad 41",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/80cada46-605c-4380-8598-a4a85d94f135\">Lego Ideas</a> </p>",
    "notes": "<p>Launchpad, two <a href=\"#MOCluxordeathbedemdsw8train\">trains</a>, rails. Compatible <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE</a>, <a href=\"#MOCluxordeathbedtitanivb\">Titan IVB</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlersoyuzlaunchpad",
    "types": [
      "other"
    ],
    "name": "Soyuz launchpad",
    "scale": "?",
    "parts": "1731",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://imgur.com/a/AWZSm\">Imgur</a>, <a href=\"http://arrakis.muportal.hu/ldd/soyuz.zip\">http</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCandorschindlersoyuztransportererector\">transporter</a>, <a href=\"#MOCandorschindlersoyuz21v\">Soyuz 2-1v</a>, <a href=\"#MOCandorschindlersoyuzfg\">Soyuz-FG</a> and <a href=\"#MOCandorschindlersoyuzuprogress\">Soyuz U</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCandorschindlersoyuztransportererector",
    "types": [
      "other"
    ],
    "name": "Soyuz transporter and erector train",
    "scale": "?",
    "parts": "1790",
    "designer": "Andor Schindler",
    "file_types": [
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://imgur.com/a/AWZSm\">Imgur</a>, <a href=\"http://arrakis.muportal.hu/ldd/soyuz.zip\">http</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCandorschindlersoyuzlaunchpad\">launchpad</a>, <a href=\"#MOCandorschindlersoyuz21v\">Soyuz 2-1v</a>, <a href=\"#MOCandorschindlersoyuzfg\">Soyuz-FG</a> and <a href=\"#MOCandorschindlersoyuzuprogress\">Soyuz U</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonlaunchpadinterface",
    "types": [
      "other"
    ],
    "name": "Proton launch pad - Launch Vehicle Interface",
    "scale": "1:110",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/787268091678444/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/787272551677998/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Other parts: <a href=\"#MOCdamobprotonlaunchpadtransport\">Erector Unit</a>. <a href=\"#MOCdamobprotontrain\">Full train</a>. Compatible rockets: <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonlaunchpadtransport",
    "types": [
      "other"
    ],
    "name": "Proton Launch pad - Transport and Erector Unit",
    "scale": "1:110",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/812278295844090/\">Facebook group Bricks in Space, io</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/812281842510402/\">Facebook group Bricks in Space, pdf</a> </p>",
    "notes": "<p>Other parts: <a href=\"#MOCdamobprotonlaunchpadinterface\">Launch Vehicle Interface</a>. <a href=\"#MOCdamobprotontrain\">Full train</a>. Compatible rockets: <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotonlaunchpad",
    "types": [
      "other"
    ],
    "name": "Proton Launch pad",
    "scale": "1:110",
    "parts": "?",
    "designer": "DamoB",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/925480171190568/\">Facebook group Bricks in Space, io</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/837812529957333/\">Facebook group Bricks in Space, pdf</a> </p>",
    "notes": "<p>Other parts: <a href=\"#MOCdamobprotonlaunchpadtransport\">Erector Unit</a>. <a href=\"#MOCdamobprotontrain\">Full train</a>. <a href=\"#MOCdamobprotonlaunchpadinterface\">Launch Vehicle Interface</a> is included in this MOC. Compatible rockets: <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdamobprotontrain",
    "types": [
      "other"
    ],
    "name": "Proton train",
    "scale": "1:110",
    "parts": "1104",
    "designer": "DamoB",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/proton-carrier-train/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/827913607613892/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/827923894279530/\">Facebook post with description</a>. Compatible <a href=\"#MOCdamobprotonlaunchpad\">launchpad</a>. Compatible rockets: <a href=\"#MOCdamobur500\">UR-500 </a>, <a href=\"#MOCdamobprotonk\">Proton-K </a>, <a href=\"#MOCdamobprotonkzarya\">Proton-K Zarya</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCfavellx15",
    "types": [
      "other"
    ],
    "name": "X-15, X-15A-2",
    "scale": "1:110",
    "parts": "80 (X-15), 105 (X-15A-2)",
    "designer": "Dan Favell, tech_niek, Eiffleman",
    "file_types": [
      "io",
      "jpg",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/781361655602421/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Instructions are inside of io file, only export to pdf is needed. Older versions without instructions are: <a href=\"https://bricksin.space/support-vehicles/x-15/\">Brick in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/436490330089557/\">Facebook group Bricks in Space</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>. More scale accurate modification <a href=\"#MOCkevinhuangx15\">by Kevin Huang</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangx15",
    "types": [
      "other"
    ],
    "name": "X-15",
    "scale": "1:110",
    "parts": "?",
    "designer": "Kevin Huang",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/797484073990179/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCfavellx15\">X-15 by Dan Favell, tech_niek, Eiffleman, Johan Broman</a> - changed the length and wingspan to be more scale accurate, cockpit.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjustinmanternachx15a2",
    "types": [
      "other"
    ],
    "name": "X-15A-2",
    "scale": "1:35",
    "parts": "?",
    "designer": "Justin Manternach",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/780783698993550/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCyitzykasowitzx15",
    "types": [
      "other"
    ],
    "name": "X-15 Rocket Plane",
    "scale": "1:35",
    "parts": "467",
    "designer": "Yitzy Kasowitz",
    "file_types": [
      "pdf"
    ],
    "price": "$182",
    "links": "<p><a href=\"https://www.brickmania.com/x-15-rocket-plane/\">Brickmania</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCtechniekbellx1",
    "types": [
      "other"
    ],
    "name": "Bell X-1",
    "scale": "1:110",
    "parts": "31",
    "designer": "tech_niek",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/668076896930898/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjordanlibellx1a",
    "types": [
      "other"
    ],
    "name": "Bell X-1A",
    "scale": "1:110",
    "parts": "35",
    "designer": "Jordan Li",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/733554950383092/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCjordanlid5582",
    "types": [
      "other"
    ],
    "name": "Douglas D-558-2 Skyrocket",
    "scale": "1:110",
    "parts": "99",
    "designer": "Jordan Li",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/733554417049812/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCmarkbalderamaf104a",
    "types": [
      "other"
    ],
    "name": "Lockheed F-104A Starfighter",
    "scale": "1:110",
    "parts": "84",
    "designer": "Mark Balderama",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/520603051678284/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCwolframbroszieslittlejoelaunchpad",
    "types": [
      "other"
    ],
    "name": "Little Joe Launchpad",
    "scale": "1:110",
    "parts": "23",
    "designer": "Wolfram Broszies",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Compatible <a href=\"#MOCwolframbroszieslittlejoe\">rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanlittlejoeiilauncher",
    "types": [
      "other"
    ],
    "name": "Little Joe II Launcher",
    "scale": "1:110",
    "parts": "102",
    "designer": "Eiffleman",
    "file_types": [
      "io",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/778401709231749/\">Facebook group Bricks in Space</a>, <a href=\"https://bricksin.space/launch-vehicles/little-joe-ii/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=2\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p>Digital file at facebook page is updated version with decreased price. Instructions shown in updates of Lego Ideas page for <a href=\"https://ideas.lego.com/projects/d061bd70-11e7-4805-b5a7-dcfa21d15030/updates?project_updates_page=1\">NASA: Mercury and Gemini</a>. Compatible rocket: <a href=\"#MOCeifflemanlittlejoeii\">Little Joe II</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCluxordeathbedemdsw8train",
    "types": [
      "other"
    ],
    "name": "Air Force EMD SW8 train",
    "scale": "1:110",
    "parts": "?",
    "designer": "luxordeathbed",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c762e1ed-6fab-4063-8c50-c01d8df59d2f\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCluxordeathbedtitaniiie\">Titan IIIE Launch Vehicle</a>. Compatible <a href=\"#MOCluxordeathbedlaunchpad41\">launch pad</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanemdsw8train",
    "types": [
      "other"
    ],
    "name": "EMD SW-8 train",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanrocketgarden",
    "types": [
      "other"
    ],
    "name": "NASA Rocket Garden",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/c3efd970-24c4-443c-8b26-1d6d7f2efa2b\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a> </p>",
    "notes": "<p>Base for <a href=\"#MOCeifflemanmercuryredstone\">Mercury - Redstone</a>, <a href=\"#MOCeifflemanmercuryatlas\">Mercury - Atlas</a>, <a href=\"#MOCeifflemangeminititan\">Gemini - Titan II</a>, <a href=\"#MOCeifflemanjuno1\">Juno 1</a>, <a href=\"#MOCeifflemanjunoii\">Juno II</a>, <a href=\"#MOCeifflemanagenad\">Atlas - Agena D</a>, <a href=\"#MOCeifflemanthordelta\">Thor - Delta</a>, <a href=\"#MOCeifflemanapolloheatshield\">Apollo capsule with heatshield</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCsunder59rd108",
    "types": [
      "other"
    ],
    "name": "RD-108 Rocket engine",
    "scale": "?",
    "parts": "1364",
    "designer": "Sunder59",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://mecabricks.com/en/models/1pyj6p0KaRq\">Mecabricks</a>, <a href=\"https://www.eurobricks.com/forum/index.php?/forums/topic/157284-moc-rd-108-rocket-engine/&amp;tab=comments#comment-2898263\">Eurobricks</a>  </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCyuchrisrocketdynef1",
    "types": [
      "other"
    ],
    "name": "Rocketdyne F-1 engine",
    "scale": "?",
    "parts": "?",
    "designer": "Yu Chris",
    "file_types": [
      "lxf",
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.flickr.com/photos/123907272@N02/34908098025/in/photostream/\">Flickr</a>, <a href=\"https://drive.google.com/file/d/11h5oysdkbvfg_nzt1g7Vhs2ZChI5Mmw6/view\">Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/786718018400118/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>lxf at google drive is original version, io at Facebook contains instructions by Johan Broman.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCoveryrocketdynef1",
    "types": [
      "other"
    ],
    "name": "Rocketdyne F-1 engine",
    "scale": "?",
    "parts": "?",
    "designer": "Robert Overy",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/856732831398636/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/856733128065273/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/841881512883768/\">Facebook post with description</a>. Modification of <a href=\"#MOCyuchrisrocketdynef1\">Yu Chris's version</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanastrovan",
    "types": [
      "other"
    ],
    "name": "Apollo Era Astrovan",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a>  </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/498814967190426/\">Facebook post with pdf instructions</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkingsknightastrovan",
    "types": [
      "other"
    ],
    "name": "Astrovan",
    "scale": "1:110",
    "parts": "86",
    "designer": "KingsKnight",
    "file_types": [
      "io",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/manned-spacecraft/space-shuttle/\">Bricks in Space webpage</a>, <a href=\"https://ideas.lego.com/projects/50a447cc-0acb-4fff-b3c9-41739fed157c/updates#content_nav_tabs\">Lego Ideas</a> </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCkingsknightspaceshuttle\">NASA Space Shuttle</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanmqf",
    "types": [
      "other"
    ],
    "name": "NASA Mobile Quarantine Facility",
    "scale": "1:110",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://ideas.lego.com/projects/0f8efc2a-ce0a-4285-9f2b-036bf3eb9f38\">Lego Ideas</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a>  </p>",
    "notes": "<p>Instructions shown in updates of Lego Ideas page for <a href=\"#MOCeifflemancrawler\">NASA Crawler-Transporter</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanm113",
    "types": [
      "other"
    ],
    "name": "M113 Astronaut Rescue Vehicle",
    "scale": "1:110",
    "parts": "38",
    "designer": "Eiffleman",
    "file_types": [
      "lxf",
      "io",
      "pdf",
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/m113/\">Bricks in Space webpage</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a>, <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a> </p>",
    "notes": "<p>5 versions: 2x Apollo era, Early/Late Space Shuttle era, Mercury and Gemini era.  <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/479021959169727/\">Facebook post with digital file</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCwolframbrosziesv2launchplatoon",
    "types": [
      "other"
    ],
    "name": "V2 Launch Platoon",
    "scale": "1:110",
    "parts": "180",
    "designer": "Wolfram Broszies",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. I</a>, <a href=\"https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w\">AB's Google drive</a> </p>",
    "notes": "<p>For improved version see <a href=\"#MOCkaeroa4withvehicles\">A-4/V-2 rocket with Support and Supply vehicles</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjagdpanzerrecoveryhelicopters",
    "types": [
      "other"
    ],
    "name": "Recovery helicopters for the Apollo and Mercury Missions, Sikorsky UH-34 D Seahorse and Sikorsky SH-3D Sea King.",
    "scale": "1:110",
    "parts": "?",
    "designer": "jagdpanzer",
    "file_types": [
      "mecabricks"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.mecabricks.com/de/models/Dp2llN4A2MV\">Mecabricks</a> </p>",
    "notes": "<p>The Sea King was basis for this <a href=\"#MOCkaeroapollorecovery\">model</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkaeroapollorecovery",
    "types": [
      "other"
    ],
    "name": "Apollo recovery mission with Sikorsky H-34 Rescue Helicopter 66",
    "scale": "1:110",
    "parts": "372",
    "designer": "Mark Balderrama, Adam Wilde, Sebastian Sch\u00f6n, Matthew Hildebrand, Wolfram Broszies, Nico Daams and Kaero",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-33162/Kaero/apollo-recovery-mission/\">Rebrickable</a> </p>",
    "notes": "<p>Standalone version or full diorama version. Also in <a href=\"http://www.ametria.org/lego/\">book Bricks in space, Vol. II</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCserenityapollorecovery",
    "types": [
      "other"
    ],
    "name": "Apollo 11 Recovery Mission - changed base",
    "scale": "1:110",
    "parts": "453",
    "designer": "Serenity",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-46882/Serenity/nasa-apollo-11-recovery-mission-changed-base/\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCkaeroapollorecovery\">Apollo recovery</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnrike89apollorecovery",
    "types": [
      "other"
    ],
    "name": "Apollo recovery Helicopter 66",
    "scale": "1:110",
    "parts": "146",
    "designer": "nrike89",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-51698/nrike89/apollo-recovery-helicopter-66/#comments\">Rebrickable</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCkaeroapollorecovery\">Apollo recovery</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanvab",
    "types": [
      "other"
    ],
    "name": "NASA-Vehicle Assembly Building",
    "scale": "?",
    "parts": "1089",
    "designer": "Eiffleman",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/695713150833939/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>First presented on <a href=\"https://ideas.lego.com/projects/cedb0297-eb89-4952-a5f7-c2c1814ebb48\">Lego Ideas</a>. Digital file contains improved version that the one shown in Lego Ideas. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/556944494710806/\">Facebook post with stickers</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/532224583849464/\">Facebook post with pdf instructions (Lego Ideas version)</a>. <a href=\"#MOCadamwildelc39aarchitecture\">Compatible LC39A</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildelc39aarchitecture",
    "types": [
      "other"
    ],
    "name": "Launch Complex 39A in LEGO Architecture style",
    "scale": "?",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/763235244081729/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/759994487739138/\">Facebook post with images</a>. <a href=\"#MOCeifflemanvab\">Compatible VAB</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCeifflemanapollodsky",
    "types": [
      "other"
    ],
    "name": "Apollo Lunar Lander Display Keyboard",
    "scale": "?",
    "parts": "?",
    "designer": "Eiffleman",
    "file_types": [
      "pdf",
      "png",
      "lxf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/603138970091358/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Required graphics: <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/606170839788171/\">image 1</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/606171386454783/\">image 2</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/606172039788051/\">image 3</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/606172489788006/\">image 4</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/601237550281500/\">Digital file</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmichaelcameronastronautbackpack",
    "types": [
      "other"
    ],
    "name": "Minifig Astronaut Backpack",
    "scale": "?",
    "parts": "?",
    "designer": "Michael Cameron",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/459540957784494/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildedextre",
    "types": [
      "other"
    ],
    "name": "Dextre",
    "scale": "1:220",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/734639620274625/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/734637673608153/\">Facebook post with description</a>. Compatible with <a href=\"#SET21321-1\">ISS set</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCyourihamondiamantlaunchpad",
    "types": [
      "other"
    ],
    "name": "Diamant A launchpad of Hammaguir",
    "scale": "1:110",
    "parts": "?",
    "designer": "Youri Hamon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/773048239767096/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"#MOCyourihamondiamantlaunchpadcheap\">Cheaper version</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/773044396434147/\">Facebook post with description</a>. <a href=\"#MOCriveaudiamant\">Compatible rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCyourihamondiamantlaunchpadcheap",
    "types": [
      "other"
    ],
    "name": "Diamant A launchpad of Hammaguir, cheaper version",
    "scale": "1:110",
    "parts": "?",
    "designer": "Youri Hamon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/773048406433746/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Modification of <a href=\"#MOCyourihamondiamantlaunchpad\">launchpad</a> to get cheaper build. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/773044396434147/\">Facebook post with description</a>. <a href=\"#MOCriveaudiamant\">Compatible rocket</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonwoomerala8",
    "types": [
      "other"
    ],
    "name": "Woomera LA-8",
    "scale": "1:110",
    "parts": "3352",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/launch-pads/woomera-la-8/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Compatible rocket <a href=\"#MOCdanfallonwresat\">WRESAT</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCnicholasriveaucylinders",
    "types": [
      "other"
    ],
    "name": "Building techniques of various diameter cylinders",
    "scale": "X",
    "parts": "X",
    "designer": "Nicholas Riveau",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/905452073193378/\">Facebook group Bricks in Space</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/786910358380884/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Reference for designing cylinder rockets, compiled from various sources.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCronaldpurviancekatherinejohnson",
    "types": [
      "other"
    ],
    "name": "Katherine Johnson, Women of NASA",
    "scale": "?",
    "parts": "?",
    "designer": "Ronald Purviance",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/312835002455091/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Fifth lady from the original Lego ideas proposal for the <a href=\"#SET21312-1\">Women of NASA set</a>. Permission was not given to use her image in LEGO set.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCadamwildekatherinejohnson",
    "types": [
      "other"
    ],
    "name": "Katherine Johnson, Women of NASA",
    "scale": "?",
    "parts": "?",
    "designer": "Adam Wilde",
    "file_types": [
      "jpg"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/778465839225336/\">Facebook group Bricks in Space, comments</a> </p>",
    "notes": "<p>Stickers <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/796735440731709/\">as pdf</a> and <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/796736374064949/\">as ai</a>. Fifth lady from the original Lego ideas proposal for the <a href=\"#SET21312-1\">Women of NASA set</a>. Permission was not given to use her image in LEGO set.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCpeterzumsteinkatherinejohnson",
    "types": [
      "other"
    ],
    "name": "Katherine Johnson, Women of NASA",
    "scale": "?",
    "parts": "?",
    "designer": "Peter Zumstein",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://onedrive.live.com/?authkey=!AAEtg62WyGwUdiI&amp;id=C7FBDFAF2D77A781!370087&amp;cid=C7FBDFAF2D77A781\">Onedrive</a> </p>",
    "notes": "<p>Fifth lady from the original Lego ideas proposal for the <a href=\"#SET21312-1\">Women of NASA set</a>. Permission was not given to use her image in LEGO set. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/778465839225336/\">Facebook post with comments</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCpeterzumsteinmaryjackson",
    "types": [
      "other"
    ],
    "name": "Mary Jackson, Women of NASA",
    "scale": "?",
    "parts": "?",
    "designer": "Peter Zumstein",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://onedrive.live.com/?authkey=!AAEtg62WyGwUdiI&amp;id=C7FBDFAF2D77A781!370087&amp;cid=C7FBDFAF2D77A781\">Onedrive</a> </p>",
    "notes": "<p>Addition to the <a href=\"#SET21312-1\">Women of NASA set</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/909603176111601/\">Facebook post with photos and description</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCkevinhuangt38",
    "types": [
      "other"
    ],
    "name": "NASA T-38",
    "scale": "1:110",
    "parts": "69",
    "designer": "Kevin Huang",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-50710/kehu05/1110-nasa-t-38/\">Rebrickable</a>, <a href=\"https://bricksin.space/support-vehicles/t-38/\">Bricks in Space webpage</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/846645592407360/\">Facebook group Bricks in Space</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerapollocsmlmlunarstand",
    "types": [
      "other"
    ],
    "name": "MuscoviteSandwich's Apollo CSM/LM Lunar Stand",
    "scale": "1:220",
    "parts": "14",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "png"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-50808/SpaceXplorer%2016/muscovitesandwich-apollo-csmlm-lunar-stand/#details\">Rebrickable</a> </p>",
    "notes": "<p>Stand for <a href=\"#MOCmuscovitesandwichminiapollolem\">Mini Apollo Command module and LEM</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCfabianreichlshuttlestand",
    "types": [
      "other"
    ],
    "name": "Horizontal display for the Shuttle",
    "scale": "1:110",
    "parts": "?",
    "designer": "Fabian Reichl",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/874577966280789/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/873842476354338/\">Facebook post with description</a>. Intended for <a href=\"#MOCkingsknightspaceshuttlev2\">KingsKnight's shuttle 2020</a></p>",
    "redacted": false
  },
  {
    "entry_id": "MOCjoechambersdisplaynameplates",
    "types": [
      "other"
    ],
    "name": "Display Nameplates",
    "scale": "?",
    "parts": "?",
    "designer": "Joe Chambers",
    "file_types": [
      "io",
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://drive.google.com/drive/folders/1s_ayc1x5znytmuSB0rCVR5iM2aZdxlKO\">Joe Chamber's Google drive</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/893156294422956/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Nameplates for Apollo, Apollo Soyuz, Atlantis, Challenger, Columbia, Discovery, Endeavour, Skylab.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCmuscovitesandwichspacextesla",
    "types": [
      "other"
    ],
    "name": "Space X Model X Tesla",
    "scale": "1:110",
    "parts": "23",
    "designer": "MuscoviteSandwich",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-57018/MuscoviteSandwich/space-x-model-x-tesla-saturn-v-scale/\">Rebrickable</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCspacexplorerstarstand",
    "types": [
      "other"
    ],
    "name": "Starstand",
    "scale": "1:110",
    "parts": "30",
    "designer": "SpaceXplorer 16",
    "file_types": [
      "pdf"
    ],
    "price": "0",
    "links": "<p><a href=\"https://rebrickable.com/mocs/MOC-55530/SpaceXplorer%2016/starstand-for-1110-scale-cst-100/\">Rebrickable</a> </p>",
    "notes": "<p>Stand for <a href=\"#MOCspacexplorerstarliner\">Boeing CST-100 Starliner</a>.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfallonbeachabort",
    "types": [
      "other"
    ],
    "name": "Beach Abort",
    "scale": "1:110",
    "parts": "64",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/nasa/beach-abort/\">Bricks in Space webpage</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCsambradfordgonavigator",
    "types": [
      "other"
    ],
    "name": "GO Navigator / GO Searcher",
    "scale": "1:110",
    "parts": "404-506",
    "designer": "Sam Bradford",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://bricksin.space/support-vehicles/go-navigator-go-searcher/\">Bricks in Space webpage</a> </p>",
    "notes": "<p>Two configurations: fairing and recovery.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCriveaustarshipsupport",
    "types": [
      "other"
    ],
    "name": "Starship Support",
    "scale": "1:110",
    "parts": "316",
    "designer": "Nicholas Riveau",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://new.legorocketcollection.com/mocs/pas-de-tir/spacex/support-starship/\">Lego Rocket Collection</a> </p>",
    "notes": "",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss00stand",
    "types": [
      "fallon iss"
    ],
    "name": "00 - Stand",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/829645627440690/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale.</p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss01zarya",
    "types": [
      "fallon iss"
    ],
    "name": "01 - Zarya",
    "scale": "1:110",
    "parts": "371",
    "designer": "Dan Fallon",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/675625839509337/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/731507163921204/\">Facebook post with digital file</a>.  </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss02unity",
    "types": [
      "fallon iss"
    ],
    "name": "02 - Unity",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/728036137601640/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/731507410587846/\">Facebook post with digital file</a>, <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/727406810997906/\">facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss03zvezda",
    "types": [
      "fallon iss"
    ],
    "name": "03 - Zvezda",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "pdf",
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/729940547411199/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/731507590587828/\">Facebook post with digital file</a>. <a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/729305427474711/\">Facebook post with description</a>. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss04z1truss",
    "types": [
      "fallon iss"
    ],
    "name": "04 - Z1 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/939937709744814/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss05p6truss",
    "types": [
      "fallon iss"
    ],
    "name": "05 - P6 Truss and solar panels",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748035238935063/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss05bp6trussmodfor16to19",
    "types": [
      "fallon iss"
    ],
    "name": "05b - P6 Truss modification for 16 to 19",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761475307591056/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss06destiny",
    "types": [
      "fallon iss"
    ],
    "name": "06 - Destiny",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748035605601693/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss07esp1",
    "types": [
      "fallon iss"
    ],
    "name": "07 - ESP 1",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748035738935013/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss08canadarm",
    "types": [
      "fallon iss"
    ],
    "name": "08 - Canadarm",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748035882268332/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss09questairlock",
    "types": [
      "fallon iss"
    ],
    "name": "09 - Quest Airlock",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748036058934981/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss10pirs",
    "types": [
      "fallon iss"
    ],
    "name": "10 - Pirs",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/748036162268304/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss11.1s0truss",
    "types": [
      "fallon iss"
    ],
    "name": "11.1 - S0 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/757795897958997/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss11.2mobiletransporter",
    "types": [
      "fallon iss"
    ],
    "name": "11.2 - Mobile transporter",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/757796007958986/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss12mobilebasesystem",
    "types": [
      "fallon iss"
    ],
    "name": "12 - Mobile base system",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/760261314379122/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss13s1truss",
    "types": [
      "fallon iss"
    ],
    "name": "13 - S1 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/760261514379102/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss14p1truss",
    "types": [
      "fallon iss"
    ],
    "name": "14 - P1 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/760261727712414/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss15esp2",
    "types": [
      "fallon iss"
    ],
    "name": "15 - ESP2",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761474587591128/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss16p3p4truss",
    "types": [
      "fallon iss"
    ],
    "name": "16 - P3/P4 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761474740924446/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss17p5truss",
    "types": [
      "fallon iss"
    ],
    "name": "17 - P5 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761474884257765/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss18s3s4truss",
    "types": [
      "fallon iss"
    ],
    "name": "18 - S3/S4 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761475060924414/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss19s5truss",
    "types": [
      "fallon iss"
    ],
    "name": "19 - S5 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/761475157591071/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss20esp3",
    "types": [
      "fallon iss"
    ],
    "name": "20 - ESP3",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/769686010103319/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss21harmony",
    "types": [
      "fallon iss"
    ],
    "name": "21 - Harmony",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/769659776772609/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss22columbus",
    "types": [
      "fallon iss"
    ],
    "name": "22 - Columbus",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/769660126772574/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss23dextre",
    "types": [
      "fallon iss"
    ],
    "name": "23 - Dextre",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/769708306767756/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss24kiboelmps",
    "types": [
      "fallon iss"
    ],
    "name": "24 - KIBO-ELM-PS",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/769720820099838/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss25jempm",
    "types": [
      "fallon iss"
    ],
    "name": "25 - JEM-PM",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/770293166709270/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss26jemrms",
    "types": [
      "fallon iss"
    ],
    "name": "26 - JEM-RMS",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/770293673375886/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss27s6truss",
    "types": [
      "fallon iss"
    ],
    "name": "27 - S6 Truss",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/770294326709154/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  },
  {
    "entry_id": "MOCdanfalloniss28jemef",
    "types": [
      "fallon iss"
    ],
    "name": "28 - JEM-EF",
    "scale": "1:110",
    "parts": "?",
    "designer": "Dan Fallon",
    "file_types": [
      "io"
    ],
    "price": "0",
    "links": "<p><a href=\"https://www.facebook.com/groups/legospacebuilds/permalink/770294933375760/\">Facebook group Bricks in Space</a> </p>",
    "notes": "<p>Part of Dan Fallon's ISS in 1:110 scale. </p>",
    "redacted": false
  }
]