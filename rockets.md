<div class="navbar">
[Top](#webtop) | [TOC](#table-of-contents) | [Sources](#main-sources-of-mocs) | [**Rockets**](#table-of-rockets-and-spacecrafts) | [**Payloads**](#table-of-satellites-and-probes-payload) | [**Fallon's ISS**](#iss-by-dan-fallon) | [**Others**](#table-of-launchpads-stands-etc.) | [LEGO sets](#table-of-official-lego-sets) | [Comparisons](#comparisons) | [Candidates](candidates.html) | [Honorary](#honorary-mentions) | [Changes](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/commits/master)
</div>
# List of LEGO models of rockets, spacecrafts and probes <a name="webtop"></a>

You have built your Saturn V and now you are thinking - it is very lonely model, and something other should be standing beside. Should you keep the same scale or build something more detailed? Maybe some space probe or mars lander? Many AFOLs already had the same thought and produced many MOCs. This page is listing the MOCs with instructions available for building. The list contains projects I have found on Rebrickable (to date 8.1.2021), Lego Ideas (to date 8.1.2021), Eurobricks forum (to date 8.1.2021), AB's Google drive (20.10.2020), Facebook group Bricks in Space (to date 8.1.2021),  Facebook group Lego Rocket Collection (to date 8.1.2021), Bricklink (to date 2.3.2019), website Bricks in Space (to date 8.1.2021), MOC pages (to date 9.10.2019), Mecabricks (to date 4.10.2019), Reddit (to do), website new.legorocketcollection.com (8.1.2021).

Lists contain both official sets and MOCs that:
- are based on real rockets, spacecrafts or probes (or at least designs of planned real rockets, spacecrafts or probes),
- have got instructions, either free or for money. 

**Thanks to all who shared their work!**

Due to the nature of the universe, this list will never be complete and perfect, so please contact me with ideas for improvements and additions. 
If you are offended and you do not want your work to be included in the list, please contact me.
Contact me in this **[Eurobricks forum topic](https://www.eurobricks.com/forum/index.php?/forums/topic/173503-list-of-lego-models-of-rockets-spacecrafts-and-probes/)** or directly **[Eurobricks - Kaero](https://www.eurobricks.com/forum/index.php?/profile/167628-kaero/)**.
This page is on [Gitlab](https://gitlab.com/KaeroDot/lego-rocket-mocs).
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. The license does NOT cover images. Images are property of MOC authors.

Tables can be sorted by clicking on the heading. If you need link to a particular MOC or set, click on the image and copy address from the address bar (URL bar) of your browser.

Last updated: MAKEFILEDATEPLACEHOLDER.

## Table of contents
1. [Top](#webtop)
1. [Table of contents](#table-of-contents)
1. [Main sources of MOCs](#main-sources-of-mocs)
    1. [Bricks in space webpage](#bricks-in-space-webpage)
    1. [Bricks in space books](#bricks-in-space-books)
    1. [Facebook group Bricks in Space](#facebook-group-bricks-in-space)
    1. [Facebook group Lego Rocket Collection](#facebook-group-lego-rocket-collection)
    1. [Rebrickable, Bricklink, Mecabricks, others](#rebrickable-bricklink-mecabricks-reddit-others)
1. [Table of rockets and spacecrafts](#table-of-rockets-and-spacecrafts)
1. [Table of satellites and Probes (payload)](#table-of-satellites-and-probes-payload)
1. [Table of launchpads, stands, etc.](#table-of-launchpads-stands-etc.)
1. [ISS by Dan Fallon](#iss-by-dan-fallon)
1. [Table of official LEGO sets](#table-of-official-lego-sets)
1. [Comparisons](#comparisons)
    1. [A4, 1:110 scale](#a4-1110-scale)
    1. [Space Shuttle](#space-shuttle)
    1. [Launch Umbilical Towers, 1:110 scale](#launch-umbilical-towers-1110-scale)
    1. [Soyuz](#soyuz)
    1. [Energia family](#energia-family)
    1. [ISS](#iss)
    1. [Falcon and Falcon heavy](#falcon-and-falcon-heavy)
    1. [Titan II Gemini Launch Vehicle](#titan-ii-gemini-launch-vehicle)
1. [List of found space projects that were considered as candidates](candidates.html)
    1. [Lego Ideas](candidates.html#projects-found-on-lego-ideas)
    1. [Eurobricks](candidates.html#projects-found-on-eurobricks)
    1. [Bricklink](candidates.html#projects-found-on-bricklink)
    1. [MOC pages](candidates.html#projects-found-on-moc-pages)
    1. [MECABRICKS](candidates.html#projects-found-on-mecabricks)
    1. [reddit](candidates.html#projects-found-on-reddit)
    1. [other](candidates.html#other)
1. [Honorary mentions](#honorary-mentions)
1. [History of changes](#history-of-changes)


## Main sources of MOCs
### Bricks in space webpage
[Bricks in Space webpage](https://bricksin.space/) was created by the most prolific MOC designers (real space MOCs), freely sharing their own work. This is the main and first web to look for MOC designs.

### Bricks in space books
Series of wonderful books [Bricks in space, Modelling Spaceflight with Lego](http://www.ametria.org/lego/) were prepared by Wolfram Broszies and can be download for free. Books offer description and technical data for many rockets and space ships with many pictures.
Also instructions for many models are included. The models are listed in following tables.
- First volume contains models: [A4](#MOCmarkbalderramaa4), [V2 launch platoon](#MOCwolframbrosziesv2launchplatoon), [Redstone](#MOCwolframbrosziesredstone), [Vanguard](#MOCbenjaminunisvanguard), [Scout X-1](#MOCbenjaminunisscoutx1), [Juno I](#MOCeifflemanjuno1), [Juno II](#MOCeifflemanjunoii), [Thor-Delta D](#MOCeifflemanthordelta), [Atlas-Agena](#MOCeifflemanagenad), [Little Joe](#MOCwolframbroszieslittlejoe), [Mercury Redstone](#MOCeifflemanmercuryredstone), [Launch Pad 5](#MOCeifflemanmercuryredstonelaunchset), [Atlas Mercury](#MOCeifflemanmercuryatlas), [Titan Gemini](#MOCeifflemangeminititan), [Rocket Garden](#MOCeifflemanrocketgarden).
- Second volume contains models: [Saturn I](#MOCdavidwells44saturn1series), [Saturn Ib](#MOCdavidwells44saturn1bseries), [Launch Complex 34-37](#MOCdavidwellinglaunchcomplex34), [The Milkstool](#MOCdavidwellingmilkstool), [Little Joe II](#MOCeifflemanlittlejoeii), [Saturn V](#SET21309-1), [Astrovan](#MOCeifflemanastrovan), [M-113 Astronaut Rescue Vehicle](#MOCeifflemanm113), [Lunar Rover](#MOCXlunarrover), [Rescue Helicopter 66](#MOCkaeroapollorecovery), [Mobile Quarantaine Facility](#MOCeifflemanmqf), [Skylab](#MOCeifflemanskylab), [The Crawler](#MOCeifflemancrawler).
- Third volume contains models: [Launch Umbilical Tower](#MOCnathanrlut).

### Facebook group Bricks in Space
[Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/) is regularly visited by majority of real space MOC designers. The file section of this group is the largest archive of digital files and instructions, unfortunately unsorted. Therefore a [AB's Google drive](https://drive.google.com/drive/folders/1XvqSDLwJMsE86fSwTltEGYQhHqX3-p1w) was set by Alexandru Bauer and is maintained from time to time.

### Facebook group Lego Rocket Collection and its website
[Facebook group Lego Rocket Collection](https://www.facebook.com/groups/178082023364995/) is a facebook private group about a collection of rockets built in scale 1: 110 by Nicholas Riveau. You can support Nicholas here: [https://en.tipeee.com/legorocketcollection/](https://en.tipeee.com/legorocketcollection/). Recently a webpage showing the collection was started: [https://new.legorocketcollection.com](https://new.legorocketcollection.com).

### Rebrickable, Bricklink, Mecabricks, Reddit, others
Some MOCs are posted to [Rebrickable](https://rebrickable.com/), very convenient web page that can manage your parts.
Some MOCs can be found on [Bricklink](https://www.bricklink.com/).
There is a discussion forum at [legoRockets at Reddit](https://www.reddit.com/r/legoRockets/) with occasional instructions.
Few MOCs are available at [Mecabricks](https://www.mecabricks.com/), one can see this models, unfortunately building using Mecabricks will be hard because export is not possible.
Minimum of real space MOCs can be found on [Eurobricks forum](https://www.eurobricks.com/).
There is a lot of MOCs on [MOCpages](http://www.moc-pages.com), but mostly without any digital file nor instructions.
Of course [Lego Ideas](https://ideas.lego.com/) hosts several real space MOCs but only a few contributors share the instructions.

## Table of rockets and spacecrafts
BUILDTABLEPLACEHOLDER(types=Rocket)

## Table of satellites and probes (payload)
BUILDTABLEPLACEHOLDER(types=Payload)

## ISS by Dan Fallon
This project is so large a separate table is needed.
BUILDTABLEPLACEHOLDER(types=Fallon ISS)

## Table of launchpads, stands, etc.
BUILDTABLEPLACEHOLDER(types=Other)

## Table of official LEGO sets
Images | ID |  Name                   | Year | Scale     | Parts  | Designer | Links | Notes
---|---|---|---|---|---|---|---|---
!<SET7467-1>!   | 7467-1        | International Space Station | 2003 | ? | 162 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38859#T=P) | 
!<SET7468-1>!   | 7468-1        | Saturn V Moon Mission | 2003 | ? | 178 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=7468-1&name=Saturn V Moon Mission&category=[Discovery]#T=P) | 
!<SET7469-1>!   | 7469-1        | Mission to Mars | 2003 | ? | 417 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38850#T=P) | 
!<SET7470-1>!   | 7470-1        | Space Shuttle Discovery | 2003 | ? | 826 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=7470-1#T=P) | 
!<SET7471-1>!   | 7471-1        | Mars Exploration Rover | 2003 | ? | 867 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?id=38856#T=P) | 
!<SET8480-1>!   | 8480-1        | Space Shuttle | 1996 | ? | 1366 | LEGO | [Bricklink](https://www.bricklink.com/v2/catalog/catalogitem.page?S=8480-1) | 
!<SET10029-1>!  | 10029-1       | Lunar Lander | 2003 | ? | 468 | LEGO | [Rebrickable](https://rebrickable.com/sets/10029-1/lunar-lander/) | 
!<SET10213-1>!  | 10213-1       | Shuttle Adventure | 2010 | ? | 1203 | LEGO | [Rebrickable](https://rebrickable.com/sets/10213-1/shuttle-adventure) |
!<SET10231-1>!  | 10231-1       | Shuttle Expedition | 2011 | ? | 1230 | LEGO | [Rebrickable](https://rebrickable.com/sets/10231-1/shuttle-expedition/) |
!<SET31091-1>!  | 31091-1       | Shuttle Transporter | 2019 | mixed | 341 | LEGO | [Rebrickable](https://rebrickable.com/sets/31091-1/shuttle-transporter/) |
!<SET10266-1>!  | 10266-1       | NASA Apollo 11 Lunar Lander | 2019 | ? | 1087 | LEGO | [Rebrickable](https://rebrickable.com/sets/10266-1/nasa-apollo-11-lunar-lander/) | 
!<SET21101-1>!  | 21101-1       | Hayabusa | 2012 | ? | 368 | Daisuke Okubo  (LEGO Ideas nick: daisuke)| [Rebrickable](https://rebrickable.com/sets/21101-1/hayabusa/) | Origin: LEGO Cuusoo, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/5dfdb4a5-ea65-43bd-b671-f7c3cb125b5c), 
!<SET21104-1>!  | 21104-1       | Mars Science Laboratory Curiosity Rover | 2014 | ? | 295 | Perijove | [Rebrickable](https://rebrickable.com/sets/21104-1/mars-science-laboratory-curiosity-rover/) | Origin: LEGO Cuusoo,  [original MOC on Rebrickable](#MOCperijovecuriosity)
!<SET21309-1>!  | 21309-1       | NASA Apollo Saturn V | 2017 | 1:110 | 1969 | whatsuptoday and saabfan | [Rebrickable](https://rebrickable.com/sets/21309-1/nasa-apollo-saturn-v/) | Origin: LEGO Ideas, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/3519b723-c59a-43b4-81be-eeb99b631627)
!<SET21312-1>!  | 21312-1       | Women of NASA | 2017 | ? | 231 | LEGO Ideas nick: 20tauri | [Rebrickable](https://rebrickable.com/sets/21312-1/women-of-nasa/) | Origin: LEGO Ideas, [original MOC on LEGO Ideas](https://ideas.lego.com/projects/388ddbe3-2f0a-42fb-9f54-93bf3b5f4fe9)
!<SET21321-1>!  | 21321-1       | International Space Station | 2020 | 1:220 - 1:230 | 864 | LEGO Ideas nick: 20tauri | [Rebrickable](https://rebrickable.com/sets/21321-1/international-space-station/) | Origin: LEGO Ideas, 10 Year anniversary fan vote, [original MOC on LEGO Ideas](https://ideas.lego.com/challenges/5fa4eb3f-1e98-47d7-abbc-fdc2a29b79c3/application/2ae74ed1-0c39-4e4b-8862-06409fb6c7a4)


## Comparisons
Because some rockets or spacecrafts are represented in several MOCs, I was curious how comparable are. I hope no one gets offended!

### A4, 1:110 scale
Models by [MuscoviteSandwich](#MOCmuscovitesandwichv2), [Mark Balderama](#MOCmarkbalderramaa4), and [Kaero](#MOCkaeroa4withvehicles).
<div class="COMimg">
![COMa4](imgCOM/COMa4.jpg "COMa4")
</div>

### Space Shuttle
<div class="COMimg">
This comparison is by KingsKnight from Lego Ideas page.
From left: real Space Shuttle, [KingsKnight](#MOCkingsknightspaceshuttle), [Lego set 10231](#SET10231-1).
![COMspaceshuttle](imgCOM/COMspaceshuttle.jpg "COMspaceshuttle")
</div>  

.

Comparison by Tony Bela. [International Space Station](#SET21321-1), [Shuttle Transporter](#SET31091-1).
<div class="COMimg">
![COMspaceshuttleiss](imgCOM/COMspaceshuttleiss.jpg "COMspaceshuttleiss")
</div>

### Launch Umbilical Towers, 1:110 scale
Pictures made by Wolfram Broszies. Click the image to show large size image (20 MB png!).
<div class="COMimg">
From left: Real LUT, [Bailey Fullarton](#MOCbaileyfullartonlut), [Joseph Chambers](#MOCjosephchamberslut), [Michael Cameron](#MOCmichaelcameronlut), [Nathan Readioff](#MOCnathanrlut), [Valerie Roche, Greg Kremer (version 10)](#MOCrochekremerlut).
[![COMluts](imgCOM/COMluts.jpg)](imgCOM/COMluts.png "COMluts")
</div>

### Soyuz
<div class="COMimg">
Various soyuz rockets. From left: [suchshibeinu](#MOCsuchshibeinusoyuz), [tech_niek](#MOCtechnieksoyuzrocket), [endrega](#MOCendregasoyuz), [Andor Schindler](#MOCandorschindlersoyuz21v), [Andor Schindler](#MOCandorschindlersoyuzfg), [Stania](#MOCstaniasoyuz).
Click the image to show large one.
[![COMsoyuz](imgCOM/COMsoyuz.jpg)](imgCOM/COMsoyuz_large.jpg "COMsoyuz")
</div>

### Energia family
<div class="COMimg">
Energia family by Allan Jordan, with help of Sunder59, KingsKnight, David Welling and Sebastian Schoen.
The family consists of several modules that allow interchangeable construction.

On the [figures](https://www.facebook.com/groups/legospacebuilds/permalink/943500896055162/) from left:
1. [Vulkan-Hercules (Variant 2)](#MOCallanjordanvulkanherculesv2)
1. [Vulkan-Hercules (Variant 2b)](#MOCallanjordanvulkanherculesv2b)
1. [Vulkan-Hercules (Variant 2a)](#MOCallanjordanvulkanherculesv2a)
1. [Vulkan-Hercules (Variant 1)](#MOCallanjordanvulcanhercules)
1. [Vulkan](#MOCallanjordanvulcan)
1. [Energia II / Uragan (Variant 2)](#MOCallanjordanuragan2)
1. [Energia II / Uragan (Variant 1)](#MOCallanjordanuragan1)
1. [Energia-Buran](#MOCallanjordanburan)
1. [Energia-Polyus](#MOCallanjordanpolyus)
1. [Buran-T (Variant 2) (record missing)]()
1. [Buran-T (Variant 1)](#MOCallanjordanburant)
1. [Energia](#MOCallanjordanenergia)
1. [Energia-M](#MOCallanjordanenergiam)

Up to it there is also: [Buran only MOC](#MOCallanjordanburanonly) and [Polyus only MOC](#MOCallanjordanpolyusonly).

![COMenergia](imgCOM/COMenergia.jpg)
![COMenergia](imgCOM/COMenergiab.jpg)
</div>

### ISS
#### by Olivier Rt
Comparison of the real ISS and the [set 21321-1](#SET21321-1) by Olivier Rt:

[https://olivierrt.wordpress.com/2020/10/20/lego-iss-21321-vs-the-real-international-space-station/](https://olivierrt.wordpress.com/2020/10/20/lego-iss-21321-vs-the-real-international-space-station/)


#### By Riccardo Russo et al.
Elements of the [set 21321-1](#SET21321) to the real ISS with history by Riccardo Russo, Luca Peccatori, Johan Broman and Zovits Ádám.

[Facebook group Bricks in Space](https://www.facebook.com/groups/legospacebuilds/permalink/907131853025400/), [Youtube video](https://youtu.be/ZBIZI4w7FL8)

### Falcon and Falcon Heavy
<div class="COMimg">
Falcon and Falcon heavy rockets in 1:110 scale. 
From left: [RepRage](#MOCrepragefalcon9), [endrega](#MOCendregaspacexfalcon9), [curtisdf](#MOCcurtisdffalcon9), [Xael](#MOCxaelsootyblockvfalcon9crewdragon), [? at AB's Google drive](#MOCXfalconheavy), [curtisdf](#MOCcurtisdffalconheavy), [Eiffleman](#MOCeifflemanfalconheavy), [endrega](#MOCendregaspacexfalconheavy), [Youri Hamon](#MOCyourihamonspacexfalconheavy).
Click the image to show large one.
[![COMfalcon](imgCOM/COMfalcon.jpg)](imgCOM/COMfalcon_large.jpg "COMfalcon")
</div>

### Titan II Gemini Launch Vehicle
<div class="COMimg">
Titan II GLV rockets in 1:110 scale.
From left:
[SpaceXplorer 16](#MOCspacexplorertitanglv), [Gilles Nawrocki's modification](#MOCgillesnawrockititaniiglvpad), [Kevin Huang, bedrockblaze135, Dan Fallon](#MOCkevinhuangtitaniiglv), [MuscoviteSandwich](#MOCmuscovitesandwichgeminititan), [legorockets](#MOClegorocketsgeminititan), [Joel Denning's](#MOCjoeldenninggeminititanmod) and [luxordeathbed's](#MOCluxordeathbedgeminitianalternative) modifications, [Eiffleman](#MOCeifflemangeminititan).
Click the image to show large one.
[![COMgeminititan](imgCOM/COMgeminititan.jpg)](imgCOM/COMgeminititan_large.jpg "COMgeminititan")
</div>

# Honorary mentions
**Bob Turner**
[Red Dwarf Lego](https://ideas.lego.com/projects/476ec4f8-d2a4-44d2-b70c-e67a2256d20d) - [The instructions for the Red Dwarf LEGO will be posted on this website on August 2020](https://www.bobsvintagebricks.com/reddwarflego)
**TheMugbearer**
X [FTL: Faster Than Light](https://ideas.lego.com/projects/714583f1-17c8-455a-969f-d5f9f9be42d1)

# History of changes
Git log ouput can be found on [Gitlab project site](https://gitlab.com/KaeroDot/lego-rocket-mocs/-/commits/master).

---

<a href='http://www.freevisitorcounters.com'>freevisitorcounters</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=4812884542147d964c56cc503265b2bd9635c0a9'></script>
<script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/755066/t/4"></script>
<script src="table_data.js" type="application/javascript"></script>
<script src="column_data.js" type="application/javascript"></script>
<script src="filter_table.js" type="application/javascript"></script>
