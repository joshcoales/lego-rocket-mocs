List of LEGO models of rockets, spacecrafts and probes.

Lists contain both official sets and MOCs that:
-are based on real rockets, spacecrafts or probes (or at least designs of real rockets, spacecrafts or probes),
-have got instructions, either free or for money.
Due to the nature of the universe, this list will never be complete and perfect, so feel free to contact me for improvements and additions.

Thanks to all who shared their work!

Webpage:
https://kaerodot.gitlab.io/lego-rocket-mocs/